Pod::Spec.new do |s|
	s.platform = :ios
	s.ios.deployment_target = '9.3'
	s.name             = 'denticom'
	s.version          = '0.1.0'
	s.summary          = 'cache for denti'
	s.requires_arc 	   = true

	s.description      = <<-DESC
Common headers used by all denti libraries
	DESC

	s.homepage         = 'https://github.com/micartu/denticom'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'micartu' => 'michael.artuerhof@gmail.com' }
	s.source           = { :git => 'https://github.com/micartu/denticom.git', :tag => s.version.to_s }

	# s.ios.deployment_target = '9.3'

	s.source_files = 'denticom/**/*.{swift}'

	s.frameworks = 'UIKit'
	s.dependency 'RxSwift', '~> 4.3.1'
	s.swift_version = "4.2"
end
