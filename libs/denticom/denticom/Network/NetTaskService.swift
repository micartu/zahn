//
//  NetTaskService.swift
//  denticom
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public struct ClinicTask {
    public let task: Task
    public let chairId: String
    public let creatorId: String
    public let forPersonId: String

    public init(task: Task,
                chairId: String,
                creatorId: String,
                forPersonId: String) {
        self.task = task
        self.chairId = chairId
        self.creatorId = creatorId
        self.forPersonId = forPersonId
    }
}

public protocol NetTaskService {
    func getTasksFor(clinicId: String,
                     from dateStart: Date,
                     till dateEnd: Date,
                     success: (([ClinicTask]) -> Void)?,
                     failure: ((NSError) -> Void)?)
}
