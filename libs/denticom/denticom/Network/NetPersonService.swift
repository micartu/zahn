//
//  NetPersonService.swift
//  denticom
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NetPersonService {
    func getPersonInfo(organization: Org,
                       requester rlogin: String,
                       requested ulogin: String,
                       success: ((Person) -> Void)?,
                       failure: ((NSError) -> Void)?)
    func updatePersonInfo(organization: Org,
                          creator: String,
                          person p: Person,
                          updPassword: String,
                          success: (() -> Void)?,
                          failure: ((NSError) -> Void)?)
    func addPerson(organization: Org,
                   creator: String,
                   person p: Person,
                   updPassword: String,
                   success: ((Person) -> Void)?,
                   failure: ((NSError) -> Void)?)
}
