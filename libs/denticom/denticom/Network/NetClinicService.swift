//
//  NetClinicService.swift
//  denticom
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct ClinicInfo {
    public let clinic: Clinic
    public let tasks: [Task]
    public let chairs: [Chair]

    public init(clinic: Clinic,
                tasks: [Task],
                chairs: [Chair]) {
        self.clinic = clinic
        self.tasks = tasks
        self.chairs = chairs
    }
}

public protocol NetClinicService {
    func getClinicsFor(organization: Org, and login: String,
                       success: (([Clinic]) -> Void)?,
                       failure: ((NSError) -> Void)?)
    func getClinicsRolesFor(organization: Org, and login: String,
                            success: (([ClinicPersonRoles]) -> Void)?,
                            failure: ((NSError) -> Void)?)
    func getInfoFor(clinicId: String,
                    success: ((ClinicInfo) -> Void)?,
                    failure: ((NSError) -> Void)?)
}
