//
//  NetOrgService.swift
//  denticom
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NetOrgService {
    func getOrganizations(like name: String, for user: String,
                          success: (([Org]) -> Void)?,
                          failure: ((NSError) -> Void)?)
}
