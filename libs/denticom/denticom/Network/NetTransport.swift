//
//  NetTransport.swift
//  denticom
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NetTransportService {
    // GET
    func getQuery(url: String,
                  success: ((NSDictionary) -> Void)?,
                  failure: ((NSError) -> Void)?)
    func getQuery(url: String,
                  success: (([NSDictionary]) -> Void)?,
                  failure: ((NSError) -> Void)?)

    // GET-raw (e.g. for loading images)
    func getRawQuery(url: String,
                     success: ((Data) -> Void)?,
                     failure: ((NSError) -> Void)?)

    // POST
    func postQuery(url: String,
                   data: Data,
                   success: ((NSDictionary) -> Void)?,
                   failure: ((NSError) -> Void)?)
    func postQuery(url: String,
                   data: Data,
                   success: (([NSDictionary]) -> Void)?,
                   failure: ((NSError) -> Void)?)
}
