//
//  NetLoginService.swift
//  denticom
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NetLoginService {
    func changeCredentials(login: String,
                           password: String,
                           success: (() -> Void)?,
                           failure: ((NSError) -> Void)?)
    func logout(success: (() -> Void)?,
                failure: ((NSError) -> Void)?)
}
