//
//  NetManager.swift
//  denticom
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NetManagerService: NetTransportService {
    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?)
    func logout(success: ((NSDictionary) -> Void)?,
                failure: ((NSError) -> Void)?)
}
