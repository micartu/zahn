//
//  String+Localization.swift
//  denti
//
//  Created by Michael Artuerhof on 15.06.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension String {
    public var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
