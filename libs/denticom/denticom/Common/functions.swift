//
//  functions.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.07.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

public func runOnMainThread(_ closure: @escaping ()->()) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async {
            closure()
        }
    }
}
