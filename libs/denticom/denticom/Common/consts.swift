//
//  consts.swift
//  denti
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Const {
    // string constants
    public static let dqueue = "com.bearmonti.denti"
    public static let domain = "denti_domain"
    public static let iv = "vsBUDZZc"

    // error codes
    public struct err {
        public static let kNet = -1
        public static let kNetAuth = -2
    }

    // intervals
    public static let kAnimationTime: TimeInterval = 0.6

    public struct notification {
        public static let kBackground = "BackgroundNotification"
        public static let kForeground = "ForegroundNotification"
        public static let kShowChannels = "ShowChannelsNotification"
        public static let kAuthErr = "AuthErrNotification"
    }

    public struct keys {
        public static let kDinaMenus = "DinamicalMenus"
        public static let kHide = "HideMenu"
        public static let kMenu = "MenuToShow"
        public static let kSection = "SectionOfMenu"
    }

    public struct ids {
        public static let kCurPerson = "CurrentLoggedPerson"
        public static let kCurOrg = "CurrentLoggedOrganization"
    }

    public struct text {
        public static let kError = "Error".localized
        public static let kClose = "Close".localized
        public static let kOk = "Ok".localized
        public static let kCancel = "Cancel".localized
        public static let kCancelAction = "CancelAction".localized
        public static let kYes = "Yes".localized
        public static let kNo = "No".localized
    }
}
