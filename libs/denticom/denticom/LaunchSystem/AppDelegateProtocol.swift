//
//  AppDelegateProtocol.swift
//  denticom
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#if os(macOS)
import Cocoa
#else
import UIKit
#endif

public protocol AppDelegateProtocol {
    func initModule() -> Bool
#if os(iOS)
    var backgroundMethod: ((UIApplication) -> Void)? { get }
    var foregroundMethod: ((UIApplication) -> Void)? { get }
    var remoteNotification: (([AnyHashable : Any]) -> Void)? { get }
    var canOpenURL: ((UIApplication, URL, [UIApplication.OpenURLOptionsKey : Any]) -> Bool)? { get }
#endif
    func description() -> String
}
