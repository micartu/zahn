//
//  denticom.h
//  denticom
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for denticom.
FOUNDATION_EXPORT double denticomVersionNumber;

//! Project version string for denticom.
FOUNDATION_EXPORT const unsigned char denticomVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <denticom/PublicHeader.h>


