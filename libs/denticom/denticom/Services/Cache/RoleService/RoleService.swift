//
//  RoleService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheRoleService {
    // common funcs
    func exists(role id: String) -> Bool

    // get
    func get(role: String) -> Role?

    // update
    func update(role: Role, completion: @escaping (() -> Void))

    // create
    func create(role: Role, completion: @escaping (() -> Void))

    // delete
    func delete(role id: String, completion: @escaping (() -> Void))
}
