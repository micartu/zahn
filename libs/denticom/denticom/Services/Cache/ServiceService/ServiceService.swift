//
//  ServiceService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheServiceService {
    // common funcs
    func exists(service id: String) -> Bool

    // get
    func get(service: String) -> Service?

    // update
    func update(service: Service, completion: @escaping (() -> Void))

    // create
    func create(service: Service, completion: @escaping (() -> Void))

    // delete
    func delete(service id: String, completion: @escaping (() -> Void))
}

public protocol CacheServiceRatingService {
    func add(rating: Rating, to srv: Service, completion: @escaping (() -> Void))
    func add(rating: Rating, srvId: String, completion: @escaping (() -> Void))
    func add(rating: String, srvId: String, completion: @escaping (() -> Void))
    func ratings(service: String) -> [Rating]
}
