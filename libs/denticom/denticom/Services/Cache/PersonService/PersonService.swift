//
//  PersonService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CachePersonService {
    // common funcs
    func exists(person login: String) -> Bool

    // get
    func get(person: String) -> Person?
    func ratingsFor(person: Person, batchSize: Int, step: Int) -> [Rating]

    // update
    func update(person: Person, completion: @escaping (() -> Void))

    // add
    func add(ratings: [Rating], to p: Person, completion: @escaping (() -> Void))

    // create
    func create(person: Person, completion: @escaping (() -> Void))

    // delete
    func delete(person id: String, completion: @escaping (() -> Void))
    func delete(ratings: [Rating], from p: Person, completion: @escaping (() -> Void))
}
