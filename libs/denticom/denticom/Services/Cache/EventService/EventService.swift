//
//  EventService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheEventService {
    // common funcs
    func exists(event id: String) -> Bool

    // get
    func get(event: String) -> Event?

    // update
    func update(event: Event, completion: @escaping (() -> Void))

    // create
    func create(event: Event, completion: @escaping (() -> Void))

    // delete
    func delete(event id: String, completion: @escaping (() -> Void))
}

public protocol CacheEventPersonService {
    // common funcs
    func takenEventExistsFor(person pid: String, event eid: String) -> Bool
    func madenEventExistsFor(person pid: String, event eid: String) -> Bool

    // get
    func takenEventsFor(person: Person, batchSize: Int, step: Int) -> [Event]
    func madenEventsFor(person: Person, batchSize: Int, step: Int) -> [Event]
    func servicesFor(event: Event) -> [Service]

    // add
    func addTakenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void))
    func addMadenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void))
    func addTakenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void))
    func addMadenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void))
    func add(services: [Service], costs: [Cost], to e: Event, completion: @escaping (() -> Void))

    // delete
    func deleteTakenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void))
    func deleteMadenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void))
    func delete(services: [Service], from e: Event, completion: @escaping (() -> Void))
}
