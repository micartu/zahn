//
//  RatingService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheRatingService {
    // common funcs
    func exists(rating id: String) -> Bool

    // get
    func get(rating: String) -> Rating?

    // update
    func update(rating: Rating, completion: @escaping (() -> Void))

    // create
    func create(rating: Rating, completion: @escaping (() -> Void))

    // delete
    func delete(rating id: String, completion: @escaping (() -> Void))
}

public protocol CacheEventRatingEvent {
    func add(rating: Rating, to evnt: Event, completion: @escaping (() -> Void))
    func add(rating: Rating, evntId: String, completion: @escaping (() -> Void))
    func add(rating: String, evntId: String, completion: @escaping (() -> Void))
    func ratings(evntId: String) -> [Rating]
}
