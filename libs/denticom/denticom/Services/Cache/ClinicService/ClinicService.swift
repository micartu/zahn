//
//  ClinicService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheClinicService {
    // common funcs
    func exists(clinic id: String) -> Bool
    func exists(role: Role, for p: Person, in clinic: Clinic) -> Bool
    func exists(event e: Event, in clinic: Clinic) -> Bool

    // get
    func get(clinic: String) -> Clinic?
    func ratingsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Rating]
    func eventsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Event]
    func clinicsFor(personId: String) -> [Clinic]

    // update
    func update(clinic: Clinic, completion: @escaping (() -> Void))

    // add
    func add(ratings: [Rating], to clinic: String, completion: @escaping (() -> Void))
    func add(role: Role, for p: Person, in clinic: String, completion: @escaping (() -> Void))

    // create
    func create(clinic: Clinic, completion: @escaping (() -> Void))

    // delete
    func delete(clinic c: String, completion: @escaping (() -> Void))
    func delete(ratings: [Rating], in clinic: String, completion: @escaping (() -> Void))
    func delete(events: [Event], in clinic: String, completion: @escaping (() -> Void))
    func delete(role: Role, for p: Person, in clinic: String, completion: @escaping (() -> Void))
}
