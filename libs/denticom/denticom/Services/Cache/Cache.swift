//
//  Cache.swift
//  denticom
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

// protocol agregator CacheService
public protocol CacheService: CachePersonService,
    CachePersonColorService,
    CacheOrgService,
    CacheOrgPersonService,
    CacheOrgClinicService,
    CacheClinicService,
    CacheClinicChairService,
    CacheChairService,
    CacheTaskService,
    CacheClinicPersonTaskService,
    CacheEventService,
    CacheCostService,
    CacheEventPersonService,
    CacheServiceCostService,
    CachePhoneService,
    CacheRatingService,
    CacheServiceService,
    CacheServiceRatingService,
    CacheEventRatingEvent,
    CacheJrnl,
    CacheRoleService { }
