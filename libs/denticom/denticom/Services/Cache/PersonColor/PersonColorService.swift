//
//  PersonColorService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CachePersonColorService {
    func set(color: IColor, for pid: String, completion: @escaping (() -> Void))
    func getColor(for pid: String) -> IColor?
}
