//
//  TaskService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheTaskService {
    // common funcs
    func exists(task id: String) -> Bool

    // get
    func get(task: String) -> Task?

    // update
    func update(task: Task, completion: @escaping (() -> Void))

    // create
    func create(task: Task, completion: @escaping (() -> Void))

    // delete
    func delete(task id: String, completion: @escaping (() -> Void))
}
