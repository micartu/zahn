//
//  Org.swift
//  denticom
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public class Org: NSObject, NSCoding {
    public let id: String
    public let name: String
    public let notes: String
    public let address: String

    public init(id: String,
                name: String,
                notes: String,
                address: String) {
        self.id = id
        self.name = name
        self.notes = notes
        self.address = address
    }

    // MARK: - NSCoding

    required convenience public init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let notes = aDecoder.decodeObject(forKey: "notes") as! String
        let address = aDecoder.decodeObject(forKey: "address") as! String
        self.init(id: id, name: name, notes: notes, address: address)
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(notes, forKey: "notes")
        aCoder.encode(address, forKey: "address")
    }
}
