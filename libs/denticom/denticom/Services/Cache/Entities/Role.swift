//
//  Role.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Role {
    public let id: String
    public let name: String
    public let right: String

    public init(id: String,
                name: String,
                right: String) {
        self.id = id
        self.name = name
        self.right = right
    }
}
