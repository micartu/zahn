//
//  Person.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public class Person: NSObject, NSCoding {
    public let id: String
    public let login: String
    public let name: String
    public let midname: String
    public let surname: String
    public let phones: [String]
    public let address: String

    public init(id: String,
                login: String,
                name: String,
                midname: String,
                surname: String,
                phones: [String],
                address: String) {
        self.id = id
        self.login = login
        self.name = name
        self.midname = midname
        self.surname = surname
        self.phones = phones
        self.address = address
    }

    // MARK: - NSCoding

    required convenience public init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let login = aDecoder.decodeObject(forKey: "login") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let midname = aDecoder.decodeObject(forKey: "midname") as! String
        let surname = aDecoder.decodeObject(forKey: "surname") as! String
        let phones = aDecoder.decodeObject(forKey: "phones") as! [String]
        let address = aDecoder.decodeObject(forKey: "address") as! String
        self.init(id: id,
                  login: login,
                  name: name,
                  midname: midname,
                  surname: surname,
                  phones: phones,
                  address: address)
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(login, forKey: "login")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(midname, forKey: "midname")
        aCoder.encode(surname, forKey: "surname")
        aCoder.encode(phones, forKey: "phones")
        aCoder.encode(address, forKey: "address")
    }
}
