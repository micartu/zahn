//
//  Rating.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Rating {
    public let id: String
    public let creatorId: String
    public let date: Date
    public let stars: Int
    
    public init(id: String,
                creatorId: String,
                date: Date,
                stars: Int) {
        self.id = id
        self.creatorId = creatorId
        self.date = date
        self.stars = stars
    }
}
