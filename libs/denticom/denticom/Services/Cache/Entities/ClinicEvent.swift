//
//  ClinicEvent.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct ClinicEvent {
    public let clinicId: String
    public let eventId: String
    
    public init(clinicId: String, eventId: String) {
        self.clinicId = clinicId
        self.eventId = eventId
    }
}
