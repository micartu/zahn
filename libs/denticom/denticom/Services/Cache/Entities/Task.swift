//
//  Task.swift
//  denticom
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public struct Task {
    public let id: String
    public let name: String
    public let time: Date
    public let duration: Double
    public let descr: String

    public init(id: String,
                name: String,
                time: Date,
                duration: Double,
                descr: String) {
        self.id = id
        self.name = name
        self.time = time
        self.duration = duration
        self.descr = descr
    }
}
