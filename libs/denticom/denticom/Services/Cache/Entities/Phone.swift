//
//  Phone.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Phone {
    public let id: String
    public let number: String
    
    public init(id: String, number: String) {
        self.id = id
        self.number = number
    }
}
