//
//  PersonEvent.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct PersonEvent {
    public let personId: String
    public let eventId: String
    
    public init(personId: String, eventId: String) {
        self.personId = personId
        self.eventId = eventId
    }
}
