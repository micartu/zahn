//
//  IColor.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public struct IColor {
    public let r: Int
    public let g: Int
    public let b: Int
    public let a: Int

    public init(r: Int,
                g: Int,
                b: Int,
                a: Int) {
        self.r = r
        self.g = g
        self.b = b
        self.a = a
    }
}
