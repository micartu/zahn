//
//  PersonRating.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct PersonRating {
    public let personId: String
    public let ratingId: String
    
    public init(personId: String, ratingId: String) {
        self.personId = personId
        self.ratingId = ratingId
    }
}
