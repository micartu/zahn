//
//  Chair.swift
//  denticom
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public struct Chair {
    public let id: String
    public let name: String
    public let descr: String

    public init(id: String,
                name: String,
                descr: String) {
        self.id = id
        self.name = name
        self.descr = descr
    }
}
