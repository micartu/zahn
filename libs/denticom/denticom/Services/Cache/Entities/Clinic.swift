//
//  Clinic.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Clinic {
    public let id: String
    public let name: String
    public let address: String

    public init(id: String,
                name: String,
                address: String) {
        self.id = id
        self.name = name
        self.address = address
    }
}

public struct ClinicPersonRoles {
    public let clinic: Clinic
    public let roles: [String]

    public init(clinic: Clinic,
                roles: [String]) {
        self.clinic = clinic
        self.roles = roles
    }
}
