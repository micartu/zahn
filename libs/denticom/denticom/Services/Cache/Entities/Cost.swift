//
//  Cost.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Cost {
    public let id: String
    public let currency: String
    public let cost: Double
    
    public init(id: String,
                currency: String,
                cost: Double) {
        self.id = id
        self.currency = currency
        self.cost = cost
    }
}
