//
//  JrnlCmd.swift
//  denticom
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct JrnlCmd {
    public let id: Date
    public let cmd: String
    public let arg: String

    public init(id: Date,
                cmd: String,
                arg: String) {
        self.id = id
        self.cmd = cmd
        self.arg = arg
    }
}
