//
//  Event.swift
//  denticom
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public struct Event {
    public let id: String
    public let date: Date
    public let services: [String]
    public let costs: [String]

    public init(id: String,
                date: Date,
                services: [String],
                costs: [String]){
        self.id = id
        self.date = date
        self.services = services
        self.costs = costs
    }
}
