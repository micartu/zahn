//
//  OrgService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheOrgService {
    // common funcs
    func exists(org id: String) -> Bool

    // get
    func get(org: String) -> Org?

    // update
    func update(org: Org, completion: @escaping (() -> Void))

    // create
    func create(org: Org, completion: @escaping (() -> Void))

    // delete
    func delete(org id: String, completion: @escaping (() -> Void))
}
