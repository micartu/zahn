//
//  TaskClinicService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheClinicPersonTaskService {
    func tasks(for chair: Chair, dateStart: Date, dateEnd: Date) -> [Task]
    func attach(task: Task, to chair: Chair, to person: Person, forPid: String, completion: @escaping (() -> Void))
    func attach(task: Task, chairId: String, pid: String, forPid: String, completion: @escaping (() -> Void))
    func creatorOfTask(tid: String) -> String?
    func forWhomCreatedTask(tid: String) -> String?
    func detachFromChairTasks(tid: String, completion: @escaping (() -> Void))
    func detachAllTasksFrom(chairId: String, completion: @escaping (() -> Void))
}
