//
//  JrnlService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheJrnl {
    // get
    func getPendingJournalRecords(limit: Int) -> [JrnlCmd]

    // add/create
    func add(jrnl: JrnlCmd, completion: @escaping (() -> Void))
    func add(jrnl: [JrnlCmd], completion: @escaping (() -> Void))

    // delete
    func delete(jrnls: [JrnlCmd], completion: @escaping (() -> Void))
}
