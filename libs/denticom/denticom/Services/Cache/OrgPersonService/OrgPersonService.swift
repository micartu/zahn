//
//  OrgPersonService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheOrgPersonService {
    func organzations(for person: Person) -> [Org]
    func organzations(for pid: String) -> [Org]
    func attach(org: Org, to person: Person, completion: @escaping (() -> Void))
    func attach(org: Org, to pid: String, completion: @escaping (() -> Void))
    func detach(org: Org, to person: Person, completion: @escaping (() -> Void))
    func detach(org: Org, to pid: String, completion: @escaping (() -> Void))
    func detachFromOrgs(pid: String, completion: @escaping (() -> Void))
    func detachFromOrgs(person: Person, completion: @escaping (() -> Void))
}
