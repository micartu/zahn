//
//  ClinicChairService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheClinicChairService {
    func chairs(clinicId: String) -> [Chair]
    func attach(chair: Chair, to clinic: Clinic, completion: @escaping (() -> Void))
    func attach(chair: Chair, clinicId: String, completion: @escaping (() -> Void))
    func detachFromClinic(chairId: String, completion: @escaping (() -> Void))
    func detachAllChairsFrom(clinicId: String, completion: @escaping (() -> Void))
}
