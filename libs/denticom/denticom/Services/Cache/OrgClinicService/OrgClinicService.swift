//
//  OrgClinicService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheOrgClinicService {
    func organzations(clinic: Clinic) -> [Org]
    func organzations(cid: String) -> [Org]
    func attach(org: Org, clinic: Clinic, completion: @escaping (() -> Void))
    func attach(org: Org, cid: String, completion: @escaping (() -> Void))
    func detach(org: Org, clinic: Clinic, completion: @escaping (() -> Void))
    func detach(org: Org, cid: String, completion: @escaping (() -> Void))
    func detachFromOrgs(cid: String, completion: @escaping (() -> Void))
    func detachFromOrgs(clinic: Clinic, completion: @escaping (() -> Void))
}
