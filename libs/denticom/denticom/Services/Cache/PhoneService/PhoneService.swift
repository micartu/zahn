//
//  PhoneService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CachePhoneService {
    // common funcs
    func exists(phone id: String) -> Bool

    // get
    func get(phone: String) -> Phone?

    // update
    func update(phone: Phone, completion: @escaping (() -> Void))

    // create
    func create(phone: Phone, completion: @escaping (() -> Void))

    // delete
    func delete(phone id: String, completion: @escaping (() -> Void))
}
