//
//  ChairService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheChairService {
    // common funcs
    func exists(chair id: String) -> Bool

    // get
    func get(chair: String) -> Chair?

    // update
    func update(chair: Chair, completion: @escaping (() -> Void))

    // create
    func create(chair: Chair, completion: @escaping (() -> Void))

    // delete
    func delete(chair id: String, completion: @escaping (() -> Void))
}
