//
//  CostService.swift
//  denticom
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CacheCostService {
    // common funcs
    func exists(cost id: String) -> Bool

    // get
    func get(cost: String) -> Cost?

    // update
    func update(cost: Cost, completion: @escaping (() -> Void))

    // create
    func create(cost: Cost, completion: @escaping (() -> Void))

    // delete
    func delete(cost id: String, completion: @escaping (() -> Void))
}

public protocol CacheServiceCostService {
    func set(cost: Cost, to srv: Service, of ev: Event, completion: @escaping (() -> Void))
    func getCostOf(service s: Service, and ev: Event) -> Cost?
}
