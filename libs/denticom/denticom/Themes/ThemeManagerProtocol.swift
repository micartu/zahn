//
//  ThemeManagerProtocol.swift
//  zahn
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

// theme must be implemented on the interface level,
// here we've got only its declaration
public protocol ITheme { }

public protocol Themeble: class {
    func apply(theme: ITheme)
}
