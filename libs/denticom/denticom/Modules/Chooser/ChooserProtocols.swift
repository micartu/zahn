//
//  ChooserProtocols.swift
//  denticom
//
//  Created by Michael Artuerhof on 12.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import RxSwift

public protocol ChooserViewModel {
    var theme: ITheme! { get set }
    var updateData: Variable<Bool> { get }
    func getContents()
    func selected(indexPath: IndexPath)
    func model(for indexPath: IndexPath) -> Any
    func modelsCount(for section: Int) -> Int
    func title(for section: Int) -> String
    func sectionsCount() -> Int
}
