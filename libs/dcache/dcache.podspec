Pod::Spec.new do |s|
	s.platform = :ios
	s.ios.deployment_target = '9.3'
	s.name             = 'dcache'
	s.version          = '0.1.0'
	s.summary          = 'cache for denti'
	s.requires_arc 	   = true

	s.description      = <<-DESC
Cache library used for working with denti framework
	DESC

	s.homepage         = 'https://github.com/micartu/dcache'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'micartu' => 'michael.artuerhof@gmail.com' }
	s.source           = { :git => 'https://github.com/micartu/dcache.git', :tag => s.version.to_s }

	s.source_files = 'dcache/**/*.{swift}'
#	s.resource_bundles = {'dcache' => ['dcache/**/*.xcdatamodeld']}
	s.resources = 'dcache/DataModel/*.xcdatamodeld'
	s.frameworks = 'Foundation'
	s.dependency 'MagicalRecord', '2.3.2'
	s.dependency 'denticom', '~> 0.1.0'
	s.swift_version = "4.2"
end
