//
//  dcacheClinic.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 03.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheClinic: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testClinicCreation() {
        let exp = expectation(description: "Clinic + Person Creation")
        let kCTestId = "_test_clinic_"
        let roleCount = 10
        let ratingCount = 100
        let ratingPId = "rating_"
        let rolePId = "role_"
        let c = Clinic(id: kCTestId,
                       name: kCTestId,
                       address: "none")
        createPerson(cache: cache) {
            self.cache.create(clinic: c) {
                let p = self.cache.get(person: kTestUser)!
                let g = DispatchGroup()
                for i in 0..<roleCount {
                    let r = genRole(id: rolePId + "\(i)")
                    g.enter()
                    self.cache.add(role: r, for: p, in: kCTestId) {
                        g.leave()
                    }
                }
                var rr = [Rating]()
                for i in 0..<ratingCount {
                    let r = genRating(id: ratingPId + "\(i)")
                    rr.append(r)
                }
                g.enter()
                self.cache.add(ratings: rr, to: kCTestId) {
                    g.leave()
                }
                g.notify(queue: .main) {
                    let rr = self.cache.ratingsFor(clinic: c, batchSize: ratingCount, step: 0)
                    XCTAssertTrue(rr.count == ratingCount)
                    let rid = Int.random(in: 0..<roleCount)
                    let role = genRole(id: rolePId + "\(rid)")
                    XCTAssertTrue(self.cache.exists(role: role, for: p, in: c))
                    self.cache.delete(clinic: kCTestId) {
                        self.cache.delete(person: kTestUser) {
                            XCTAssertFalse(self.cache.exists(role: role, for: p, in: c))
                            let rr = self.cache.get(rating: ratingPId + "\(rid)")
                            XCTAssertTrue(rr == nil, "but it was \(String(describing: rr))")
                            let cc = self.cache.get(clinic: kCTestId)
                            XCTAssertTrue(cc == nil, "but it was \(String(describing: cc))")
                            let pp = self.cache.get(person: kTestUser)
                            XCTAssertTrue(pp == nil, "but it was \(String(describing: pp))")
                            exp.fulfill()
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}

// MARK: - Helpers

func genRole(id: String) -> Role {
    return Role(id: id,
                name: "role_\(id)", right: id)
}
