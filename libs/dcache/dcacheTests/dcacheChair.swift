//
//  dcacheChair.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheChair: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testAttachChairToClinic() {
        let exp = expectation(description: "Clinic + Chair Creation + Attach of Chair")
        let kCTestId = "_test_key_id_"
        let chair = Chair(id: kCTestId,
                          name: kCTestId,
                          descr: kCTestId)
        let c = Clinic(id: kCTestId,
                       name: kCTestId,
                       address: "none")
        cache.attach(chair: chair, to: c) {
            let cha = self.cache.get(chair: kCTestId)
            XCTAssert(cha?.id == chair.id, "but it was: \(String(describing: cha))")
            let chairs = self.cache.chairs(clinicId: c.id)
            XCTAssert(chairs.count == 1)
            self.cache.detachAllChairsFrom(clinicId: c.id) {
                let chairs = self.cache.chairs(clinicId: c.id)
                XCTAssert(chairs.count == 0)
                self.cache.delete(clinic: c.id) {
                    self.cache.delete(chair: chair.id) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
