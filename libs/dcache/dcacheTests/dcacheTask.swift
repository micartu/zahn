//
//  dcacheTask.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheTask: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testTaskChairCreation() {
        let exp = expectation(description: "Person + Task + Chair Creation")
        let kCTestId = "_test_task_id_"
        let date = Date()
        let kDuration: Double = 2.5
        let t = Task(id: kCTestId,
                     name: kCTestId,
                     time: date,
                     duration: kDuration,
                     descr: kCTestId)
        let chair = Chair(id: kCTestId,
                          name: kCTestId,
                          descr: kCTestId)
        createPerson(cache: cache) {
            let p = self.cache.get(person: kTestUser)!
            self.cache.attach(task: t, to: chair, to: p, forPid: p.id) {
                let c = self.cache.get(chair: kCTestId)
                XCTAssert(c?.id == kCTestId, "but it was: \(String(describing: c))")
                let t = self.cache.get(task: kCTestId)
                XCTAssert(t?.id == kCTestId, "but it was: \(String(describing: t))")
                // check if we'll return attached tasks based on time
                var end = Date(timeInterval: kDuration * 60 * 60, since: date)
                var tasks = self.cache.tasks(for: chair, dateStart: date, dateEnd: end)
                XCTAssert(tasks.count == 1, "but it was: \(tasks)")
                end = Date(timeInterval: (kDuration - 1) * 60 * 60, since: date)
                tasks = self.cache.tasks(for: chair, dateStart: date, dateEnd: end)
                XCTAssert(tasks.count == 1, "but it was: \(tasks)")
                // date is too old, should get nothing:
                let dateOld = Date(timeInterval: -(kDuration + 1) * 60 * 60, since: date)
                end = Date(timeInterval: -kDuration * 60 * 60, since: date)
                tasks = self.cache.tasks(for: chair, dateStart: dateOld, dateEnd: end)
                XCTAssert(tasks.count == 0, "but it was: \(tasks)")
                self.cache.delete(chair: kCTestId) {
                    let c = self.cache.get(chair: kCTestId)
                    let t = self.cache.get(task: kCTestId)
                    XCTAssert(c == nil, "but it was: \(String(describing: c))")
                    XCTAssert(t == nil, "but it was: \(String(describing: t))")
                    self.cache.delete(person: kTestUser) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
