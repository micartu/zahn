//
//  dcacheTests.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

let kTimeout: TimeInterval = 5
let kTestUser = "test_user_login"
let kTPersonPhoneId = "_personal_phone_id_"

func createPerson(cache: CacheService, completion: @escaping (() -> Void)) {
    let phone = Phone(id: kTPersonPhoneId, number: "+7(123)456-78-99")
    let p = Person(id: kTestUser,
                   login: kTestUser,
                   name: "john",
                   midname: "vas",
                   surname: "smith",
                   phones: [kTPersonPhoneId],
                   address: "none")
    cache.create(person: p) {
        cache.create(phone: phone) {
            completion()
        }
    }
}


func cleanUpPerson(cache: CacheService, completion: @escaping (() -> Void)) {
    cache.delete(person: kTestUser) {
        cache.delete(phone: kTPersonPhoneId) {
            completion()
        }
    }
}

class dcacheTests: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testPersonCreation() {
        let exp = self.expectation(description: "Person creation")
        createPerson(cache: cache) { [weak self] in
            let p = self?.cache.get(person: kTestUser)
            XCTAssertTrue(p != nil)
            XCTAssertTrue(p?.id == kTestUser,
                          "but person was: \(String(describing: p))")
            cleanUpPerson(cache: self!.cache) {
                XCTAssertTrue(self?.cache.get(phone: kTPersonPhoneId) == nil)
                XCTAssertTrue(self?.cache.get(person: kTestUser) == nil)
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
