//
//  dcacheJrnl.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheJrnl: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testJrnlCreation() {
        let exp = self.expectation(description: "Journal Creation")
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = NSTimeZone.local
        let cmd1 = "test1"
        let cmd2 = "test2"
        let cmd3 = "test3"
        let j1 = JrnlCmd(id: df.date(from: "2018-12-12 10:10:11")!, cmd: cmd1, arg: "v1|")
        let j2 = JrnlCmd(id: df.date(from: "2018-12-12 10:10:12")!, cmd: cmd2, arg: "v1|")
        let j3 = JrnlCmd(id: df.date(from: "2018-12-12 10:10:13")!, cmd: cmd3, arg: "v1|")
        let j4 = JrnlCmd(id: df.date(from: "2018-12-12 10:10:13")!, cmd: cmd3, arg: "v1|")
        let max = 10
        cache.add(jrnl: [j1, j2, j3, j4]) {
            let jrnls = self.cache.getPendingJournalRecords(limit: max)
            XCTAssertTrue(jrnls.count == 4, "but it was: \(jrnls.count)")
            let jj1 = jrnls.first!
            XCTAssertTrue(jj1.cmd == cmd1, "but it was: \(jj1)")
            let jj2 = jrnls[1]
            XCTAssertTrue(jj2.cmd == cmd2, "but it was: \(jj2)")
            self.cache.delete(jrnls: [j4]) {
                let jrnls = self.cache.getPendingJournalRecords(limit: max)
                XCTAssertTrue(jrnls.count == 0, "but it was: \(jrnls.count)")
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
