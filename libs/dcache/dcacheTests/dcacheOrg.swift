//
//  dcacheOrg.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheOrgTests: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testOrgPersonCreation() {
        let exp = self.expectation(description: "Org + Person Creation")
        let oid = "1"
        let kNote = "test"
        let org = Org(id: oid, name: kNote, notes: kNote, address: kNote)
        createPerson(cache: cache) {
            self.cache.attach(org: org, to: kTestUser) {
                var orgs = self.cache.organzations(for: kTestUser)
                XCTAssert(orgs.count == 1, "but orgs were: \(orgs)")
                self.cache.delete(person: kTestUser) {
                    orgs = self.cache.organzations(for: kTestUser)
                    XCTAssert(orgs.count == 0, "but orgs were: \(orgs)")
                    self.cache.delete(org: oid) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testOrgClinicCreation() {
        let exp = self.expectation(description: "Org + Clinic Creation")
        let oid = "1"
        let kNote = "test"
        let org = Org(id: oid, name: kNote, notes: kNote, address: kNote)
        let cli = Clinic(id: oid, name: kNote, address: kNote)
        cache.attach(org: org, clinic: cli) {
            if let c = self.cache.get(clinic: oid) {
                XCTAssertTrue(c.id == oid, "but it was: \(c)")
            } else {
                XCTAssertTrue(false)
            }
            var orgs = self.cache.organzations(cid: oid)
            XCTAssert(orgs.count == 1, "but orgs were: \(orgs)")
            self.cache.delete(clinic: oid) {
                orgs = self.cache.organzations(cid: oid)
                XCTAssert(orgs.count == 0, "but orgs were: \(orgs)")
                self.cache.delete(org: oid) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
