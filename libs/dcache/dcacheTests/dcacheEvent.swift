//
//  dcacheEvent.swift
//  dcacheTests
//
//  Created by Michael Artuerhof on 01.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import MagicalRecord
@testable import dcache

class dcacheEvent: XCTestCase {
    var cache: CacheService!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testEventPersonCreation() {
        let exp = self.expectation(description: "Event + Person Creation")
        let kTRatingId = "_test_rating_"
        let kTEventId = "_test_event_"
        let r = Rating(id: kTRatingId,
                       creatorId: kTestUser,
                       date: Date(),
                       stars: 5)
        let s = Service(id: kTEventId,
                        name: "test_service")
        let c = Cost(id: kTEventId,
                     currency: "rub",
                     cost: 100)
        let e = Event(id: kTEventId,
                      date: Date(),
                      services: [],
                      costs: [])
        let p = Person(id: kTestUser,
                       login: kTestUser,
                       name: kTestUser,
                       midname: kTestUser,
                       surname: kTestUser,
                       phones: [],
                       address: "")
        cache.add(services: [s],
                  costs: [c],
                  to: e) { [weak self] in
                    self?.cache.add(rating: r, srvId: kTEventId) {
                        self?.cache.addTakenEventsTo(person: p, events: [kTEventId]) {
                            if let ev = self?.cache.takenEventsFor(person: p, batchSize: 10, step: 0) {
                                XCTAssertTrue(ev.count == 1)
                                XCTAssertTrue(ev[0].services.count == 1)
                            }
                            if let p = self?.cache.get(person: kTestUser) {
                                XCTAssertTrue(p.id == kTestUser, "but user was: \(p)")
                                XCTAssertTrue(self?.cache.takenEventExistsFor(person: kTestUser, event: kTEventId) == true)
                            } else {
                                XCTAssertTrue(false)
                            }
                            if let cost = self?.cache.get(cost: kTEventId) {
                                XCTAssertTrue(c.id == cost.id && c.currency == cost.currency, "but cost was: \(c)")
                            } else {
                                XCTAssertTrue(false)
                            }
                            if let rt = self?.cache.get(rating: kTRatingId) {
                                XCTAssertTrue(rt.id == r.id && rt.stars == r.stars, "but rating was: \(r)")
                            } else {
                                XCTAssertTrue(false)
                            }
                            let kChangedCostId = "_changed_cost_id_"
                            let kChangedCur = "us"
                            let changedCost = Cost(id: kChangedCostId,
                                                   currency: kChangedCur,
                                                   cost: 1)
                            self?.cache.set(cost: changedCost, to: s, of: e) {
                                XCTAssertTrue(self?.cache.get(cost: kTEventId) == nil)
                                let cc = self?.cache.get(cost: kChangedCostId)
                                XCTAssertTrue(cc != nil)
                                XCTAssertTrue(cc?.currency == kChangedCur)
                                let gcc = self?.cache.getCostOf(service: s, and: e)
                                XCTAssertTrue(cc?.id == gcc?.id)
                                self?.cache.delete(person: kTestUser) {
                                    XCTAssertTrue(self?.cache.takenEventExistsFor(person: kTestUser, event: kTEventId) == false)
                                    XCTAssertTrue(self?.cache.get(person: kTestUser) == nil)
                                    XCTAssertTrue(self?.cache.get(cost: kTEventId) == nil)
                                    XCTAssertTrue(self?.cache.get(event: kTEventId) == nil)
                                    XCTAssertTrue(self?.cache.get(rating: kTRatingId) == nil)
                                    exp.fulfill()
                                }
                            }
                        }
                    }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testPersonCreationWithALotOfEventsAmount() {
        let exp = expectation(description: "A lot of Events + Person Creation")
        let kTSrvId = "_test_service_"
        let kTEventId = "_test_event_"
        let kGenEvAmount = 10
        let kGenSrvAmount = 10
        createPerson(cache: cache) { [weak self] in
            guard let `self` = self else { return }
            let per = self.cache.get(person: kTestUser)
            var evIds = [String]()
            let g = DispatchGroup()
            for i in 0..<kGenEvAmount {
                let ev = genEvent(id: kTEventId, idPostFix: "\(i)")
                var srvs = [Service]()
                var csts = [Cost]()
                for i in 0..<kGenSrvAmount {
                    let (s, c) = genService(id: kTSrvId + kTEventId,
                                            idPostFix: "\(i)", cost: Double(i + 1) * 10)
                    srvs.append(s)
                    csts.append(c)
                }
                let r = genRating(id: kTSrvId + kTEventId,
                                  idPostFix: "\(i)", stars: i % 5)
                g.enter()
                self.cache.add(services: srvs, costs: csts, to: ev) {
                    self.cache.add(rating: r, evntId: ev.id) {
                        g.leave()
                    }
                }
                evIds.append(ev.id)
            }
            XCTAssertNotNil(per)
            g.notify(queue: .main) {
                self.cache.addMadenEventsTo(person: per!, events: evIds) {
                    let i = Int.random(in: 0..<kGenEvAmount)
                    let eid = kTEventId + "\(i)"
                    let e = self.cache.get(event: eid)
                    XCTAssertNotNil(e)
                    XCTAssertTrue(e?.services.count == kGenSrvAmount,
                                  "but it was: \(String(describing: e))")
                    let rts = self.cache.ratings(evntId: eid)
                    XCTAssertTrue(rts.count == 1, "but it was: \(rts)")
                    // test of batch operations
                    let kBatchSz = 3
                    var events = self.cache.madenEventsFor(person: per!, batchSize: kBatchSz, step: 0)
                    XCTAssertTrue(events.count == kBatchSz, "but it was: \(events)")
                    if let ev0 = events.first {
                        XCTAssertTrue(ev0.id == kTEventId + "\(kGenEvAmount - 1)", "but it was: \(ev0)")
                    }
                    let id2comp: Int
                    if let ev = events.last {
                        let eid = ev.id
                        let i = eid.index(eid.startIndex, offsetBy: kTEventId.count)
                        id2comp = String(eid[i...]).toInt
                    } else {
                        id2comp = 0
                    }
                    // test of offset to the batch operation
                    events = self.cache.madenEventsFor(person: per!, batchSize: kBatchSz, step: 1)
                    if let ev0 = events.first {
                        XCTAssertTrue(ev0.id == kTEventId + "\(id2comp - 1)", "but it was: \(ev0)")
                    }
                    self.cache.delete(person: kTestUser) {
                        let i = Int.random(in: 0..<kGenEvAmount)
                        let eid = kTEventId + "\(i)"
                        let e = self.cache.get(event: eid)
                        XCTAssertTrue(e == nil, "but it was: \(String(describing: e))")
                        XCTAssertTrue(self.cache.get(service:  kTSrvId + eid) == nil)
                        let p = self.cache.get(person: kTestUser)
                        XCTAssertTrue(p == nil, "but it was: \(String(describing: p))")
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}

// MARK: - Helpers

func genEvent(id: String, idPostFix: String = "") -> Event {
    let e = Event(id: id + idPostFix,
                  date: Date(),
                  services: [],
                  costs: [])
    return e
}

func genService(id: String, idPostFix: String = "", cost: Double = 100) -> (Service, Cost) {
    let s = Service(id: id + idPostFix,
                    name: "test_service_\(id)_\(idPostFix)")
    let c = Cost(id: id + idPostFix,
                 currency: "rub",
                 cost: cost)
    return (s, c)
}

func genRating(id: String, idPostFix: String = "", stars: Int = 5) -> Rating {
    return Rating(id: id + idPostFix,
                  creatorId: kTestUser,
                  date: Date(),
                  stars: stars)
}
