//
//  CPhone+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CPhone {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPhone> {
        return NSFetchRequest<CPhone>(entityName: "CPhone")
    }

    @NSManaged public var id: String?
    @NSManaged public var number: String?

}
