//
//  CChair+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CChair {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CChair> {
        return NSFetchRequest<CChair>(entityName: "CChair")
    }

    @NSManaged public var descr: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var clinic: CClinic?

}
