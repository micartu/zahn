//
//  CService+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 03.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CService {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CService> {
        return NSFetchRequest<CService>(entityName: "CService")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var ratings: String?

}
