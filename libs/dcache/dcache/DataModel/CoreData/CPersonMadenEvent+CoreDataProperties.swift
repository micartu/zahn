//
//  CPersonMadenEvent+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 30.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CPersonMadenEvent {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPersonMadenEvent> {
        return NSFetchRequest<CPersonMadenEvent>(entityName: "CPersonMadenEvent")
    }

    @NSManaged public var personId: String?
    @NSManaged public var eventId: String?
    @NSManaged public var date: NSDate?

}
