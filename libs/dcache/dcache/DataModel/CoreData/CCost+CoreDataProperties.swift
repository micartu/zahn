//
//  CCost+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CCost {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CCost> {
        return NSFetchRequest<CCost>(entityName: "CCost")
    }

    @NSManaged public var id: String?
    @NSManaged public var cost: Double
    @NSManaged public var currency: String?

}
