//
//  CClinicPersonRole+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CClinicPersonRole {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CClinicPersonRole> {
        return NSFetchRequest<CClinicPersonRole>(entityName: "CClinicPersonRole")
    }

    @NSManaged public var cid: String?
    @NSManaged public var pid: String?
    @NSManaged public var rid: String?

}
