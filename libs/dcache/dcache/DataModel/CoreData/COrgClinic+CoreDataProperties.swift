//
//  COrgClinic+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension COrgClinic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<COrgClinic> {
        return NSFetchRequest<COrgClinic>(entityName: "COrgClinic")
    }

    @NSManaged public var oid: Int64
    @NSManaged public var cid: String?

}
