//
//  COrg+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension COrg {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<COrg> {
        return NSFetchRequest<COrg>(entityName: "COrg")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: Int64
    @NSManaged public var notes: String?
    @NSManaged public var address: String?

}
