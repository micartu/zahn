//
//  CRating+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CRating {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CRating> {
        return NSFetchRequest<CRating>(entityName: "CRating")
    }

    @NSManaged public var creatorId: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var stars: Int16
    @NSManaged public var used: Int32

}
