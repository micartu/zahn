//
//  CRole+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CRole {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CRole> {
        return NSFetchRequest<CRole>(entityName: "CRole")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var right: String?

}
