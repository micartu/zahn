//
//  CPersonRating+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CPersonRating {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPersonRating> {
        return NSFetchRequest<CPersonRating>(entityName: "CPersonRating")
    }

    @NSManaged public var personId: String?
    @NSManaged public var ratingId: String?
    @NSManaged public var date: NSDate?

}
