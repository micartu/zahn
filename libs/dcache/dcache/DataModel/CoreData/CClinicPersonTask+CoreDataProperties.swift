//
//  CClinicPersonTask+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CClinicPersonTask {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CClinicPersonTask> {
        return NSFetchRequest<CClinicPersonTask>(entityName: "CClinicPersonTask")
    }

    @NSManaged public var chairId: String?
    @NSManaged public var pid: String?
    @NSManaged public var forPid: String?
    @NSManaged public var task: CTask?

}
