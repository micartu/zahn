//
//  COrgPerson+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension COrgPerson {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<COrgPerson> {
        return NSFetchRequest<COrgPerson>(entityName: "COrgPerson")
    }

    @NSManaged public var pid: String?
    @NSManaged public var oid: Int64

}
