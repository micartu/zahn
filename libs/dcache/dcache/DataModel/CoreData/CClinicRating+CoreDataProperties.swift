//
//  CClinicRating+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 30.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CClinicRating {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CClinicRating> {
        return NSFetchRequest<CClinicRating>(entityName: "CClinicRating")
    }

    @NSManaged public var clinicId: String?
    @NSManaged public var ratingId: String?
    @NSManaged public var date: NSDate?

}
