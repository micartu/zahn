//
//  CClinicEvent+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 30.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CClinicEvent {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CClinicEvent> {
        return NSFetchRequest<CClinicEvent>(entityName: "CClinicEvent")
    }

    @NSManaged public var clinicId: String?
    @NSManaged public var eventId: String?
    @NSManaged public var date: NSDate?

}
