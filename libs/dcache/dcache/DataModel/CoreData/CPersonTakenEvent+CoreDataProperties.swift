//
//  CPersonTakenEvent+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 30.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CPersonTakenEvent {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPersonTakenEvent> {
        return NSFetchRequest<CPersonTakenEvent>(entityName: "CPersonTakenEvent")
    }

    @NSManaged public var personId: String?
    @NSManaged public var eventId: String?
    @NSManaged public var date: NSDate?

}
