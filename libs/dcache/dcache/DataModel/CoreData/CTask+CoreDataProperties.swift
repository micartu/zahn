//
//  CTask+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CTask {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CTask> {
        return NSFetchRequest<CTask>(entityName: "CTask")
    }

    @NSManaged public var descr: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var time: NSDate?
    @NSManaged public var end: NSDate?
    @NSManaged public var clinicPerson: CClinicPersonTask?

}
