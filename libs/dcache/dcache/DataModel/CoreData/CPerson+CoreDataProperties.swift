//
//  CPerson+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CPerson {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPerson> {
        return NSFetchRequest<CPerson>(entityName: "CPerson")
    }

    @NSManaged public var address: String?
    @NSManaged public var id: String?
    @NSManaged public var login: String?
    @NSManaged public var middleName: String?
    @NSManaged public var name: String?
    @NSManaged public var phones: String?
    @NSManaged public var surname: String?
    @NSManaged public var color: CColor?

}
