//
//  CEvent+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 03.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CEvent {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CEvent> {
        return NSFetchRequest<CEvent>(entityName: "CEvent")
    }

    @NSManaged public var costs: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var services: String?
    @NSManaged public var used: Int32
    @NSManaged public var ratings: String?

}
