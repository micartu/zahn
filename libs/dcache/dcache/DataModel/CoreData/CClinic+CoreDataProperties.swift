//
//  CClinic+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CClinic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CClinic> {
        return NSFetchRequest<CClinic>(entityName: "CClinic")
    }

    @NSManaged public var address: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var chairs: NSSet?

}

// MARK: Generated accessors for chairs
extension CClinic {

    @objc(addChairsObject:)
    @NSManaged public func addToChairs(_ value: CChair)

    @objc(removeChairsObject:)
    @NSManaged public func removeFromChairs(_ value: CChair)

    @objc(addChairs:)
    @NSManaged public func addToChairs(_ values: NSSet)

    @objc(removeChairs:)
    @NSManaged public func removeFromChairs(_ values: NSSet)

}
