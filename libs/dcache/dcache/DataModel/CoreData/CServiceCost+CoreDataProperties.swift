//
//  CServiceCost+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CServiceCost {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CServiceCost> {
        return NSFetchRequest<CServiceCost>(entityName: "CServiceCost")
    }

    @NSManaged public var serviceId: String?
    @NSManaged public var costId: String?
    @NSManaged public var eventId: String?

}
