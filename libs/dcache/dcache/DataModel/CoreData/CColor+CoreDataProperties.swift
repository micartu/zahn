//
//  CColor+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CColor {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CColor> {
        return NSFetchRequest<CColor>(entityName: "CColor")
    }

    @NSManaged public var a: Int16
    @NSManaged public var b: Int16
    @NSManaged public var g: Int16
    @NSManaged public var r: Int16
    @NSManaged public var persons: NSSet?

}

// MARK: Generated accessors for persons
extension CColor {

    @objc(addPersonsObject:)
    @NSManaged public func addToPersons(_ value: CPerson)

    @objc(removePersonsObject:)
    @NSManaged public func removeFromPersons(_ value: CPerson)

    @objc(addPersons:)
    @NSManaged public func addToPersons(_ values: NSSet)

    @objc(removePersons:)
    @NSManaged public func removeFromPersons(_ values: NSSet)

}
