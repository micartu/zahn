//
//  CJrnlCmd+CoreDataProperties.swift
//  dcache
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CJrnlCmd {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CJrnlCmd> {
        return NSFetchRequest<CJrnlCmd>(entityName: "CJrnlCmd")
    }

    @NSManaged public var cmd: String?
    @NSManaged public var arg: String?
    @NSManaged public var date: NSDate?

}
