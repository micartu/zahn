//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheClinicService {
    public func exists(clinic id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CClinic.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    public func exists(role: Role, for p: Person, in clinic: Clinic) -> Bool {
        let predicate = NSPredicate(format: "(cid == %@) AND (pid == %@) AND (rid == %@)",
                                    clinic.id, p.id, role.id)
        if CClinicPersonRole.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    public func exists(event e: Event, in clinic: Clinic) -> Bool {
        let predicate = NSPredicate(format: "(clinicId == %@) AND (eventId == %@)",
                                    clinic.id, e.id)
        if CClinicEvent.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(clinic: String) -> Clinic? {
        return _get(clinic: clinic)
    }

    public func ratingsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Rating] {
        return _ratingsFor(clinic: clinic, batchSize: batchSize, step: step)
    }

    public func eventsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Event] {
        return _eventsFor(clinic: clinic, batchSize: batchSize, step: step)
    }

    public func clinicsFor(personId: String) -> [Clinic] {
        return _clinicsFor(personId: personId)
    }

    public func update(clinic: Clinic, completion: @escaping (() -> Void)) {
        _update(clinic: clinic, completion: completion)
    }

    public func add(ratings: [Rating], to clinic: String, completion: @escaping (() -> Void)) {
        _add(ratings: ratings, to: clinic, completion: completion)
    }

    public func add(role: Role, for p: Person, in clinic: String, completion: @escaping (() -> Void)) {
        _add(role: role, for: p, to: clinic, completion: completion)
    }

    public func create(clinic: Clinic, completion: @escaping (() -> Void)) {
        _create(clinic: clinic, completion: completion)
    }

    public func delete(clinic c: String, completion: @escaping (() -> Void)) {
        _delete(clinic: c, completion: completion)
    }

    public func delete(ratings: [Rating], in clinic: String, completion: @escaping (() -> Void)) {
        _delete(ratings: ratings, in: clinic, completion: completion)
    }

    public func delete(events: [Event], in clinic: String, completion: @escaping (() -> Void)) {
        _delete(events: events, in: clinic, completion: completion)
    }

    public func delete(role: Role, for p: Person, in clinic: String, completion: @escaping (() -> Void)) {
        _delete(role: role, for: p, in: clinic, completion: completion)
    }
}
