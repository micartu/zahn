//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(clinic: String) -> Clinic? {
        if let c = getClinic(clinic) {
            return convert(clinic: c)
        }
        return nil
    }

    func _ratingsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Rating] {
        var out = [Rating]()
        if let ratings = ratingsFor(clinic: clinic.id,
                                    batchSize: batchSize,
                                    step: step, in: NSManagedObjectContext.mr_default()) {
            for rr in ratings {
                if let cr = getRating(rr.ratingId!) {
                    let r = convert(rating: cr)
                    out.append(r)
                }
            }
        }
        return out
    }

    func _eventsFor(clinic: Clinic, batchSize: Int, step: Int) -> [Event] {
        var out = [Event]()
        if let events = eventsFor(clinic: clinic.id,
                                  batchSize: batchSize,
                                  step: step, in: NSManagedObjectContext.mr_default()) {
            for re in events {
                if let ce = getEvent(re.eventId!) {
                    let e = convert(event: ce)
                    out.append(e)
                }
            }
        }
        return out
    }

    func _clinicsFor(personId: String) -> [Clinic] {
        var out = [Clinic]()
        let predicate = NSPredicate(format: "pid == %@", personId)
        if let cliRoles = CClinicPersonRole.mr_findAllSorted(by: "cid", ascending: false, with: predicate) as? [CClinicPersonRole] {
            for cc in cliRoles {
                if let c = get(clinic: cc.cid!) {
                    out.append(c)
                }
            }
        }
        return out
    }

    // MARK: - internal tools

    internal func ratingsFor(clinic id: String,
                             batchSize: Int,
                             step: Int,
                             in context: NSManagedObjectContext) -> [CClinicRating]? {
        let predicate = NSPredicate(format: "clinicId == %@", id)
        let request = CClinicRating.mr_requestAllSorted(by: "date", ascending: false, with: predicate, in: context)
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CClinicRating.mr_executeFetchRequest(request) as? [CClinicRating]
    }

    internal func eventsFor(clinic id: String,
                            batchSize: Int,
                            step: Int,
                            in context: NSManagedObjectContext) -> [CClinicEvent]? {
        let predicate = NSPredicate(format: "clinicId == %@", id)
        let request = CClinicEvent.mr_requestAllSorted(by: "date", ascending: false, with: predicate, in: context)
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CClinicEvent.mr_executeFetchRequest(request) as? [CClinicEvent]
    }

    // MARK: - Private

    internal func getClinic(_ id: String, in context: NSManagedObjectContext) -> CClinic? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CClinic.mr_findFirst(with: predicate, in: context)
    }

    internal func getClinic(_ id: String) -> CClinic? {
        return getClinic(id, in: NSManagedObjectContext.mr_default())
    }
}
