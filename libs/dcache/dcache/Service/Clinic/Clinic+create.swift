//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] context in
            self?.create(clinic: clinic, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _add(ratings: [Rating], to clinic: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for r in ratings {
                // try to find rating in the PersonRating table:
                let predicate = NSPredicate(format: "(ratingId == %@) AND (clinicId == %@)", r.id, clinic)
                if CClinicRating.mr_countOfEntities(with: predicate, in: context) == 0 {
                    let newR = self.create(rating: r, in: context)
                    let bind = CClinicRating.mr_createEntity(in: context)!
                    bind.date = newR.date
                    bind.clinicId = clinic
                    bind.ratingId = r.id
                } else {
                    // oh... it's there! update it?
                    if let oldR = self.getRating(r.id, in: context) {
                        self.update(rating: oldR, from: r)
                    }
                }
            }
            }, completion: { success, err in
                completion()
        })
    }

    func _add(role: Role, for p: Person, to clinic: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            let predicate = NSPredicate(format: "(rid == %@) AND (cid == %@) AND (pid == %@)",
                                        role.id, clinic, p.id)
            if CClinicPersonRole.mr_countOfEntities(with: predicate, in: context) == 0 {
                let rp = CClinicPersonRole.mr_createEntity(in: context)!
                rp.cid = clinic
                rp.pid = p.id
                rp.rid = role.id

                // create role if it's not there
                if !self.exists(role: role.id) {
                    self.create(role: role, in: context)
                }

                // create person if it's not there
                if !self.exists(person: p.id) {
                    self.create(person: p, in: context)
                }
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(clinic: Clinic, in context: NSManagedObjectContext) -> CClinic {
        let c = CClinic.mr_createEntity(in: context)!
        update(clinic: c, from: clinic)
        return c
    }

    @discardableResult
    internal func createOrUpdate(clinic: Clinic, in context: NSManagedObjectContext) -> CClinic {
        let c: CClinic
        if let _c = getClinic(clinic.id) {
            c = _c
        } else {
            let _c = CClinic.mr_createEntity(in: context)!
            c = _c
        }
        update(clinic: c, from: clinic)
        return c
    }
}
