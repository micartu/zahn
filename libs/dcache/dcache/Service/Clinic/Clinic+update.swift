//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let c = self.getClinic(clinic.id, in: context) {
                self.update(clinic: c, from: clinic)
            } else {
                self.create(clinic: clinic, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    internal func update(clinic cc: CClinic, from c: Clinic) {
        cc.id = c.id
        cc.name = c.name
        cc.address = c.address
    }
}
