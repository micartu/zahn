//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(clinic id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let cc = self.getClinic(id, in: context) {
                self.delete(clinic: cc, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    func _delete(ratings: [Rating], in clinic: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(ratings: ratings, in: clinic, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _delete(events: [Event], in clinic: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(events: events, in: clinic, in: context)
        }, completion: { success, err in
            completion()
        })
    }

    func _delete(role: Role, for p: Person, in clinic: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            let predicate = NSPredicate(format: "(cid == %@) AND (rid == %@) AND (pid == %@)",
                                        clinic, role.id, p.id)
            let cr = CClinicPersonRole.mr_findFirst(with: predicate, in: context)
            cr?.mr_deleteEntity(in: context)
        }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Internal

    internal func delete(ratings: [Rating], in clinic: String, in context: NSManagedObjectContext) {
        for r in ratings {
            let predicate = NSPredicate(format: "(ratingId == %@) AND (clinicId == %@)",
                                        r.id, clinic)
            if let ccr = CClinicRating.mr_findFirst(with: predicate, in: context) {
                decrementUsageAndDelete(rating: r.id, in: context)
                ccr.mr_deleteEntity(in: context)
            }
        }
    }

    internal func delete(events: [Event], in clinic: String, in context: NSManagedObjectContext) {
        for e in events {
            let predicate = NSPredicate(format: "(eventId == %@) AND (clinicId == %@)",
                                        e.id, clinic)
            if let cce = CClinicEvent.mr_findFirst(with: predicate, in: context) {
                decrementUsageAndDelete(event: e.id, in: context)
                cce.mr_deleteEntity(in: context)
            }
        }
    }

    internal func delete(role: Role, for p: Person, in clinic: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(cid == %@) AND (rid == %@) AND (pid == %@)",
                                    clinic, role.id, p.id)
        if let cr = CClinicPersonRole.mr_findFirst(with: predicate, in: context) {
            if let pr = getRole(cr.rid!, in: context) {
                pr.mr_deleteEntity(in: context)
            }
            cr.mr_deleteEntity(in: context)
        }
    }

    internal func delete(clinic c: CClinic, in context: NSManagedObjectContext) {
        // I have to delete: 1) clinic; 2) its person-roles; 3) its ratings

        var predicate = NSPredicate(format: "clinicId == %@", c.id!)
        // delete ratings
        if let ratings = CClinicRating.mr_findAll(with: predicate, in: context) as? [CClinicRating] {
            for ccr in ratings {
                decrementUsageAndDelete(rating: ccr.ratingId!, in: context)
                ccr.mr_deleteEntity(in: context)
            }
        }

        // delete events of clinic
        if let events = CClinicEvent.mr_findAll(with: predicate, in: context) as? [CClinicEvent] {
            for cce in events {
                decrementUsageAndDelete(event: cce.eventId!, in: context)
                cce.mr_deleteEntity(in: context)
            }
        }

        // delete roles of users:
        predicate = NSPredicate(format: "cid == %@", c.id!)
        if let roles = CClinicPersonRole.mr_findAll(with: predicate, in: context) as? [CClinicPersonRole] {
            for ccr in roles {
                if let pr = getRole(ccr.rid!, in: context) {
                    pr.mr_deleteEntity(in: context)
                }
                ccr.mr_deleteEntity(in: context)
            }
        }

        detachFromOrgs(cid: c.id!, in: context)
        detachAllChairsFrom(clinicId: c.id!, in: context)

        c.mr_deleteEntity(in: context)
    }
}
