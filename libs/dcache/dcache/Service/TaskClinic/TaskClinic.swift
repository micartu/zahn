//
//  TaskClinic.swift
//  dcache
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheClinicPersonTaskService {
    public func tasks(for chair: Chair, dateStart: Date, dateEnd: Date) -> [Task] {
        let predicate = NSPredicate(format: "(chairId == %@) AND (task.time >= %@) AND (task.time <= %@)",
                                    chair.id, dateStart as NSDate, dateEnd as NSDate)
        var tasks = [Task]()
        if let cpts = CClinicPersonTask.mr_findAll(with: predicate) as? [CClinicPersonTask] {
            for ccpt in cpts {
                if let ctask = ccpt.task {
                    let task = convert(task: ctask)
                    tasks.append(task)
                }
            }
        }
        return tasks
    }

    public func attach(task: Task, to chair: Chair, to person: Person, forPid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if !self.exists(chair: chair.id) {
                self.create(chair: chair, in: context)
            }
            if !self.exists(person: person.login) {
                self.create(person: person, in: context)
            }
            self.attach(task: task, chairId: chair.id, pid: person.id, forPid: forPid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func attach(task: Task, chairId: String, pid: String, forPid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.attach(task: task, chairId: chairId, pid: pid, forPid: forPid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func creatorOfTask(tid: String) -> String? {
        let predicate = NSPredicate(format: "(task.id == %@)", tid)
        if let cpt = CClinicPersonTask.mr_findFirst(with: predicate, in: NSManagedObjectContext.mr_default()) {
            return cpt.pid
        }
        return nil
    }

    public func forWhomCreatedTask(tid: String) -> String? {
        let predicate = NSPredicate(format: "(task.id == %@)", tid)
        if let cpt = CClinicPersonTask.mr_findFirst(with: predicate, in: NSManagedObjectContext.mr_default()) {
            return cpt.forPid
        }
        return nil
    }

    public func detachFromChairTasks(tid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] context in
            let predicate = NSPredicate(format: "(task.id == %@)", tid)
            self?.detachAllTasksWith(predicate: predicate, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachAllTasksFrom(chairId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] context in
            self?.detachAllTasksFrom(chairId: chairId, in: context)
        }, completion: { success, err in
            completion()
        })
    }


    // MARK: - Private

    internal func detachAllTasksFrom(chairId: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(chairId == %@)", chairId)
        detachAllTasksWith(predicate: predicate, in: context)
    }

    internal func detachAllTasksWith(predicate: NSPredicate, in context: NSManagedObjectContext) {
        if let ccpts = CClinicPersonTask.mr_findAll(with: predicate, in: context) as? [CClinicPersonTask] {
            for ccpt in ccpts {
                if let t = ccpt.task {
                    t.mr_deleteEntity(in: context)
                }
                ccpt.mr_deleteEntity(in: context)
            }
        }
    }

    private func attach(task: Task, chairId: String, pid: String, forPid: String, in context: NSManagedObjectContext) {
        let ctask: CTask
        if !exists(task: task.id) {
            ctask = create(task: task, in: context)
        } else {
            ctask = getTask(task.id, in: context)!
            update(task: ctask, from: task)
        }
        let predicate = NSPredicate(format: "(task.id == %@)", task.id)
        if CClinicPersonTask.mr_countOfEntities(with: predicate, in: context) == 0 {
            let ct = CClinicPersonTask.mr_createEntity(in: context)!
            ct.chairId = chairId
            ct.pid = pid
            ct.forPid = forPid
            ct.task = ctask
        } else {
            let ct = CClinicPersonTask.mr_findFirst(with: predicate, in: context)
            ct?.task = ctask
        }
    }
}
