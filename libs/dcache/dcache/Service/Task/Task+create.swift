//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(task p: Task, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(task: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(task: Task, in context: NSManagedObjectContext) -> CTask {
        let p = CTask.mr_createEntity(in: context)!
        update(task: p, from: task)
        return p
    }
}
