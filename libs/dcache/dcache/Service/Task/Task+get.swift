//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(task id: String) -> Task? {
        if let p = getTask(id) {
            return convert(task: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getTask(_ id: String, in context: NSManagedObjectContext) -> CTask? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CTask.mr_findFirst(with: predicate, in: context)
    }

    internal func getTask(_ id: String) -> CTask? {
        return getTask(id, in: NSManagedObjectContext.mr_default())
    }
}
