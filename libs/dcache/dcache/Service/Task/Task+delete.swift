//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(task id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(task: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    internal func delete(task id: String, in context: NSManagedObjectContext) {
        if let p = getTask(id, in: context) {
            p.mr_deleteEntity(in: context)
        }
    }
}
