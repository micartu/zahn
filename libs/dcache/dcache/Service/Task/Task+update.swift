//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(task: Task, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getTask(task.id, in: context) {
                self.update(task: p, from: task)
            } else {
                self.create(task: task, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func createOrUpdateAndIncreaseUsage(task r: Task,
                                                 in context: NSManagedObjectContext) -> CTask {
        let cr: CTask
        if let oldR = getTask(r.id, in: context) {
            cr = oldR
            update(task: oldR, from: r)
        } else {
            cr = create(task: r, in: context)
        }
        return cr
    }

    internal func update(task cr: CTask, from r: Task) {
        cr.id = r.id
        cr.name = r.name
        cr.time = r.time as NSDate
        cr.end = NSDate(timeInterval: r.duration * TaskConst.kSecInHour, // duration in hours
                        since: r.time)
        cr.descr = r.descr
    }
}
