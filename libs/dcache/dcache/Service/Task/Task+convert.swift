//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(task r: CTask) -> Task {
        let duration = r.end!.timeIntervalSince(r.time! as Date)
        return Task(id: r.id!,
                    name: r.name!,
                    time: r.time! as Date,
                    duration: duration / TaskConst.kSecInHour, // duration in hours
                    descr: r.descr!)
    }
}
