//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheTaskService {
    public func exists(task id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CTask.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(task: String) -> Task? {
        return _get(task: task)
    }

    public func update(task: Task, completion: @escaping (() -> Void)) {
        _update(task: task, completion: completion)
    }

    public func create(task: Task, completion: @escaping (() -> Void)) {
        _create(task: task, completion: completion)
    }

    public func delete(task id: String, completion: @escaping (() -> Void)) {
        _delete(task: id, completion: completion)
    }
    
    internal struct TaskConst {
        static let kSecInHour: Double = 60 * 60
    }
}
