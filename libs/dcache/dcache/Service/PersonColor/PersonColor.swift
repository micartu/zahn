//
//  PersonColor.swift
//  dcache
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CachePersonColorService {
    public func set(color: IColor, for pid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            let predicate = NSPredicate(format: "(r == %ld) AND (g == %ld) AND (b == %ld) AND (a == %ld)",
                                        color.r, color.g, color.b, color.a)
            let ccolor: CColor
            if let cc = CColor.mr_findFirst(with: predicate, in: context) {
                ccolor = cc
            } else {
                ccolor = CColor.mr_createEntity(in: context)!
                self.update(color: ccolor, with: color)
            }
            if let p = self.getPerson(id: pid, in: context) {
                p.color = ccolor
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func getColor(for pid: String) -> IColor? {
        let predicate = NSPredicate(format: "(id == %@)", pid)
        if let p = CPerson.mr_findFirst(with: predicate) {
            if let c = p.color {
                return convert(color: c)
            }
        }
        return nil
    }

    // MARK: - Private
    internal func convert(color c: CColor) -> IColor {
        return IColor(r: Int(c.r),
                      g: Int(c.g),
                      b: Int(c.b),
                      a: Int(c.a))
    }

    internal func update(color cc: CColor, with c: IColor) {
        cc.r = Int16(c.r)
        cc.g = Int16(c.g)
        cc.b = Int16(c.b)
        cc.a = Int16(c.a)
    }
}
