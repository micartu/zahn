//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(person p: CPerson) -> Person {
        var phones = [String]()
        if let phs = p.phones {
            let pp = phs.split(separator: kDelimiter)
            for p in pp {
                phones.append(String(p))
            }
        }
        return Person(id: p.id!,
                      login: p.login!,
                      name: p.name!,
                      midname: p.middleName!,
                      surname: p.surname!,
                      phones: phones,
                      address: p.address!)
    }
}
