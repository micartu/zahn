//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(person login: String) -> Person? {
        if let p = getPerson(login) {
            return convert(person: p)
        }
        return nil
    }

    func _ratingsFor(person: Person, batchSize: Int, step: Int) -> [Rating] {
        var out = [Rating]()
        if let rait = ratingsFor(person: person.id,
                                 batchSize: batchSize,
                                 step: step,
                                 in: NSManagedObjectContext.mr_default()) {
            for r in rait {
                if let cr = getRating(r.ratingId!) {
                    let rr = convert(rating: cr)
                    out.append(rr)
                }
            }
        }
        return out
    }

    // MARK: - internal tools

    internal func ratingsFor(person id: String,
                             batchSize: Int,
                             step: Int,
                             in context: NSManagedObjectContext) -> [CPersonRating]? {
        let predicate = NSPredicate(format: "personId == %@", id)
        let request = CPersonRating.mr_requestAllSorted(by: "date", ascending: false, with: predicate, in: context)
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CPersonRating.mr_executeFetchRequest(request) as? [CPersonRating]
    }

    internal func getPerson(_ login: String, in context: NSManagedObjectContext) -> CPerson? {
        let predicate = NSPredicate(format: "login == %@", login)
        return CPerson.mr_findFirst(with: predicate, in: context)
    }

    internal func getPerson(_ login: String) -> CPerson? {
        return getPerson(login, in: NSManagedObjectContext.mr_default())
    }

    internal func getPerson(id: String, in context: NSManagedObjectContext) -> CPerson? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CPerson.mr_findFirst(with: predicate, in: context)
    }
}
