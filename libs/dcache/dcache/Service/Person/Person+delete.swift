//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(person id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            // try to remove ratings for the person if any
            self.deleteAllRatings(from: id, in: context)
            // ..and all connected events
            let predicate = NSPredicate(format: "(personId == %@)", id)
            if let evnts = CPersonMadenEvent.mr_findAll(with: predicate, in: context) as? [CPersonMadenEvent] {
                for cce in evnts {
                    self.deleteMadenEventFrom(person: id, event: cce.eventId!, in: context)
                }
            }
            if let evnts = CPersonTakenEvent.mr_findAll(with: predicate, in: context) as? [CPersonTakenEvent] {
                for cce in evnts {
                    self.deleteTakenEventFrom(person: id, event: cce.eventId!, in: context)
                }
            }
            // delete the person itself
            self.delete(person: id, in: context)
            self.detachFromOrgs(pid: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }


    func _delete(ratings: [Rating], from p: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for r in ratings {
                self.delete(rating: r.id, from: p.id, in: context)
            }
            }, completion: { success, err in
                completion()
        })

    }

    internal func deleteAllRatings(from person: String, in context: NSManagedObjectContext) {
        var step = 0
        var ratings: [CPersonRating]? = nil
        repeat {
            ratings = ratingsFor(person: person,
                                 batchSize: kDefBatchSize,
                                 step: step,
                                 in: context)
            if let rats = ratings {
                for r in rats {
                    delete(rating: r.ratingId!,
                           from: person, in: context)
                }
            }
            step += 1
        } while (ratings?.count == kDefBatchSize)
    }

    internal func delete(person id: String, in context: NSManagedObjectContext) {
        if let p = getPerson(id, in: context) {
            // remove phones if any
            if let phones = p.phones?.split(separator: self.kDelimiter) {
                for phId in phones {
                    if let pphone = getPhone(String(phId), in: context) {
                        pphone.mr_deleteEntity(in: context)
                    }
                }
            }
            p.mr_deleteEntity(in: context)
        }
    }

    internal func delete(rating: String, from person: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(ratingId == %@) AND (personId == %@)", rating, person)
        if let pr = CPersonRating.mr_findFirst(with: predicate, in: context) {
            decrementUsageAndDelete(rating: rating, in: context)
            pr.mr_deleteEntity(in: context)
        }
    }
}
