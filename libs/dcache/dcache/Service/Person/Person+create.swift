//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(person: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(person: person, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _add(ratings: [Rating], to p: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for r in ratings {
                // try to find rating in the PersonRating table:
                let predicate = NSPredicate(format: "(ratingId == %@) AND (personId == %@)", r.id, p.id)
                if CPersonRating.mr_countOfEntities(with: predicate, in: context) == 0 {
                    let bind = CPersonRating.mr_createEntity(in: context)!
                    let newR = self.createOrUpdateAndIncreaseUsage(rating: r, in: context)
                    bind.date = newR.date
                    bind.personId = p.id
                    bind.ratingId = r.id
                } else {
                    // oh... it's there! update it?
                    self.createOrUpdateAndIncreaseUsage(rating: r, in: context)
                }
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(person: Person, in context: NSManagedObjectContext) -> CPerson {
        let p = CPerson.mr_createEntity(in: context)!
        update(person: p, from: person)
        return p
    }
}
