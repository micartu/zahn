//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CachePersonService {
    public func exists(person login: String) -> Bool {
        let predicate = NSPredicate(format: "login == %@", login)
        if CPerson.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }
    
    public func get(person: String) -> Person? {
        return _get(person: person)
    }
    
    public func ratingsFor(person: Person, batchSize: Int, step: Int) -> [Rating] {
        return _ratingsFor(person: person, batchSize: batchSize, step: step)
    }
    
    public func update(person: Person, completion: @escaping (() -> Void)) {
        _update(person: person, completion: completion)
    }
    
    public func add(ratings: [Rating], to p: Person, completion: @escaping (() -> Void)) {
        _add(ratings: ratings, to: p, completion: completion)
    }
    
    public func create(person: Person, completion: @escaping (() -> Void)) {
        _create(person: person, completion: completion)
    }
    
    public func delete(person id: String, completion: @escaping (() -> Void)) {
        _delete(person: id, completion: completion)
    }
    
    public func delete(ratings: [Rating], from p: Person, completion: @escaping (() -> Void)) {
        _delete(ratings: ratings, from: p, completion: completion)
    }
}
