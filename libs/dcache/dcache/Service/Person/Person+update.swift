//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(person: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getPerson(person.login, in: context) {
                self.update(person: p, from: person)
            } else {
                self.create(person: person, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    internal func update(person cp: CPerson, from p: Person) {
        cp.id = p.id
        cp.login = p.login
        cp.name = p.name
        cp.middleName = p.midname
        cp.surname = p.surname
        cp.address = p.address
        cp.phones = p.phones.joined(separator: String(kDelimiter))
    }
}
