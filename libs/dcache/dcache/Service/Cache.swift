//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
public class Cache: CacheService {
    public init() {
    }

    // MARK: - Common Internals

    internal let kDelimiter: Character = "|"
    internal let kDefBatchSize = 100
}
