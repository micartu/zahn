//
//  Jrnl+common.swift
//  dcache
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache: CacheJrnl {
    public func getPendingJournalRecords(limit: Int) -> [JrnlCmd] {
        return _getPendingJournalRecords(limit: limit)
    }
    
    public func add(jrnl: JrnlCmd, completion: @escaping (() -> Void)) {
        _add(jrnl: jrnl, completion: completion)
    }
    
    public func add(jrnl: [JrnlCmd], completion: @escaping (() -> Void)) {
        _add(jrnl: jrnl, completion: completion)
    }
    
    public func delete(jrnls: [JrnlCmd], completion: @escaping (() -> Void)) {
        _delete(jrnls: jrnls, completion: completion)
    }
}
