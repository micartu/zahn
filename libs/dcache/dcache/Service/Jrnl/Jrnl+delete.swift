//
//  Jrnl+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(jrnls: [JrnlCmd], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            for j in jrnls {
                // remove all journal records with date older then the current one:
                let predicate = NSPredicate(format: "date >= %@", j.id as NSDate)
                if let cjnrls = CJrnlCmd.mr_findAll(with: predicate, in: context) as? [CJrnlCmd] {
                    for cj in cjnrls {
                        cj.mr_deleteEntity(in: context)
                    }
                }
            }
            }, completion: { success, err in
                completion()
        })
    }
}
