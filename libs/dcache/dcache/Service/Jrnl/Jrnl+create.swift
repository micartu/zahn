//
//  Jrnl+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _add(jrnl: JrnlCmd, completion: @escaping (() -> Void)) {
        _add(jrnl: [jrnl], completion: completion)
    }

    func _add(jrnl: [JrnlCmd], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for j in jrnl {
                self.create(jrnl: j, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Internal

    internal func create(jrnl: JrnlCmd, in context: NSManagedObjectContext) {
        let j = CJrnlCmd.mr_createEntity(in: context)!
        j.date = NSDate()
        j.cmd = jrnl.cmd
        j.arg = jrnl.arg
    }
}
