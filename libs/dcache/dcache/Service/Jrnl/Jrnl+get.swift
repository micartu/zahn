//
//  Jrnl+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 12.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _getPendingJournalRecords(limit: Int) -> [JrnlCmd] {
        var out = [JrnlCmd]()
        let request = CJrnlCmd.mr_requestAllSorted(by: "date", ascending: true)
        if limit > 0 {
            request.fetchLimit = limit
        }
        if let jrnls = CPersonRating.mr_executeFetchRequest(request) as? [CJrnlCmd] {
            for cj in jrnls {
                let j = JrnlCmd(id: cj.date! as Date,
                                cmd: cj.cmd!,
                                arg: cj.arg!)
                out.append(j)
            }
        }
        return out
    }
}
