//
//  ClinicChair.swift
//  dcache
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheClinicChairService {
    public func chairs(clinicId: String) -> [Chair] {
        var o = [Chair]()
        let predicate = NSPredicate(format: "(clinic.id == %@)", clinicId)
        if let cchairs = CChair.mr_findAllSorted(by: "id",
                                                 ascending: false,
                                                 with: predicate) as? [CChair] {
            for cchair in cchairs {
                let chair = convert(chair: cchair)
                o.append(chair)
            }
        }
        return o
    }

    public func attach(chair: Chair, to clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.createOrUpdate(clinic: clinic, in: context)
            self.attach(chair: chair, cid: clinic.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func attach(chair: Chair, clinicId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.attach(chair: chair, cid: clinicId, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachFromClinic(chairId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            let predicate = NSPredicate(format: "(id == %@)", chairId)
            if let cchair = CChair.mr_findFirst(with: predicate, in: context) {
                cchair.clinic?.removeFromChairs(cchair)
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func detachAllChairsFrom(clinicId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] context in
            self?.detachAllChairsFrom(clinicId: clinicId, in: context)
        }, completion: { success, err in
            completion()
        })
    }

    // MARK: - Private

    internal func attach(chair: Chair, cid: String, in context: NSManagedObjectContext) {
        let cchair: CChair
        if let _cchair = getChair(chair.id, in: context) {
            cchair = _cchair
        } else {
            cchair = create(chair: chair, in: context)
        }
        let predicate = NSPredicate(format: "(clinic.id == %@)", cid)
        if CChair.mr_countOfEntities(with: predicate, in: context) == 0 {
            let clinic = getClinic(cid, in: context)
            clinic?.addToChairs(cchair)
            cchair.clinic = clinic
        }
    }

    internal func detachAllChairsFrom(clinicId: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(id == %@)", clinicId)
        if let cclinic = CClinic.mr_findFirst(with: predicate, in: context) {
            if let cchairs = cclinic.chairs as? Set<CChair> {
                cclinic.removeFromChairs(cchairs as NSSet)
                for chair in cchairs {
                    chair.clinic = nil
                }
            }
        }
    }
}
