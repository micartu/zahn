//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(org p: COrg) -> Org {
        return Org(id: "\(p.id)",
                   name: p.name!,
                   notes: p.notes!,
                   address: p.address!)
    }
}
