//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(org id: String) -> Org? {
        if let p = getOrg(id) {
            return convert(org: p)
        }
        return nil
    }
    
    // MARK: - Internal
    
    internal func getOrg(_ id: String, in context: NSManagedObjectContext) -> COrg? {
        let predicate = NSPredicate(format: "id == %ld", id.toInt)
        return COrg.mr_findFirst(with: predicate, in: context)
    }
    
    internal func getOrg(_ id: String) -> COrg? {
        return getOrg(id, in: NSManagedObjectContext.mr_default())
    }
}
