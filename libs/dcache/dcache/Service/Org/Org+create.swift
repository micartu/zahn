//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(org p: Org, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(org: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }
    
    // MARK: - internal tools
    
    @discardableResult
    internal func create(org: Org, in context: NSManagedObjectContext) -> COrg {
        let p = COrg.mr_createEntity(in: context)!
        update(org: p, from: org)
        return p
    }
}
