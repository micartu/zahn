//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheOrgService {
    public func exists(org id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %ld", id.toInt)
        if COrg.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }
    
    // MARK: - Stubs
    
    public func get(org: String) -> Org? {
        return _get(org: org)
    }
    
    public func update(org: Org, completion: @escaping (() -> Void)) {
        _update(org: org, completion: completion)
    }
    
    public func create(org: Org, completion: @escaping (() -> Void)) {
        _create(org: org, completion: completion)
    }
    
    public func delete(org id: String, completion: @escaping (() -> Void)) {
        _delete(org: id, completion: completion)
    }
}
