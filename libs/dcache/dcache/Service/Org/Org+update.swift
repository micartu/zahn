//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(org: Org, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getOrg(org.id, in: context) {
                self.update(org: p, from: org)
            } else {
                self.create(org: org, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }
    
    // MARK: - internal tools
    
    internal func update(org cp: COrg, from p: Org) {
        cp.id = Int64(p.id.toInt)
        cp.name = p.name
        cp.notes = p.notes
        cp.address = p.address
    }
}
