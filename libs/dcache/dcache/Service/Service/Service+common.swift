//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheServiceService {
    public func exists(service id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CService.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }
    
    // MARK: - Stubs
    
    public func get(service: String) -> Service? {
        return _get(service: service)
    }
    
    public func update(service: Service, completion: @escaping (() -> Void)) {
        _update(service: service, completion: completion)
    }
    
    public func create(service: Service, completion: @escaping (() -> Void)) {
        _create(service: service, completion: completion)
    }
    
    public func delete(service id: String, completion: @escaping (() -> Void)) {
        _delete(service: id, completion: completion)
    }
}
