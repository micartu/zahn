//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(service id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(service: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }
    
    internal func delete(service id: String, in context: NSManagedObjectContext) {
        if let p = getService(id, in: context) {
            let rts = (p.ratings ?? "")
                .split(separator: self.kDelimiter).map(String.init)
            for rid in rts {
                if let cr = getRating(rid, in: context) {
                    cr.mr_deleteEntity(in: context)
                }
            }
            p.mr_deleteEntity(in: context)
        }
    }
}
