//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(service: Service, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getService(service.id, in: context) {
                self.update(service: p, from: service)
            } else {
                self.create(service: service, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    internal func update(service cr: CService, from r: Service) {
        cr.id = r.id
        cr.name = r.name
    }
}
