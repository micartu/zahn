//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(service id: String) -> Service? {
        if let p = getService(id) {
            return convert(service: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getService(_ id: String, in context: NSManagedObjectContext) -> CService? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CService.mr_findFirst(with: predicate, in: context)
    }

    internal func getService(_ id: String) -> CService? {
        return getService(id, in: NSManagedObjectContext.mr_default())
    }
}
