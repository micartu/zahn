//
//  Service+Rating.swift
//  dcache
//
//  Created by Michael Artuerhof on 03.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache: CacheServiceRatingService {
    public func add(rating: Rating, to srv: Service, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.add(rating: rating, to: srv, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func add(rating: Rating, srvId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let csrv = self.getService(srvId, in: context) {
                self.add(rating: rating, to: csrv, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func add(rating: String, srvId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let csrv = self.getService(srvId, in: context) {
                self.add(rating: rating, to: csrv, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func ratings(service: String) -> [Rating] {
        var out = [Rating]()
        if let ce = getService(service) {
            guard let rts = ce.ratings?.split(separator: kDelimiter)
                .map(String.init)
                else {
                    return out
            }
            for rid in rts {
                if let r = getRating(rid) {
                    let rr = convert(rating: r)
                    out.append(rr)
                }
            }
        }
        return out
    }

    // MARK: - Internal

    internal func add(rating: Rating, to srv: Service, in context: NSManagedObjectContext) {
        let csrv: CService
        if let cs = getService(srv.id, in: context) {
            csrv = cs
            update(service: cs, from: srv)
        } else {
            csrv = create(service: srv, in: context)
        }
        self.add(rating: rating, to: csrv, in: context)
    }

    internal func add(rating r: Rating, to csrv: CService, in context: NSManagedObjectContext) {
        if !exists(rating: r.id) {
            create(rating: r, in: context)
        } else {
            if let cr = getRating(r.id, in: context) {
                update(rating: cr, from: r)
            }
        }
        add(rating: r.id, to: csrv, in: context)
    }

    internal func add(rating rid: String, to csrv: CService, in context: NSManagedObjectContext) {
        var rts = csrv.ratings ?? ""
        let d = (rts.count > 0) ? String(kDelimiter) : ""
        rts += d + rid
        csrv.ratings = rts
    }
}
