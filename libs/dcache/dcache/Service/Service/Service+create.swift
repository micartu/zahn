//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(service p: Service, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(service: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(service: Service, in context: NSManagedObjectContext) -> CService {
        let p = CService.mr_createEntity(in: context)!
        update(service: p, from: service)
        return p
    }
}
