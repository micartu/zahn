//
//  OrgPerson.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheOrgPersonService {
    public func organzations(for person: Person) -> [Org] {
        return organzations(for: person.id)
    }

    public func organzations(for pid: String) -> [Org] {
        var o = [Org]()
        let predicate = NSPredicate(format: "(pid == %@)", pid)
        if let opers = COrgPerson.mr_findAll(with: predicate) as? [COrgPerson] {
            for oper in opers {
                let oid = "\(oper.oid)"
                if let org = get(org: oid) {
                    o.append(org)
                }
            }
        }
        return o
    }

    public func attach(org: Org, to person: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrPerson(org: org, person: person, in: context)
            self.attach(oid: org.id, to: person.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detach(org: Org, to person: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrPerson(org: org, person: person, in: context)
            self.detach(oid: org.id, to: person.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detach(org: Org, to pid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrPerson(org: org, person: nil, in: context)
            self.detach(oid: org.id, to: pid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func attach(org: Org, to pid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrPerson(org: org, person: nil, in: context)
            self.attach(oid: org.id, to: pid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachFromOrgs(person: Person, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrPerson(org: nil, person: person, in: context)
            self.detachFromOrgs(pid: person.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachFromOrgs(pid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.detachFromOrgs(pid: pid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Private

    internal func detachFromOrgs(pid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(pid == %@)", pid)
        if let opers = COrgPerson.mr_findAll(with: predicate) as? [COrgPerson] {
            for oper in opers {
                oper.mr_deleteEntity(in: context)
            }
        }
    }

    private func detach(oid: String, to pid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(pid == %@) AND (oid == %ld)", pid, oid.toInt)
        if let oper = COrgPerson.mr_findFirst(with: predicate) {
            oper.mr_deleteEntity(in: context)
        }
    }

    public func attach(oid: String, to pid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(pid == %@) AND (oid == %ld)", pid, oid.toInt)
        if COrgPerson.mr_countOfEntities(with: predicate) == 0 {
            let oper = COrgPerson.mr_createEntity(in: context)!
            oper.pid = pid
            oper.oid = Int64(oid.toInt)
        }
    }

    private func CreateOrgOrPerson(org: Org?, person: Person?, in context: NSManagedObjectContext) {
        if let org = org {
            if !self.exists(org: org.id) {
                self.create(org: org, in: context)
            }
        }
        if let p = person {
            if !self.exists(person: p.id) {
                self.create(person: p, in: context)
            }
        }
    }
}
