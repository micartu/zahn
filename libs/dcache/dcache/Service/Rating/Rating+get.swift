//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(rating id: String) -> Rating? {
        if let p = getRating(id) {
            return convert(rating: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getRating(_ id: String, in context: NSManagedObjectContext) -> CRating? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CRating.mr_findFirst(with: predicate, in: context)
    }

    internal func getRating(_ id: String) -> CRating? {
        return getRating(id, in: NSManagedObjectContext.mr_default())
    }
}
