//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(rating: Rating, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getRating(rating.id, in: context) {
                self.update(rating: p, from: rating)
            } else {
                self.create(rating: rating, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools
    
    @discardableResult
    internal func createOrUpdateAndIncreaseUsage(rating r: Rating,
                                                 in context: NSManagedObjectContext) -> CRating {
        let cr: CRating
        if let oldR = getRating(r.id, in: context) {
            cr = oldR
            update(rating: oldR, from: r)
        } else {
            cr = create(rating: r, in: context)
        }
        cr.used += 1
        return cr
    }

    internal func update(rating cr: CRating, from r: Rating) {
        cr.id = r.id
        cr.date = r.date as NSDate
        cr.creatorId = r.creatorId
        cr.stars = Int16(r.stars)
    }
}
