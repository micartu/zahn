//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheRatingService {
    public func exists(rating id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CRating.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(rating: String) -> Rating? {
        return _get(rating: rating)
    }

    public func update(rating: Rating, completion: @escaping (() -> Void)) {
        _update(rating: rating, completion: completion)
    }

    public func create(rating: Rating, completion: @escaping (() -> Void)) {
        _create(rating: rating, completion: completion)
    }

    public func delete(rating id: String, completion: @escaping (() -> Void)) {
        _delete(rating: id, completion: completion)
    }
}
