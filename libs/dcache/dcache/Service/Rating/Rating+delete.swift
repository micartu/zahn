//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(rating id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(rating: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }
    
    internal func delete(rating id: String, in context: NSManagedObjectContext) {
        if let p = getRating(id, in: context) {
            p.mr_deleteEntity(in: context)
        }
    }
    
    internal func decrementUsageAndDelete(rating id: String, in context: NSManagedObjectContext) {
        if let r = getRating(id, in: context) {
            r.used -= 1
            if r.used <= 0 {
                r.mr_deleteEntity(in: context)
            }
        }
    }
}
