//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(rating r: CRating) -> Rating {
        return Rating(id: r.id!,
                      creatorId: r.creatorId!,
                      date: r.date! as Date,
                      stars: Int(r.stars))
    }
}
