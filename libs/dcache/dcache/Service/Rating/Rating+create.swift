//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(rating p: Rating, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(rating: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(rating: Rating, in context: NSManagedObjectContext) -> CRating {
        let p = CRating.mr_createEntity(in: context)!
        update(rating: p, from: rating)
        return p
    }
}
