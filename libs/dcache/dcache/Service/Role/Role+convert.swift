//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(role r: CRole) -> Role {
        return Role(id: r.id!,
                    name: r.name!,
                    right: r.right!)
    }
}
