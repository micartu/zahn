//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheRoleService {
    public func exists(role id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CRole.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }
    
    // MARK: - Stubs
    
    public func get(role: String) -> Role? {
        return _get(role: role)
    }
    
    public func update(role: Role, completion: @escaping (() -> Void)) {
        _update(role: role, completion: completion)
    }
    
    public func create(role: Role, completion: @escaping (() -> Void)) {
        _create(role: role, completion: completion)
    }
    
    public func delete(role id: String, completion: @escaping (() -> Void)) {
        _delete(role: id, completion: completion)
    }
}
