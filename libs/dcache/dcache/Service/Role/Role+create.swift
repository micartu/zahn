//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(role r: Role, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(role: r, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(role: Role, in context: NSManagedObjectContext) -> CRole {
        let p = CRole.mr_createEntity(in: context)!
        update(role: p, from: role)
        return p
    }
}
