//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(role: Role, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getRole(role.id, in: context) {
                self.update(role: p, from: role)
            } else {
                self.create(role: role, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }


    // MARK: - internal tools

    internal func update(role cr: CRole, from r: Role) {
        cr.id = r.id
        cr.name = r.name
        cr.right = r.right
    }
}
