//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(role id: String) -> Role? {
        if let r = getRole(id) {
            return convert(role: r)
        }
        return nil
    }

    // MARK: - Internal

    internal func getRole(_ id: String, in context: NSManagedObjectContext) -> CRole? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CRole.mr_findFirst(with: predicate, in: context)
    }

    internal func getRole(_ id: String) -> CRole? {
        return getRole(id, in: NSManagedObjectContext.mr_default())
    }
}
