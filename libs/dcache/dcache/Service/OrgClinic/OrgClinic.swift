//
//  OrgClinic.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheOrgClinicService {
    public func organzations(clinic: Clinic) -> [Org] {
        return organzations(cid: clinic.id)
    }

    public func organzations(cid: String) -> [Org] {
        var o = [Org]()
        let predicate = NSPredicate(format: "(cid == %@)", cid)
        if let opers = COrgClinic.mr_findAll(with: predicate) as? [COrgClinic] {
            for oper in opers {
                let oid = "\(oper.oid)"
                if let org = get(org: oid) {
                    o.append(org)
                }
            }
        }
        return o
    }

    public func attach(org: Org, clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrClinic(org: org, clinic: clinic, in: context)
            self.attach(oid: org.id, cid: clinic.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detach(org: Org, clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrClinic(org: org, clinic: clinic, in: context)
            self.detach(oid: org.id, cid: clinic.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detach(org: Org, cid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrClinic(org: org, clinic: nil, in: context)
            self.detach(oid: org.id, cid: cid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func attach(org: Org, cid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrClinic(org: org, clinic: nil, in: context)
            self.attach(oid: org.id, cid: cid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachFromOrgs(clinic: Clinic, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.CreateOrgOrClinic(org: nil, clinic: clinic, in: context)
            self.detachFromOrgs(cid: clinic.id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func detachFromOrgs(cid: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.detachFromOrgs(cid: cid, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Private

    internal func detachFromOrgs(cid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(cid == %@)", cid)
        if let opers = COrgClinic.mr_findAll(with: predicate) as? [COrgClinic] {
            for oper in opers {
                oper.mr_deleteEntity(in: context)
            }
        }
    }

    private func detach(oid: String, cid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(cid == %@) AND (oid == %ld)", cid, oid.toInt)
        if let oper = COrgClinic.mr_findFirst(with: predicate) {
            oper.mr_deleteEntity(in: context)
        }
    }

    public func attach(oid: String, cid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(cid == %@) AND (oid == %ld)", cid, oid.toInt)
        if COrgClinic.mr_countOfEntities(with: predicate) == 0 {
            let oper = COrgClinic.mr_createEntity(in: context)!
            oper.cid = cid
            oper.oid = Int64(oid.toInt)
        }
    }

    private func CreateOrgOrClinic(org: Org?, clinic: Clinic?, in context: NSManagedObjectContext) {
        if let org = org {
            if !self.exists(org: org.id) {
                self.create(org: org, in: context)
            }
        }
        if let p = clinic {
            if !self.exists(clinic: p.id) {
                self.create(clinic: p, in: context)
            }
        }
    }
}
