//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(cost r: CCost) -> Cost {
        return Cost(id: r.id!,
                    currency: r.currency!,
                    cost: r.cost)
    }
}
