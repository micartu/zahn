//
//  Cache+Cost.swift
//  dcache
//
//  Created by Michael Artuerhof on 29.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache: CacheServiceCostService {
    public func set(cost: Cost, to srv: Service, of ev: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let ce = self.getEvent(ev.id, in: context) {
                var servs = (ce.services ?? "")
                    .split(separator: self.kDelimiter).map(String.init)
                var csts = (ce.costs ?? "")
                    .split(separator: self.kDelimiter).map(String.init)
                for i in servs.indices {
                    let sid = servs[i]
                    if sid == srv.id {
                        // delete old values:
                        self.delete(cost: csts[i], in: context)
                        // create new ones:
                        self.create(cost: cost, in: context)

                        // replace with new one ids:
                        csts[i] = cost.id
                        ce.costs = csts.joined(separator: String(self.kDelimiter))
                        break
                    }
                }
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func getCostOf(service s: Service, and ev: Event) -> Cost? {
        if let ce = getEvent(ev.id) {
            var servs = (ce.services ?? "")
                .split(separator: self.kDelimiter).map(String.init)
            var csts = (ce.costs ?? "")
                .split(separator: self.kDelimiter).map(String.init)
            for i in servs.indices {
                let sid = servs[i]
                if sid == s.id {
                    if let c = getCost(csts[i]) {
                        return convert(cost: c)
                    }
                }
            }
        }
        return nil
    }
}
