//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(cost id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(cost: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    internal func delete(cost id: String, in context: NSManagedObjectContext) {
        if let p = getCost(id, in: context) {
            p.mr_deleteEntity(in: context)
        }
    }
}
