//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(cost p: Cost, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(cost: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(cost: Cost, in context: NSManagedObjectContext) -> CCost {
        let p = CCost.mr_createEntity(in: context)!
        update(cost: p, from: cost)
        return p
    }
}
