//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheCostService {
    public func exists(cost id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CCost.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(cost: String) -> Cost? {
        return _get(cost: cost)
    }

    public func update(cost: Cost, completion: @escaping (() -> Void)) {
        _update(cost: cost, completion: completion)
    }

    public func create(cost: Cost, completion: @escaping (() -> Void)) {
        _create(cost: cost, completion: completion)
    }

    public func delete(cost id: String, completion: @escaping (() -> Void)) {
        _delete(cost: id, completion: completion)
    }
}
