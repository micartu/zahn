//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(cost: Cost, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getCost(cost.id, in: context) {
                self.update(cost: p, from: cost)
            } else {
                self.create(cost: cost, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func createOrUpdateAndIncreaseUsage(cost r: Cost,
                                                 in context: NSManagedObjectContext) -> CCost {
        let cr: CCost
        if let oldR = getCost(r.id, in: context) {
            cr = oldR
            update(cost: oldR, from: r)
        } else {
            cr = create(cost: r, in: context)
        }
        return cr
    }

    internal func update(cost cr: CCost, from r: Cost) {
        cr.id = r.id
        cr.currency = r.currency
        cr.cost = r.cost
    }
}
