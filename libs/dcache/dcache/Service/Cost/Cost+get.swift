//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(cost id: String) -> Cost? {
        if let p = getCost(id) {
            return convert(cost: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getCost(_ id: String, in context: NSManagedObjectContext) -> CCost? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CCost.mr_findFirst(with: predicate, in: context)
    }

    internal func getCost(_ id: String) -> CCost? {
        return getCost(id, in: NSManagedObjectContext.mr_default())
    }
}
