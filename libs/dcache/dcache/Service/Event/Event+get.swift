//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _takenEventsFor(person p: Person, batchSize: Int, step: Int) -> [Event] {
        var out = [Event]()
        if let cevs = takenEventsFor(person: p.id,
                                     batchSize: batchSize,
                                     step: step,
                                     in: NSManagedObjectContext.mr_default()) {
            for cev in cevs {
                if let ce = getEvent(cev.eventId!) {
                    let e = convert(event: ce)
                    out.append(e)
                }
            }
        }
        return out
    }

    func _madenEventsFor(person p: Person, batchSize: Int, step: Int) -> [Event] {
        var out = [Event]()
        if let cevs = madenEventsFor(person: p.id,
                                     batchSize: batchSize,
                                     step: step,
                                     in: NSManagedObjectContext.mr_default()) {
            for cev in cevs {
                if let ce = getEvent(cev.eventId!) {
                    let e = convert(event: ce)
                    out.append(e)
                }
            }
        }
        return out
    }

    func _servicesFor(event: Event) -> [Service] {
        var out = [Service]()
        for sid in event.services {
            if let cs = getService(sid) {
                let s = convert(service: cs)
                out.append(s)
            }
        }
        return out
    }

    // MARK: - internal tools

    internal func takenEventsFor(person pid: String,
                                 batchSize: Int,
                                 step: Int,
                                 in context: NSManagedObjectContext) -> [CPersonTakenEvent]? {
        let predicate = NSPredicate(format: "(personId == %@)", pid)
        let request = CPersonTakenEvent.mr_requestAllSorted(by: "date", ascending: false, with: predicate, in: context)
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CPersonTakenEvent.mr_executeFetchRequest(request) as? [CPersonTakenEvent]
    }

    internal func madenEventsFor(person pid: String,
                                 batchSize: Int,
                                 step: Int,
                                 in context: NSManagedObjectContext) -> [CPersonMadenEvent]? {
        let predicate = NSPredicate(format: "(personId == %@)", pid)
        let request = CPersonMadenEvent.mr_requestAllSorted(by: "date", ascending: false, with: predicate, in: context)
        request.fetchLimit = batchSize
        if step >= 0 {
            request.fetchOffset = step * batchSize
        }
        return CPersonMadenEvent.mr_executeFetchRequest(request) as? [CPersonMadenEvent]
    }
}

extension Cache {
    func _get(event id: String) -> Event? {
        if let p = getEvent(id) {
            return convert(event: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getEvent(_ id: String, in context: NSManagedObjectContext) -> CEvent? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CEvent.mr_findFirst(with: predicate, in: context)
    }

    internal func getEvent(_ id: String) -> CEvent? {
        return getEvent(id, in: NSManagedObjectContext.mr_default())
    }
}
