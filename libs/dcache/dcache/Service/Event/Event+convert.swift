//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(event e: CEvent) -> Event {
        let costs = e.costs!.split(separator: kDelimiter).map(String.init)
        let services = e.services!.split(separator: kDelimiter).map(String.init)
        return Event(id: e.id!,
                     date: e.date! as Date,
                     services: services,
                     costs: costs)
    }
}
