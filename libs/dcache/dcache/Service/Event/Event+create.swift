//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _addTakenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if !self.exists(person: person.id) {
                self.create(person: person, in: context)
            }
            self.addTakenEventsTo(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _addMadenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if !self.exists(person: person.id) {
                self.create(person: person, in: context)
            }
            self.addMadenEventsTo(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _addTakenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if !self.exists(person: person.id) {
                self.create(person: person, in: context)
            }
            self.addTakenEventsTo(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _addMadenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if !self.exists(person: person.id) {
                self.create(person: person, in: context)
            }
            self.addMadenEventsTo(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _add(services: [Service], costs: [Cost], to e: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            let ce: CEvent
            if let tce = self.getEvent(e.id, in: context) {
                self.update(event: tce, from: e)
                ce = tce
            } else {
                ce = self.create(event: e, in: context)
            }
            self.add(services: services, costs: costs, to: ce, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Internal

    internal func addTakenEventsTo(person pid: String, events: [Event], in context: NSManagedObjectContext) {
        for e in events {
            let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, e.id)
            if CPersonTakenEvent.mr_countOfEntities(with: predicate) == 0 {
                let cev = CPersonTakenEvent.mr_createEntity(in: context)!
                let ce = createOrUpdateAndIncreaseUsage(event: e, in: context)
                cev.personId = pid
                cev.eventId = e.id
                cev.date = ce.date
            }
        }
    }

    internal func addTakenEventsTo(person pid: String, events: [String], in context: NSManagedObjectContext) {
        for eid in events {
            let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, eid)
            if CPersonTakenEvent.mr_countOfEntities(with: predicate) == 0 {
                if let ce = self.getEvent(eid, in: context) {
                    let cev = CPersonTakenEvent.mr_createEntity(in: context)!
                    cev.personId = pid
                    cev.eventId = eid
                    cev.date = ce.date
                }
            }
        }
    }

    internal func addMadenEventsTo(person pid: String, events: [Event], in context: NSManagedObjectContext) {
        for e in events {
            let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, e.id)
            if CPersonMadenEvent.mr_countOfEntities(with: predicate) == 0 {
                let cev = CPersonMadenEvent.mr_createEntity(in: context)!
                let ce = createOrUpdateAndIncreaseUsage(event: e, in: context)
                cev.personId = pid
                cev.eventId = e.id
                cev.date = ce.date
            }
        }
    }

    internal func addMadenEventsTo(person pid: String, events: [String], in context: NSManagedObjectContext) {
        for eid in events {
            let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, eid)
            if CPersonMadenEvent.mr_countOfEntities(with: predicate) == 0 {
                if let ce = self.getEvent(eid, in: context) {
                    let cev = CPersonMadenEvent.mr_createEntity(in: context)!
                    cev.personId = pid
                    cev.eventId = eid
                    cev.date = ce.date
                }
            }
        }
    }

    internal func add(services: [Service], costs: [Cost], to ce: CEvent, in context: NSManagedObjectContext) {
        if costs.count != services.count { return }
        var servs = ce.services ?? ""
        for s in services {
            if !exists(service: s.id) {
                create(service: s, in: context)
            }
            let d = (servs.count > 0) ? String(kDelimiter) : ""
            servs += d + s.id
        }
        var csts = ce.costs ?? ""
        for c in costs {
            if !exists(cost: c.id) {
                create(cost: c, in: context)
            }
            let d = (csts.count > 0) ? String(kDelimiter) : ""
            csts += d + c.id
        }
        ce.services = servs
        ce.costs = csts
    }
}

extension Cache {
    func _create(event p: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(event: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(event: Event, in context: NSManagedObjectContext) -> CEvent {
        let p = CEvent.mr_createEntity(in: context)!
        update(event: p, from: event)
        return p
    }
}
