//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _deleteTakenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.deleteTakenEventsFrom(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _deleteMadenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.deleteMadenEventsFrom(person: person.id, events: events, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    func _delete(services: [Service], from e: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let ce = self.getEvent(e.id, in: context) {
                self.delete(services: services, from: ce, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - Internal

    internal func deleteMadenEventFrom(person pid: String, event eid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, eid)
        let ce = CPersonMadenEvent.mr_findFirst(with: predicate, in: context)
        decrementUsageAndDelete(event: eid, in: context)
        ce?.mr_deleteEntity(in: context)
    }

    internal func deleteMadenEventsFrom(person pid: String, events: [Event], in context: NSManagedObjectContext) {
        for e in events {
            deleteMadenEventFrom(person: pid, event: e.id, in: context)
        }
    }

    internal func deleteTakenEventFrom(person pid: String, event eid: String, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "(personId == %@) AND (eventId == %@)", pid, eid)
        let ce = CPersonTakenEvent.mr_findFirst(with: predicate, in: context)
        decrementUsageAndDelete(event: eid, in: context)
        ce?.mr_deleteEntity(in: context)
    }

    internal func deleteTakenEventsFrom(person pid: String, events: [Event], in context: NSManagedObjectContext) {
        for e in events {
            deleteTakenEventFrom(person: pid, event: e.id, in: context)
        }
    }

    internal func delete(services: [Service], from ce: CEvent, in context: NSManagedObjectContext) {
        for s in services {
            delete(service: s.id, from: ce, in: context)
        }
    }

    internal func delete(services: [String], from ce: CEvent, in context: NSManagedObjectContext) {
        for sid in services {
            delete(service: sid, from: ce, in: context)
        }
    }

    internal func delete(service sid: String, from ce: CEvent, in context: NSManagedObjectContext) {
        guard var srvs = ce.services?.split(separator: kDelimiter).map(String.init) else { return }
        guard var csts = ce.costs?.split(separator: kDelimiter).map(String.init) else { return }
        if csts.count != srvs.count {
            print("inconsitancy in delete(services:from: in:")
            return
        }
        for i in srvs.indices.reversed() {
            if srvs[i] == sid {
                let cid = csts[i]
                delete(cost: cid, in: context)
                delete(service: sid, in: context)
                srvs.remove(at: i)
                csts.remove(at: i)
                break
            }
        }
        ce.costs = csts.joined(separator: String(kDelimiter))
        ce.services = srvs.joined(separator: String(kDelimiter))
    }
}

extension Cache {
    func _delete(event id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(event: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    internal func delete(event id: String, in context: NSManagedObjectContext) {
        if let ce = getEvent(id, in: context) {
            delete(event: ce, in: context)
        }
    }

    internal func delete(event ce: CEvent, in context: NSManagedObjectContext) {
        let srvs = ce.services!.split(separator: kDelimiter).map(String.init)
        delete(services: srvs, from: ce, in: context)
        ce.mr_deleteEntity(in: context)
    }

    internal func decrementUsageAndDelete(event id: String, in context: NSManagedObjectContext) {
        if let e = getEvent(id, in: context) {
            e.used -= 1
            if e.used <= 0 {
                delete(event: e, in: context)
            }
        }
    }
}
