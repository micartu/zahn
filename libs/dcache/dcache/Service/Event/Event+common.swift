//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheEventPersonService {
    public func takenEventExistsFor(person pid: String, event eid: String) -> Bool {
        let predicate = NSPredicate(format: "(eventId == %@) AND (personId == %@)", eid, pid)
        if CPersonTakenEvent.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    public func madenEventExistsFor(person pid: String, event eid: String) -> Bool {
        let predicate = NSPredicate(format: "(eventId == %@) AND (personId == %@)", eid, pid)
        if CPersonMadenEvent.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func takenEventsFor(person: Person, batchSize: Int, step: Int) -> [Event] {
        return _takenEventsFor(person: person, batchSize: batchSize, step: step)
    }

    public func madenEventsFor(person: Person, batchSize: Int, step: Int) -> [Event] {
        return _madenEventsFor(person: person, batchSize: batchSize, step: step)
    }

    public func servicesFor(event: Event) -> [Service] {
        return _servicesFor(event: event)
    }

    public func addTakenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        _addTakenEventsTo(person: person, events: events, completion: completion)
    }

    public func addMadenEventsTo(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        _addMadenEventsTo(person: person, events: events, completion: completion)
    }

    public func addTakenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void)) {
        _addTakenEventsTo(person: person, events: events, completion: completion)
    }

    public func addMadenEventsTo(person: Person, events: [String], completion: @escaping (() -> Void)) {
        _addMadenEventsTo(person: person, events: events, completion: completion)
    }

    public func add(services: [Service], costs: [Cost], to e: Event, completion: @escaping (() -> Void)) {
        _add(services: services, costs: costs, to: e, completion: completion)
    }

    public func deleteTakenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        _deleteTakenEventsFrom(person: person, events: events, completion: completion)
    }

    public func deleteMadenEventsFrom(person: Person, events: [Event], completion: @escaping (() -> Void)) {
        _deleteMadenEventsFrom(person: person, events: events, completion: completion)
    }

    public func delete(services: [Service], from e: Event, completion: @escaping (() -> Void)) {
        _delete(services: services, from: e, completion: completion)
    }
}

extension Cache: CacheEventService {
    public func exists(event id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CEvent.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(event: String) -> Event? {
        return _get(event: event)
    }

    public func update(event: Event, completion: @escaping (() -> Void)) {
        _update(event: event, completion: completion)
    }

    public func create(event: Event, completion: @escaping (() -> Void)) {
        _create(event: event, completion: completion)
    }

    public func delete(event id: String, completion: @escaping (() -> Void)) {
        _delete(event: id, completion: completion)
    }
}
