//
//  Event+Rating.swift
//  dcache
//
//  Created by Michael Artuerhof on 03.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache: CacheEventRatingEvent {
    public func add(rating: Rating, to evnt: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.add(rating: rating, to: evnt, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    public func add(rating: Rating, evntId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let cevnt = self.getEvent(evntId, in: context) {
                self.add(rating: rating, to: cevnt, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func add(rating: String, evntId: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let cevnt = self.getEvent(evntId, in: context) {
                self.add(rating: rating, to: cevnt, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    public func ratings(evntId: String) -> [Rating] {
        var out = [Rating]()
        if let ce = getEvent(evntId) {
            guard let rts = ce.ratings?.split(separator: kDelimiter)
                .map(String.init)
                else {
                    return out
            }
            for rid in rts {
                if let r = getRating(rid) {
                    let rr = convert(rating: r)
                    out.append(rr)
                }
            }
        }
        return out
    }

    // MARK: - Internal

    internal func add(rating: Rating, to evnt: Event, in context: NSManagedObjectContext) {
        let cevnt: CEvent
        if let cs = getEvent(evnt.id, in: context) {
            cevnt = cs
            update(event: cs, from: evnt)
        } else {
            cevnt = create(event: evnt, in: context)
        }
        self.add(rating: rating, to: cevnt, in: context)
    }

    internal func add(rating r: Rating, to cevnt: CEvent, in context: NSManagedObjectContext) {
        if !exists(rating: r.id) {
            create(rating: r, in: context)
        } else {
            if let cr = getRating(r.id, in: context) {
                update(rating: cr, from: r)
            }
        }
        add(rating: r.id, to: cevnt, in: context)
    }

    internal func add(rating rid: String, to cevnt: CEvent, in context: NSManagedObjectContext) {
        var rts = cevnt.ratings ?? ""
        let d = (rts.count > 0) ? String(kDelimiter) : ""
        rts += d + rid
        cevnt.ratings = rts
    }
}
