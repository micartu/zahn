//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
}

extension Cache {
    func _update(event: Event, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getEvent(event.id, in: context) {
                self.update(event: p, from: event)
            } else {
                self.create(event: event, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func createOrUpdateAndIncreaseUsage(event r: Event,
                                                 in context: NSManagedObjectContext) -> CEvent {
        let cr: CEvent
        if let oldR = getEvent(r.id, in: context) {
            cr = oldR
            update(event: oldR, from: r)
        } else {
            cr = create(event: r, in: context)
        }
        cr.used += 1
        return cr
    }

    internal func update(event ce: CEvent, from e: Event) {
        ce.id = e.id
        ce.date = e.date as NSDate
        ce.services = e.services.joined(separator: String(kDelimiter))
        ce.costs = e.costs.joined(separator: String(kDelimiter))
    }
}
