//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(phone: Phone, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getPhone(phone.id, in: context) {
                self.update(phone: p, from: phone)
            } else {
                self.create(phone: phone, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }
    
    // MARK: - internal tools
    
    internal func update(phone cp: CPhone, from p: Phone) {
        cp.id = p.id
        cp.number = p.number
    }
}
