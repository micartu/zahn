//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CachePhoneService {
    public func exists(phone id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CPhone.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }
    
    // MARK: - Stubs
    
    public func get(phone: String) -> Phone? {
        return _get(phone: phone)
    }
    
    public func update(phone: Phone, completion: @escaping (() -> Void)) {
        _update(phone: phone, completion: completion)
    }
    
    public func create(phone: Phone, completion: @escaping (() -> Void)) {
        _create(phone: phone, completion: completion)
    }
    
    public func delete(phone id: String, completion: @escaping (() -> Void)) {
        _delete(phone: id, completion: completion)
    }
}
