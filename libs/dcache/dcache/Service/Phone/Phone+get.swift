//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(phone id: String) -> Phone? {
        if let p = getPhone(id) {
            return convert(phone: p)
        }
        return nil
    }
    
    // MARK: - Internal
    
    internal func getPhone(_ id: String, in context: NSManagedObjectContext) -> CPhone? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CPhone.mr_findFirst(with: predicate, in: context)
    }
    
    internal func getPhone(_ id: String) -> CPhone? {
        return getPhone(id, in: NSManagedObjectContext.mr_default())
    }
}
