//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(phone p: Phone, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(phone: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }
    
    // MARK: - internal tools
    
    @discardableResult
    internal func create(phone: Phone, in context: NSManagedObjectContext) -> CPhone {
        let p = CPhone.mr_createEntity(in: context)!
        update(phone: p, from: phone)
        return p
    }
}
