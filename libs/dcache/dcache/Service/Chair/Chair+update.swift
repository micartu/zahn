//
//  Cache+update.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _update(chair: Chair, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.getChair(chair.id, in: context) {
                self.update(chair: p, from: chair)
            } else {
                self.create(chair: chair, in: context)
            }
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func createOrUpdateAndIncreaseUsage(chair r: Chair,
                                                 in context: NSManagedObjectContext) -> CChair {
        let cr: CChair
        if let oldR = getChair(r.id, in: context) {
            cr = oldR
            update(chair: oldR, from: r)
        } else {
            cr = create(chair: r, in: context)
        }
        return cr
    }

    internal func update(chair cr: CChair, from r: Chair) {
        cr.id = r.id
        cr.name = r.name
        cr.descr = r.descr
    }
}
