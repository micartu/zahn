//
//  Cache+get.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _get(chair id: String) -> Chair? {
        if let p = getChair(id) {
            return convert(chair: p)
        }
        return nil
    }

    // MARK: - Internal

    internal func getChair(_ id: String, in context: NSManagedObjectContext) -> CChair? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CChair.mr_findFirst(with: predicate, in: context)
    }

    internal func getChair(_ id: String) -> CChair? {
        return getChair(id, in: NSManagedObjectContext.mr_default())
    }
}
