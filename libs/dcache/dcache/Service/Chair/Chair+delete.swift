//
//  Cache+delete.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _delete(chair id: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(chair: id, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    internal func delete(chair id: String, in context: NSManagedObjectContext) {
        detachAllTasksFrom(chairId: id, in: context)
        if let p = getChair(id, in: context) {
            p.mr_deleteEntity(in: context)
        }
    }
}
