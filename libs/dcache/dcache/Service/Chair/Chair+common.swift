//
//  Cache.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

// common methods
extension Cache: CacheChairService {
    public func exists(chair id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        if CChair.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    // MARK: - Stubs

    public func get(chair: String) -> Chair? {
        return _get(chair: chair)
    }

    public func update(chair: Chair, completion: @escaping (() -> Void)) {
        _update(chair: chair, completion: completion)
    }

    public func create(chair: Chair, completion: @escaping (() -> Void)) {
        _create(chair: chair, completion: completion)
    }

    public func delete(chair id: String, completion: @escaping (() -> Void)) {
        _delete(chair: id, completion: completion)
    }
}
