//
//  Cache+convert.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    internal func convert(chair r: CChair) -> Chair {
        return Chair(id: r.id!,
                     name: r.name!,
                     descr: r.descr!)
    }
}
