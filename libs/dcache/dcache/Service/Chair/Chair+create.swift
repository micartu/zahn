//
//  Cache+create.swift
//  dcache
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord
import denticom

extension Cache {
    func _create(chair p: Chair, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(chair: p, in: context)
            }, completion: { success, err in
                completion()
        })
    }

    // MARK: - internal tools

    @discardableResult
    internal func create(chair: Chair, in context: NSManagedObjectContext) -> CChair {
        let p = CChair.mr_createEntity(in: context)!
        update(chair: p, from: chair)
        return p
    }
}
