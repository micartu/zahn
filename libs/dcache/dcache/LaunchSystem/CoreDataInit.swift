//
//  CoreDataInit.swift
//  zahn
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import MagicalRecord
import denticom

public class CoreDataInit: NSObject { }

extension CoreDataInit: AppDelegateProtocol {
    public func initModule() -> Bool {
        var bundles = [Bundle.main]
        if let b = Bundle(identifier: "com.bearmonti.dcache") {
            bundles.append(b)
        }
        if let cpod = Bundle(identifier: "org.cocoapods.dcache") {
            bundles.append(cpod)
        }
        MagicalRecord.setShouldAutoCreateManagedObjectModel(false)
        NSManagedObjectModel.mr_setDefaultManagedObjectModel(NSManagedObjectModel.mergedModel(from: bundles))
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: "zahn")
        return true
    }

    public func description() -> String {
        return "CoreDataInit"
    }

    public var backgroundMethod: ((UIApplication) -> Void)? {
        return { a in
            NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (success, err) in
                if let e = err {
                    print("error while saving core data: \(e.localizedDescription)")
                }
            })
        }
    }

    // MARK: - Not used

    public var foregroundMethod: ((UIApplication) -> Void)? { return nil }
    public var remoteNotification: (([AnyHashable : Any]) -> Void)? { return nil }
    public var canOpenURL: ((UIApplication, URL, [UIApplication.OpenURLOptionsKey : Any]) -> Bool)? { return nil }
}
