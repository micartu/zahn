//
//  CommonCellProtocol.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol CellCommonProtocol: Themeble {
    func set(focused: Bool)
}

public protocol CommonCellModel {
    var theme: ITheme { get }
    var id: String { get }
    var focused: Bool { get set }
}
