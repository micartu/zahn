//
//  BaseCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

open class BaseCoordinator <AlertBuilder: AlertBuilderProtocol> : Coordinator, ParentCoordinator, BaseDispatcher {
    public var viewControllable: ViewControlable?
    public var navigationController: NavigationControlable? = nil
    internal var parent: BaseCoordinator? = nil

    public init(parent: BaseCoordinator?) {
        self.parent = parent
    }

    // MARK: - Private / Internal

    /// Unique identifier
    private let identifier = UUID()

    /// Dictionary of the child coordinators. Every child coordinator should be added
    /// to that dictionary in order to keep it in memory
    /// Key is an `identifier` of the child coordinator and value is the coordinator itself
    /// Value type is `Any` because Swift doesn't allow to store generic types in the array
    internal var childCoordinators = [UUID: Any]()

    /// Stores coordinator to the `childCoordinators` dictionary
    ///
    /// - Parameter coordinator: Child coordinator to store
    internal func store(coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = coordinator
    }

    /// Release coordinator from the `childCoordinators` dictionary
    ///
    /// - Parameter coordinator: Coordinator to release
    internal func free(coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = nil
    }

    internal func exit(coordinator: BaseCoordinator, with result: Any?) {
        free(coordinator: coordinator)
    }

    // MARK: - Public

    /// 1. Stores coordinator in a dictionary of child coordinators
    /// 2. Calls method `start()` on that coordinator
    /// 3. If controlled view is set tries to present it
    ///
    /// - Parameter coordinator: Coordinator to start
    public func coordinate(to coordinator: BaseCoordinator) {
        store(coordinator: coordinator)
        coordinator.start()
        if let vc = self.viewControllable {
            coordinator.presented(on: vc, what: coordinator.viewControllable)
        }
    }

    /// called if controller should be presented from another vc
    open func presented(on vc: ViewControlable, what nvc: ViewControlable?) {
        parent?.presented(on: vc, what: nvc)
    }

    /// Starts job of the coordinator
    ///
    /// - Returns: Result of coordinator job
    open func start() {
        fatalError("Start method should be implemented.")
    }

    /// Stops coordinator's children
    public func stopChildren() {
        for (_, v) in childCoordinators {
            // emergency situation...
            if let coord = v as? BaseCoordinator {
                coord.stop()
            }
        }
        childCoordinators.removeAll()
    }

    /// Stops current coordinator and its children
    open func stop() {
        stopChildren()
    }

    // MARK: - ParentCoordinator

    open func exitFrom(coordinator: BaseCoordinator<AlertBuilder>, with result: Any?) {
        parent?.exitFrom(coordinator: coordinator, with: result)
        exit(coordinator: coordinator, with: result)
    }

    // MARK: - BaseDispatcher

    open func exitWith(result: Any?) {
        exitFrom(coordinator: self, with: result)
    }
}
