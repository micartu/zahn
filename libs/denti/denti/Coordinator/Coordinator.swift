//
//  Coordinator.swift
//  denti
//
//  Created by Michael Artuerhof on 23.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol Coordinator {
    var viewControllable: ViewControlable? { get }
    func start()
    func stopChildren()
    func stop()
    func presented(on vc: ViewControlable, what nvc: ViewControlable?)
}

public protocol ParentCoordinator: class {
    associatedtype alerter: AlertBuilderProtocol
    func exitFrom(coordinator: BaseCoordinator<alerter>, with result: Any?)
}

public protocol ParentAnyCoordinator: class {
    func exitFrom(coordinator: Any?, with result: Any?)
}

public protocol BaseDispatcher: class {
    func exitWith(result: Any?)
}

public protocol CommonCoordinator: ParentAnyCoordinator, BaseDispatcher, AlertProtocol { }
