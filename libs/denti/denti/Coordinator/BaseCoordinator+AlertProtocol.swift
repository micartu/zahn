//
//  BaseCoordinator+AlertProtocol.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension BaseCoordinator: AlertProtocol {
    public func show(error: String) {
        show(title: Const.text.kError, error: error, action: nil)
    }

    public func show(title: String, error: String, action: (() -> Void)?) {
        show(title: title, message: error, okTitle: Const.text.kOk, action: action)
    }

    public func show(title: String, error: String) {
        show(title: title, error: error, action: nil)
    }

    public func show(title: String, message: String) {
        show(title: title, message: message, okTitle: Const.text.kClose, action: nil)
    }

    public func show(title: String, message: String, okTitle: String, action: (() -> Void)?) {
        let ac = AlertBuilder.controller(title: title, message: message, style: .alert)
        let okAction = AlertBuilder.action(title: okTitle, style: .def, handler: action)
        ac.add(action: okAction)
        runOnMainThread { [weak self] in
            self?.viewControllable?.present(ac, animated: true, completion: nil)
        }
    }

    public func showYesNO(title: String,
                   message: String,
                   actionYes: (() -> Void)?,
                   actionNo: (() -> Void)?) {
        let ac = AlertBuilder.controller(title: title, message: message, style: .alert)
        let yesAction = AlertBuilder.action(title: Const.text.kYes, style: .def, handler: actionYes)
        ac.add(action: yesAction)
        let noAction = AlertBuilder.action(title: Const.text.kNo, style: .def, handler: actionNo)
        ac.add(action: noAction)
        runOnMainThread { [weak self] in
            self?.viewControllable?.present(ac, animated: true, completion: nil)
        }
    }

    public func showInputDialog(title: String,
                         message: String,
                         okAction:((String) -> Void)?) {
        let ac = AlertBuilder.controller(title: title, message: message, style: .alert)
        ac.addTextFieldWith(cfgHandler: nil)
        let yesAction = AlertBuilder.action(title: Const.text.kOk, style: .def, handler: {
            if let act = okAction {
                let textfields = ac.textFieldsInController!
                act(textfields.first!.text!)
            }
        })
        ac.add(action: yesAction)
        let noAction = AlertBuilder.action(title: Const.text.kCancel, style: .def, handler: nil)
        ac.add(action: noAction)
        runOnMainThread { [weak self] in
            self?.viewControllable?.present(ac, animated: true, completion: nil)
        }
    }

    public func chooseFrom(titles: [String],
                           title: String,
                           okAction:((String, Int) -> Void)?) {
        let ac = AlertBuilder.chooseFrom(titles: titles, title: title, okAction: okAction)
        runOnMainThread { [weak self] in
            self?.viewControllable?.present(ac, animated: true, completion: nil)
        }
    }

    public func showBusyIndicator() {
        AlertBuilder.showBusyIndicator()
    }

    public func hideBusyIndicator() {
        AlertBuilder.hideBusyIndicator()
    }
}
