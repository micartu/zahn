//
//  AlertController.swift
//  denti
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

// wrapper to UIAlertController.Style
public enum alertStyle {
    case actSheet
    case alert
}

// wrapper to UIAlertAction.Style
public enum alertButtonStyle {
    case destruct
    case cancel
    case def
}

public protocol AlertControllerAction: class {
    var atitle: String { get }
    var astyle: alertButtonStyle { get }
    static func create(atitle: String?,
                       astyle: alertButtonStyle,
                       ahandler: (() -> Void)?) -> AlertControllerAction
}

public protocol TextField: class {
    var text: String? { get set }
}

public protocol AlertController: ViewControlableCommon {
    var textFieldsInController: [TextField]? { get }
    func add(action: AlertControllerAction)
    func addTextFieldWith(cfgHandler: ((TextField) -> Void)?)
}

public protocol AlertBuilderProtocol: class {
    static func controller(title: String?, message: String?, style: alertStyle) -> AlertController
    static func action(title: String, style: alertButtonStyle, handler: (() -> Void)?) -> AlertControllerAction
    static func chooseFrom(titles: [String],
                    title: String,
                    okAction:((String, Int) -> Void)?) -> ViewControlable
    static func showBusyIndicator()
    static func hideBusyIndicator()
}
