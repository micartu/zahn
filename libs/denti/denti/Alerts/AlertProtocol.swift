//
//  AlertProtocol.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol AlertProtocol: class {
    func show(error: String)
    func show(title: String, error: String)
    func show(title: String, error: String, action: (() -> Void)?)
    func show(title: String, message: String)
    func show(title: String, message: String, okTitle: String, action: (() -> Void)?)
    func showYesNO(title: String, message: String, actionYes: (() -> Void)?, actionNo: (() -> Void)?)
    func showInputDialog(title: String, message: String, okAction:((String) -> Void)?)
    func chooseFrom(titles: [String], title: String, okAction:((String, Int) -> Void)?)

    // busy indicator
    func showBusyIndicator()
    func hideBusyIndicator()
}
