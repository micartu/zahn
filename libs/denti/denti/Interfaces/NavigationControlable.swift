//
//  NavigationControlable.swift
//  denti
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol NavigationControlable {
    func pushViewController(_ vc: ViewControlable, animated: Bool)
}
