//
//  ViewControlable.swift
//  denti
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

public protocol ViewControlableCommon: class {
}

public protocol ViewControlable: ViewControlableCommon {
    func present(_ vc: ViewControlableCommon, animated: Bool)
    func present(_ vc: ViewControlableCommon, animated: Bool, completion:(() -> Void)?)
}
