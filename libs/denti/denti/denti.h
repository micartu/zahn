//
//  denti.h
//  denti
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for denti.
FOUNDATION_EXPORT double dentiVersionNumber;

//! Project version string for denti.
FOUNDATION_EXPORT const unsigned char dentiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <denti/PublicHeader.h>

