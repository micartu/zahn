//
//  Tasker+Content.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

extension TaskerViewModel {
    internal func generateSomething() {
        addTextModel(title: "Name", text: "Name")
        addTextModel(title: "Notes", text: "Notes")
        addReadOnlyText(title: "Person", text: "Vasya hoho", image: "menu")
        addTime(start: "10:20", end: "2 hours", date: "13.04.19")
        addButton(title: "Modify", id: "Modify")
        editableItemFocusedAfter(section: 0, row: -1)
    }

    internal func addTextModel(title: String, text: String) {
        addTextModelCommon(title: title, text: text, image: nil, editable: true)
    }

    internal func addReadOnlyText(title: String, text: String, image: String) {
        addTextModelCommon(title: title, text: text, image: image, editable: false)
    }

    internal func addTextModelCommon(title: String, text: String, image: String?, editable: Bool) {
        let e = editable ? 1 : 0
        let m = TaskTextModel(theme: theme,
                              focused: false,
                              id: "\(section);\(index);\(e)",
                              delegate: self,
                              err: false,
                              editable: editable,
                              image: image,
                              title: text,
                              text: text)
        add(model: m)
    }

    internal func addButton(title: String, id: String) {
        let m = TaskButtonModel(theme: theme,
                                focused: false,
                                id: "\(section);\(index);\(id)",
                                delegate: self,
                                err: false,
                                title: title)
        add(model: m)
    }

    internal func addTime(start: String, end: String, date: String) {
        let m = TaskTimeModel(theme: theme,
                              focused: false,
                              id: "\(section);\(index)",
                              delegate: self,
                              err: false,
                              start: start,
                              duration: end,
                              date: date)
        add(model: m)
    }

    internal func add(model: TaskerCellModel) {
        index += 1
        append(model: model, into: section)
    }
}
