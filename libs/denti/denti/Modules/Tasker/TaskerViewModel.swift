//
//  TaskerViewModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public enum TaskerMode {
    case create
    case edit
}

public protocol TaskerRoutable: CommonCoordinator {
    func choosePerson(completion: @escaping ((Person?) -> Void))
}

public protocol NetTaskerProtocol:
    NetPersonService {}

public protocol CacheTaskerProtocol:
    CacheOrgService,
    CachePersonService {}

final public class TaskerViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: TaskerRoutable? = nil

    /// current application theme
    public var theme: ITheme!

    /// what will the model do?
    public var mode: TaskerMode = .edit

    /// task's info to be edited
    public var task: Task? = nil

    /// organization of a person who makes update/creation of a task
    public var org: Org? = nil

    // MARK: - Outputs

    /// update data shown to user
    public let updateData = Variable<Bool>(false)

    /// some data was changed
    public let dataChanged = Variable<Bool>(false)

    // MARK: - init
    public init(network: NetTaskerProtocol,
                secret: SecretService,
                cache: CacheTaskerProtocol) {
        self.network = network
        self.secret = secret
        self.cache = cache
    }

    // MARK: - public methods
    public func updateEnteredData(completion: @escaping ((NSError?) -> Void)) {

    }

    public func getContents() {
        generateSomething() // TODO: delete me
        updateData.value = true
    }

    public func model(for indexPath: IndexPath) -> TaskerCellModel {
        let m = models[indexPath.section]
        return m![indexPath.row]
    }

    public func selected(indexPath: IndexPath) {
        if let section = models[indexPath.section] {
            if let m = section[indexPath.row] as? TaskTextModel {
                let ids = m.id.split(separator: ";").map({String($0)})
                if ids.count >= 3 && ids[2] == "0" {
                    dispatcher?.choosePerson { p in
                        print("!!!person selected: \(String(describing: p))") // TODO: delete me
                    }
                }
            }
        }
    }

    public func modelsCount(for section: Int) -> Int {
        if let m = models[section] {
            return m.count
        }
        return 0
    }

    public func title(for section: Int) -> String {
        return "Section \(section)" // TODO: add normal section title here
    }

    public func sectionsCount() -> Int {
        return models.count
    }

    // MARK: - Private

    internal func append(model: TaskerCellModel, into section: Int) {
        if models[section] == nil {
            models[section] = [TaskerCellModel]()
        }
        models[section]?.append(model)
    }

    internal func separateId(_ id: String) -> (Int, Int) {
        let ids = id.split(separator: ";").map({String($0)})
        assert(ids.count >= 2)
        let section = ids[0].toInt
        let row = ids[1].toInt
        return (section, row)
    }

    internal func findModel(with id: String) -> TaskerCellModel? {
        let (section, i) = separateId(id)
        if let s = models[section] {
            if i < s.count {
                return s[i]
            }
        }
        return nil
    }

    internal func updateModel(_ m: TaskerCellModel) {
        let (section, i) = separateId(m.id)
        models[section]?[i] = m
        updateData.value = true
    }

    internal func editableItemFocusedAfter(section s: Int, row r: Int) {
        for i in 0..<models.count {
            if let mm = models[i] {
                for j in mm.indices {
                    if i <= s && j <= r {
                        models[i]?[j].focused = false
                    } else {
                        if let ch = mm[j] as? TaskTextModel,
                            ch.editable {
                            models[i]?[j].focused = true
                            // mission acomplished
                            return
                        }
                    }
                }
            }
        }
    }

    internal func removeAllFocuses() {
        for i in 0..<models.count {
            if let mm = models[i] {
                for j in mm.indices {
                    models[i]?[j].focused = false
                }
            }
        }
    }

    /// array of models to be shown
    internal var models = [Int:[TaskerCellModel]]()
    internal let network: NetTaskerProtocol
    internal let cache: CacheTaskerProtocol
    internal let secret: SecretService
    internal var index = 0
    internal var section = 0
    private let disposeBag = DisposeBag()

    internal struct const {
        static let kCreate = "createId"
        static let kUpdate = "updateId"
    }
}

extension TaskerViewModel: TaskTextDelegateProtocol {
    public func changed(text: String, id: String) {
        // TODO: add me
    }
}

extension TaskerViewModel: TaskButtonDelegateProtocol {
    public func buttonTouched(id: String) {
        // TODO: add me
    }
}

extension TaskerViewModel: TaskTimeDelegateProtocol {
    public func changeTime(id: String) {
        // TODO: add me
    }

    public func changeDuration(id: String) {
        // TODO: add me
    }

    public func changeDate(id: String) {
        // TODO: add me
    }
}
