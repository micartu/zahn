//
//  ChoosePersonViewModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 12.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public protocol ChosenPersonRes {
    func chosen(person: Person)
}

public final class ChoosePersonViewModel: ChooserViewModel {
    // inputs
    /// dispatcher
    public weak var dispatcher: (CommonCoordinator & ChosenPersonRes)? = nil

    /// theme to be used by components
    public var theme: ITheme!

    /// need to update contents
    public var updateData: Variable<Bool> = Variable<Bool>(false)

    // MARK: - Public Methods

    public init() {
    }

    public func getContents() {
        add(text: "Person A")
        add(text: "Person B")
        add(text: "Person C")
        updateData.value = true
    }

    public func selected(indexPath: IndexPath) {
        let selected = indexPath.row
        var m = models[selected]
        let t = m.title
        let p = Person(id: t,
                       login: t,
                       name: t,
                       midname: t,
                       surname: t,
                       phones: [],
                       address: t)
        dispatcher?.chosen(person: p)
        m.focused = true
        removeAllFocuses()
        models[selected] = m
        updateData.value = true
    }

    public func model(for indexPath: IndexPath) -> Any {
        return models[indexPath.row]
    }

    public func modelsCount(for section: Int) -> Int {
        return models.count
    }

    public func title(for section: Int) -> String {
        return ""
    }

    public func sectionsCount() -> Int {
        return 1
    }

    // MARK: - Private

    internal func removeAllFocuses() {
        for i in models.indices {
            models[i].focused = false
        }
    }

    internal func add(text: String) {
        let m = ChooseTextModel(theme: theme,
                                focused: false,
                                id: "",
                                title: text)
        models.append(m)
    }

    /// array of models to be shown
    internal var models = [ChooseTextModel]()
}
