//
//  ChooseTextModel.swift
//  denti
//
//  Created by Michael Artuerhof on 12.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol ChooseTextProtocol: CellCommonProtocol {
    func set(title: String)
    func set(image: String?)
}

public struct ChooseTextModel: CommonCellModel {
    public let theme: ITheme
    public var focused: Bool
    public var id: String
    var title: String

    public func setup(textCell c: ChooseTextProtocol) {
        c.apply(theme: theme)
        c.set(title: title)
        c.set(focused: focused)
        c.set(image: nil)
    }
}
