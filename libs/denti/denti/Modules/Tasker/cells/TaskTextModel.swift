//
//  TaskTextModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol TaskTextDelegateProtocol: class {
    func changed(text: String, id: String)
}

public protocol TaskTextProtocol: CellCommonProtocol {
    func set(delegate: TaskTextDelegateProtocol)
    func set(title: String)
    func set(text: String)
    func set(id: String)
    func set(image: String?)
    func set(editable: Bool)
    func set(err: Bool)
}

public struct TaskTextModel: TaskerCellModel {
    public let theme: ITheme
    public var focused: Bool
    public let id: String
    var delegate: TaskTextDelegateProtocol
    public var err: Bool
    public var editable: Bool
    var image: String?
    var title: String
    var text: String

    public func setup(textCell c: TaskTextProtocol) {
        c.set(id: id)
        c.set(delegate: delegate)
        c.apply(theme: theme)
        c.set(title: title)
        c.set(text: text)
        c.set(image: image)
        c.set(editable: editable)
        c.set(focused: focused)
    }
}
