//
//  TaskTimeModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol TaskTimeDelegateProtocol: class {
    func changeTime(id: String)
    func changeDuration(id: String)
    func changeDate(id: String)
}

public protocol TaskTimeProtocol: CellCommonProtocol {
    func set(delegate: TaskTimeDelegateProtocol)
    func set(start: String)
    func set(duration: String)
    func set(date: String)
    func set(id: String)
}

public struct TaskTimeModel: TaskerCellModel {
    public let theme: ITheme
    public var focused: Bool
    public let id: String
    var delegate: TaskTimeDelegateProtocol
    public var err: Bool
    var start: String
    var duration: String
    var date: String

    public func setup(timeCell c: TaskTimeProtocol) {
        c.set(id: id)
        c.set(delegate: delegate)
        c.apply(theme: theme)
        c.set(start: start)
        c.set(duration: duration)
        c.set(date: date)
        c.set(focused: focused)
    }
}
