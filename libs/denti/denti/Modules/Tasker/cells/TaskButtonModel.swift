//
//  TaskButtonModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol TaskButtonDelegateProtocol: class {
    func buttonTouched(id: String)
}

public protocol TaskButtonProtocol: CellCommonProtocol {
    func set(delegate: TaskButtonDelegateProtocol)
    func set(title: String)
    func set(id: String)
}

public struct TaskButtonModel: TaskerCellModel {
    public let theme: ITheme
    public var focused: Bool
    public let id: String
    var delegate: TaskButtonDelegateProtocol
    public var err: Bool
    var title: String

    public func setup(buttonCell c: TaskButtonProtocol) {
        c.set(id: id)
        c.set(delegate: delegate)
        c.apply(theme: theme)
        c.set(title: title)
        c.set(focused: focused)
    }
}
