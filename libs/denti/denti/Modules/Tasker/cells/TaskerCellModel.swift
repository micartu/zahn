//
//  TaskerCellModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol TaskerCellModel: CommonCellModel {
    var err: Bool { get set }
}
