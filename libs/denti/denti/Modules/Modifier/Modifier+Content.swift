//
//  Modifier+Content.swift
//  denti
//
//  Created by Michael Artuerhof on 10.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

extension ModifierViewModel {
    internal func preparePerson() {
        // put everything in first section
        section = 0
        index = 0
        let creation = (person == nil) ? true : false
        addTextModel(text: person?.name ?? "",
                     editable: creation,
                     title: "Name".localized)
        addTextModel(text: person?.midname ?? "",
                     editable: creation,
                     title: "Midname".localized)
        addTextModel(text: person?.surname ?? "",
                     editable: creation,
                     title: "Surname".localized)
        if creation {
            addTextModel(text: person?.login ?? "",
                         editable: creation,
                         title: "Login".localized)
        }
        if creation || secret.user == person?.login {
            addPassModel(text: secret.password,
                         title: "Password".localized)
        }
        if creation {
            addButton(title: "Create".localized,
                       id: const.kCreate)
        } else {
            addButton(title: "Update".localized,
                      id: const.kUpdate)
        }
        editableItemFocusedAfter(section: 0, row: -1)
    }

    internal func addTextModel(text: String, editable: Bool, title: String) {
        let m = ChangeTextModel(type: .text,
                                focused: false,
                                delegate: self,
                                showPass: true,
                                editable: editable,
                                err: false,
                                theme: theme,
                                text: text,
                                id: "\(section);\(index)",
                                title: title)
        add(model: m)
    }

    internal func addPassModel(text: String, title: String) {
        let m = ChangeTextModel(type: .password,
                                focused: false,
                                delegate: self,
                                showPass: false,
                                editable: false,
                                err: false,
                                theme: theme,
                                text: text,
                                id: "\(section);\(index)",
            title: title)
        add(model: m)
    }

    internal func addButton(title: String, id: String) {
        let m = ModifierButtonModel(theme: theme,
                                    focused: false,
                                    id: "\(section);\(index);\(id)",
                                    delegate: self,
                                    title: title)
        add(model: m)
    }

    internal func add(model: ModifierCellModel) {
        index += 1
        append(model: model, into: section)
    }
}
