//
//  ChangeTextModel.swift
//  denti
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public enum ChangeTextType {
    case text
    case phone
    case password
    case mail
    case birthday
}

public protocol EditableCellDelegateProtocol: class {
    func btnEyeTouched(id: String)
    func btnEditTouched(id: String)
    func editEnded(id: String, text: String)
}

public protocol ChangeTextCell: CellCommonProtocol {
    func set(delegate: EditableCellDelegateProtocol)
    func set(type: ChangeTextType, showPass: Bool)
    func set(title: String)
    func set(text: String)
    func set(id: String)
    func set(editable: Bool, err: Bool)
}

public struct ChangeTextModel: ModifierCellModel {
    let type: ChangeTextType
    public var focused: Bool
    var delegate: EditableCellDelegateProtocol
    var showPass: Bool
    var editable: Bool
    var err: Bool
    public let theme: ITheme
    var text: String
    public let id: String
    let title: String

    public func setup(changeTextCell c: ChangeTextCell) {
        c.apply(theme: theme)
        c.set(delegate: delegate)
        c.set(type: type, showPass: showPass)
        c.set(editable: editable, err: err)
        c.set(title: title)
        c.set(text: text)
        c.set(id: id)
        c.set(focused: focused)
    }
}
