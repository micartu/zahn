//
//  ModifierButtonModel.swift
//  denti
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol ModifierButtonDelegateProtocol: class {
    func buttonTouched(id: String)
}

public protocol ModifierButtonProtocol: CellCommonProtocol {
    func set(delegate: ModifierButtonDelegateProtocol)
    func set(title: String)
    func set(id: String)
}

public struct ModifierButtonModel: ModifierCellModel {
    public let theme: ITheme
    public var focused: Bool
    public let id: String
    var delegate: ModifierButtonDelegateProtocol
    var title: String

    public func setup(buttonCell c: ModifierButtonProtocol) {
        c.set(id: id)
        c.set(delegate: delegate)
        c.apply(theme: theme)
        c.set(title: title)
        c.set(focused: focused)
    }
}
