//
//  Modifier+Update.swift
//  denti
//
//  Created by Michael Artuerhof on 10.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension ModifierViewModel {
    private func isEmpty(text t: String,
                         message msg: String) -> NSError? {
        if t.count > 0 {
            return nil
        } else {
            let err = NSError(domain: Const.domain,
                              code: -1,
                              userInfo: [NSLocalizedDescriptionKey: msg])
            return err
        }
    }

    internal func findPersonInData(checkEmptiness: Bool) -> (Person?, NSError?, String) {
        var updPass = ""
        var name = ""
        var midname = ""
        var surname = ""
        var login = ""
        for (k, _) in models {
            if let mm = models[k] {
                for mraw in mm {
                    if var m = mraw as? ChangeTextModel {
                        if checkEmptiness {
                            if let err = isEmpty(text: m.text,
                                                 message: m.title + ": " + "field should not be empty".localized) {
                                m.err = true
                                m.editable = true
                                removeAllFocuses()
                                m.focused = true
                                updateModel(m)
                                return (nil, err, updPass)
                            }
                        }
                        if m.type == .text {
                            if m.title == "Name".localized {
                                name = m.text
                            }
                            if m.title == "Midname".localized {
                                midname = m.text
                            }
                            if m.title == "Surname".localized {
                                surname = m.text
                            }
                            if m.title == "Login".localized {
                                login = m.text
                            }
                        }
                        if m.type == .password {
                            updPass = m.text
                        }
                    }
                }
            }
        }
        if !checkEmptiness && login.isEmpty {
            login = secret.user
        }
        let p = Person(id: person?.id ?? "",
                       login: login,
                       name: name,
                       midname: midname,
                       surname: surname,
                       phones: [],
                       address: "")
        return (p, nil, updPass)
    }

    internal func createPersonFor(org o: Org, completion: @escaping ((NSError?) -> Void)) {
        let (p, err, updPass) = findPersonInData(checkEmptiness: true)
        if let pers = p {
            network.addPerson(organization: o,
                              creator: secret.user,
                              person: pers,
                              updPassword: updPass,
                              success: { [weak self] p in
                                // add it to cache
                                self?.cache.update(person: p) {
                                    completion(nil)
                                }
                }, failure: { e in
                    completion(e)
            })
        } else {
            completion(err)
        }
    }



    internal func update(person p: Person, org o: Org, completion: @escaping ((NSError?) -> Void)) {
        let (p, _, updPass) = findPersonInData(checkEmptiness: false)
        network.updatePersonInfo(organization: o,
                                 creator: secret.user,
                                 person: p!,
                                 updPassword: updPass,
                                 success: {
                                    completion(nil)
        }, failure: { e in
            completion(e)
        })
    }
}
