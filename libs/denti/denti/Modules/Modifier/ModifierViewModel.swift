//
//  ModifierViewModel.swift
//  denti
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public enum ModifierMode {
    case person
}

public protocol NetModifierProtocol:
    NetPersonService {}

public protocol CacheModifierProtocol:
    CacheOrgService,
    CachePersonService {}

final public class ModifierViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: CommonCoordinator? = nil

    /// current application theme
    public var theme: ITheme!

    /// what will the model do?
    public var mode: ModifierMode = .person

    /// person's info to be edited
    public var person: Person? = nil

    /// person's organization
    public var org: Org? = nil

    // MARK: - Outputs

    /// update data shown to user
    public let updateData = Variable<Bool>(false)

    /// some data was changed
    public let dataChanged = Variable<Bool>(false)

    // MARK: - init
    public init(network: NetModifierProtocol,
                secret: SecretService,
                cache: CacheModifierProtocol) {
        self.network = network
        self.secret = secret
        self.cache = cache
    }

    // MARK: - public methods
    public func updateEnteredData(completion: @escaping ((NSError?) -> Void)) {
        if let o = org,
            let p = person {
            // update given person's info:
            update(person: p, org: o, completion: completion)
        } else if let o = org {
            // creation mode, gather all data and create a person out of it
            createPersonFor(org: o, completion: completion)
        }
        else {
            completion(nil)
        }
    }

    public func getContents() {
        switch mode {
        case .person:
            preparePerson()
        }
        updateData.value = true
    }

    public func model(for indexPath: IndexPath) -> ModifierCellModel {
        let m = models[indexPath.section]
        return m![indexPath.row]
    }

    public func modelsCount(for section: Int) -> Int {
        if let m = models[section] {
            return m.count
        }
        return 0
    }

    public func title(for section: Int) -> String {
        return "Section \(section)" // TODO: add normal section title here
    }

    public func sectionsCount() -> Int {
        return models.count
    }

    // MARK: - Private

    internal func append(model: ModifierCellModel, into section: Int) {
        if models[section] == nil {
            models[section] = [ModifierCellModel]()
        }
        models[section]?.append(model)
    }

    internal func separateId(_ id: String) -> (Int, Int) {
        let ids = id.split(separator: ";").map({String($0)})
        assert(ids.count >= 2)
        let section = ids[0].toInt
        let row = ids[1].toInt
        return (section, row)
    }

    internal func findModel(with id: String) -> ModifierCellModel? {
        let (section, i) = separateId(id)
        if let s = models[section] {
            if i < s.count {
                return s[i]
            }
        }
        return nil
    }

    internal func updateModel(_ m: ModifierCellModel) {
        let (section, i) = separateId(m.id)
        models[section]?[i] = m
        updateData.value = true
    }

    internal func editableItemFocusedAfter(section s: Int, row r: Int) {
        for i in 0..<models.count {
            if let mm = models[i] {
                for j in mm.indices {
                    if i <= s && j <= r {
                        models[i]?[j].focused = false
                    } else {
                        if let ch = mm[j] as? ChangeTextModel,
                            ch.editable {
                            models[i]?[j].focused = true
                            // mission acomplished
                            return
                        }
                    }
                }
            }
        }
    }

    internal func removeAllFocuses() {
        for i in 0..<models.count {
            if let mm = models[i] {
                for j in mm.indices {
                    models[i]?[j].focused = false
                }
            }
        }
    }

    /// array of models to be shown
    internal var models = [Int:[ModifierCellModel]]()
    internal let network: NetModifierProtocol
    internal let cache: CacheModifierProtocol
    internal let secret: SecretService
    internal var index = 0
    internal var section = 0
    private let disposeBag = DisposeBag()

    internal struct const {
        static let kCreate = "createId"
        static let kUpdate = "updateId"
    }
}

extension ModifierViewModel: EditableCellDelegateProtocol {
    public func btnEyeTouched(id: String) {
        if var m = findModel(with: id) as? ChangeTextModel {
            m.showPass = !m.showPass
            updateModel(m)
        }
    }

    public func btnEditTouched(id: String) {
        if var m = findModel(with: id) as? ChangeTextModel {
            m.editable = !m.editable
            removeAllFocuses()
            m.focused = true
            updateModel(m)
        }
    }

    public func editEnded(id: String, text: String) {
        if var m = findModel(with: id) as? ChangeTextModel {
            let (s, r) = separateId(id)
            m.focused = false
            m.err = false
            m.editable = !m.editable
            m.text = text
            editableItemFocusedAfter(section: s, row: r)
            updateModel(m)
            dataChanged.value = true
        }
    }
}

extension ModifierViewModel: ModifierButtonDelegateProtocol {
    public func buttonTouched(id: String) {
        if id.contains(const.kCreate) || id.contains(const.kUpdate) {
            dispatcher?.showBusyIndicator()
            updateEnteredData { [weak self] e in
                self?.dispatcher?.hideBusyIndicator()
                if let err = e {
                    self?.dispatcher?.show(error: err.localizedDescription)
                } else {
                    // person was created, nothing more here to and time to exit bro!
                    if id.contains(const.kCreate) {
                        self?.dispatcher?.exitWith(result: nil)
                    }
                }
            }
        }
    }
}
