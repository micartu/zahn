//
//  LoginViewModel.swift
//  denti
//
//  Created by michael on 13.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public protocol NetLoginProtocol:
    NetOrgService,
    NetPersonService,
    NetLoginService {}

public protocol CacheLoginProtocol:
    CacheOrgService,
    CacheOrgPersonService,
    CachePersonService {}

public struct loginOut {
    public let person: Person
    public let org: Org
}

final public class LoginViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: CommonCoordinator? = nil

    /// login text
    public let user = Variable<String>("")

    /// password text
    public let password = Variable<String>("")

    /// user tapped login button try to proceed
    public let performLogin = PublishSubject<Void>()

    // MARK: - Outputs

    // MARK: - init

    public init(network: NetLoginProtocol,
                secrets: SecretService,
                cache: CacheLoginProtocol) {
        self.network = network
        self.secrets = secrets
        self.cache = cache
        bind()
    }

    // MARK: - Private

    private func bind() {
        performLogin.asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                let ulogin = self.user.value
                let pass = self.password.value
                self.dispatcher?.showBusyIndicator()
                self.secrets.erase() // remove old secret info
                self.network.changeCredentials(login: ulogin,
                                               password: pass,
                                               success: {
                                                self.dispatcher?.hideBusyIndicator()
                                                self.checkOrgAndPerson(login: ulogin) {
                                                    if let p = self.cache.get(person: ulogin),
                                                        let o = self.cache.get(org: self.orgId) {
                                                        let out = loginOut(person: p, org: o)
                                                        self.secrets.user = ulogin
                                                        self.secrets.password = pass
                                                        self.dispatcher?.exitWith(result: out)
                                                    } else {
                                                        self.dispatcher?.show(error: "Org/User were not found in cache!".localized)
                                                    }
                                                }
                }, failure: { e in
                    self.dispatcher?.hideBusyIndicator()
                    self.dispatcher?.show(error: e.localizedDescription)
                })
            })
            .disposed(by: disposeBag)
    }

    private func checkOrgAndPerson(login: String, completion: @escaping (() -> Void)) {
        dispatcher?.showInputDialog(title: "First time login".localized,
                                    message: "Name your organization:".localized,
                                    okAction: { [weak self] orgName in
                                        self?.dispatcher?.showBusyIndicator()
                                        self?.network.getOrganizations(like: orgName,
                                                                       for: login, success: { orgs in
                                                                        self?.dispatcher?.hideBusyIndicator()
                                                                        if orgs.count > 1 {
                                                                            self?.chooseFrom(orgs: orgs, login: login, completion: completion)
                                                                        } else if orgs.count == 1 {
                                                                            self?.checkPerson(login: login,
                                                                                              for: orgs.first!,
                                                                                              completion: completion)
                                                                        } else {
                                                                            self?.dispatcher?.show(error: "Organization isn't found".localized)
                                                                        }
                                        } , failure: { e in
                                            self?.dispatcher?.hideBusyIndicator()
                                            self?.dispatcher?.show(error: e.localizedDescription)
                                        })
        })
    }

    private func chooseFrom(orgs: [Org], login: String, completion: @escaping (() -> Void)) {
        var titles = [String]()
        for o in orgs {
            titles.append(o.name)
        }
        dispatcher?.chooseFrom(titles: titles,
                               title: "Choose your organization".localized,
                               okAction: { [weak self] (_, index) in
                                if index < orgs.count {
                                    self?.checkPerson(login: login,
                                                      for: orgs[index],
                                                      completion: completion)
                                }
        })
    }

    private func checkPerson(login: String, for org: Org, completion: @escaping (() -> Void)) {
        dispatcher?.showBusyIndicator()
        let attachOrgTo = { [weak self] (p: Person) in
            self?.cache.attach(org: org, to: p) {
                self?.orgId = org.id
                completion()
            }
        }
        if let p = cache.get(person: login) {
            dispatcher?.hideBusyIndicator()
            attachOrgTo(p)
        } else {
            network.getPersonInfo(organization: org,
                                  requester: login,
                                  requested: login, success: { [weak self] p in
                                    self?.dispatcher?.hideBusyIndicator()
                                    self?.cache.update(org: org) {
                                        // detach from organization will be performed somewhere in profile page
                                        attachOrgTo(p)
                                    }
                }, failure: { [weak self] e in
                    self?.dispatcher?.hideBusyIndicator()
                    self?.dispatcher?.show(error: e.localizedDescription)
            })
        }
    }

    private var orgId = ""
    private let network: NetLoginProtocol
    private var secrets: SecretService
    private let cache: CacheLoginProtocol
    private let disposeBag = DisposeBag()
}
