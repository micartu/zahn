//
//  ClinicModel.swift
//  denti
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol ClinicCell: Themeble {
    func set(title: String)
    func set(notes: String)
    func set(address: String)
}

public struct ClinicModel {
    let theme: ITheme
    let clinic: Clinic

    public func setup(clinicCell c: ClinicCell) {
        c.apply(theme: theme)
        c.set(title: clinic.name)
        c.set(notes: "")
        c.set(address: clinic.address)
    }
}
