//
//  ListClinicsViewModel.swift
//  denti
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public protocol NetClinicsProtocol:
    NetPersonService,
    NetClinicService { }

public protocol CacheClinicsProtocol:
    CacheOrgService,
    CacheClinicService,
    CachePersonService {}

final public class ListClinicsViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: CommonCoordinator? = nil

    /// current application theme
    public var theme: ITheme!

    // MARK: - Outputs

    /// update data shown to user
    public let updateData = Variable<Bool>(false)

    // MARK: - init
    public init(network: NetClinicsProtocol,
                person: Person,
                org: Org,
                cache: CacheClinicsProtocol) {
        self.network = network
        self.cache = cache
        self.person = person
        self.org = org
        bind()
    }

    // MARK: - public methods

    public func getContents() {
        models.removeAll()
        let clis = cache.clinicsFor(personId: person.id)
        for c in clis {
            let model = ClinicModel(theme: theme,
                                    clinic: c)
            append(model: model, into: 0)
        }
        updateData.value = true
        getContentsFromNetwork()
    }

    public func model(for indexPath: IndexPath) -> ClinicModel {
        let m = models[indexPath.section]
        return m![indexPath.row]
    }

    public func modelsCount(for section: Int) -> Int {
        if let m = models[section] {
            return m.count
        }
        return 0
    }

    public func title(for section: Int) -> String {
        return ""
    }

    public func sectionsCount() -> Int {
        return 1
    }

    // MARK: - Private

    private func bind() {
        // ..nothing to bind?..
    }

    private func getContentsFromNetwork() {
        let now = Date()
        let fetchAndUpdate = { [weak self] in
            guard let `self` = self else { return }
            self.latestNetwokUpdate = now.addingTimeInterval(const.kUpdateTime)
            if self.firstTime {
                self.dispatcher?.showBusyIndicator()
            }
            self.sync()
        }
        if latestNetwokUpdate == nil {
            fetchAndUpdate()
        } else if let nUpdate = latestNetwokUpdate, nUpdate < now {
            fetchAndUpdate()
        }
    }

    private func sync() {
        let resetFirstTime = { [weak self] in
            guard let `self` = self else { return }
            if self.firstTime {
                self.firstTime = false
            }
            self.dispatcher?.hideBusyIndicator()
        }
        if firstTime {
            dispatcher?.showBusyIndicator()
        }
        network.getClinicsRolesFor(organization: org, and: person.id,
                                   success: { [weak self] clinics in
                                    guard let `self` = self else { return }
                                    let g = DispatchGroup()
                                    if let c = clinics.last {
                                        if !self.cache.exists(clinic: c.clinic.id) {
                                            for cc in clinics {
                                                g.enter()
                                                self.cache.update(clinic: cc.clinic) {
                                                    g.leave()
                                                }
                                                for r in cc.roles {
                                                    let rr = Role(id: r, name: r, right: r)
                                                    g.enter()
                                                    self.cache.add(role: rr, for: self.person, in: cc.clinic.id) {
                                                        g.leave()
                                                    }
                                                }
                                            }
                                            g.notify(queue: .main) {
                                                resetFirstTime()
                                                self.getContents()
                                            }
                                        }
                                        else {
                                            resetFirstTime()
                                        }
                                    }
        }, failure: { e in
            print("!cannot get clinics from network: \(e.localizedDescription)")
            resetFirstTime()
        })
    }

    private func append(model: ClinicModel, into section: Int) {
        if models[section] == nil {
            models[section] = [ClinicModel]()
        }
        models[section]?.append(model)
    }

    /// array of models to be shown
    private var models = [Int:[ClinicModel]]()
    /// are we trying to download contents for the first time?
    private var firstTime = false

    /// time to update messages from network?
    private var latestNetwokUpdate: Date? = nil

    private let network: NetClinicsProtocol
    private let person: Person
    private let org: Org
    private let cache: CacheClinicsProtocol
    private let disposeBag = DisposeBag()

    struct const {
        static let kUpdateTime: TimeInterval = 30
    }
}
