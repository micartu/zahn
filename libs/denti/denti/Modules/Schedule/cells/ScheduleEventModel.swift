//
//  ScheduleEventModel.swift
//  denti
//
//  Created by Michael Artuerhof on 31.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

public protocol ScheduleCell: Themeble {
    func set(title: String)
    func set(color: IColor)
    func set(time: String)
}

public struct ScheduleEventModel {
    public let theme: ITheme
    public let color: IColor
    public let title: String
    public let timespan: NSRange
    public let timeDiv: Int

    public func setup(scheduleCell c: ScheduleCell) {
        c.apply(theme: theme)
        c.set(title: title)
        c.set(color: color)
        let hour = timespan.location / timeDiv
        let minute = timespan.location % timeDiv
        c.set(time: String(format: "%02ld:%02ld", hour, minute))
    }
}
