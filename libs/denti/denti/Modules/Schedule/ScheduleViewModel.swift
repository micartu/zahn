//
//  ScheduleViewModel.swift
//  denti
//
//  Created by Michael Artuerhof on 31.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

public enum calendarState {
    case opened
    case closed
}

public protocol NetScheduleProtocol:
    NetClinicService,
    NetTaskService { }

public protocol CacheScheduleProtocol:
    CacheTaskService,
    CacheChairService,
    CacheClinicChairService,
    CacheClinicPersonTaskService,
    CacheClinicService,
    CachePersonColorService,
    CachePersonService {}

final public class ScheduleViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: CommonCoordinator? = nil

    /// current application theme
    public var theme: ITheme!

    /// if set then we'll grab all information
    /// for given person and organization
    public var org: Org? = nil

    /// if set then we'll grab all information for given person
    /// also here we should set organization
    public var person: Person? = nil

    /// we want gather information about clinics (ids to fetch)
    /// could be set with org=nil and person = nil!
    public var clinics = [String]()

    /// we want gather information about chairs (ids to fetch)
    /// could be set with org=nil and person = nil!
    public var chairs = [String]()

    /// user tapped top calendar panel
    public let showHideCalendar = PublishSubject<Void>()

    /// time diviator for events:
    public let timeDiv: Int = 100

    /// date on which we'd like to gather information
    public var fetchDate = Date() {
        didSet {
            // reset all previous attempts for getting data
            // and try it again:
            firstTime = true
            latestNetwokUpdate = nil
            getContentsFromNetwork()
        }
    }

    // MARK: - Outputs

    /// calendar state
    public let calState = Variable<calendarState>(.closed)

    /// update data shown to user
    public let updateData = Variable<Bool>(false)

    // MARK: - init
    public init(network: NetScheduleProtocol,
                cache: CacheScheduleProtocol) {
        self.network = network
        self.cache = cache
        queue = DispatchQueue(label: Const.dqueue + ".ScheduleViewModel",
                              qos: .userInitiated,
                              attributes: .concurrent)
        bind()
    }

    // MARK: - public methods

    public func getContents() {
        models.removeAll()
        titles.removeAll()
        if let p = person {
            var update = false
            let clinics = cache.clinicsFor(personId: p.id)
            let tillDate = Date(timeInterval: const.kDay, since: fetchDate)
            let cal = Calendar.current
            for c in clinics {
                let chairs = cache.chairs(clinicId: c.id)
                for cci in chairs.indices {
                    let cc = chairs[cci]
                    let tasks = cache.tasks(for: cc, dateStart: fetchDate, dateEnd: tillDate)
                    let nsection = "\(c.name) - \(cc.name)" // clinic - chair
                    for t in tasks {
                        let hour = cal.component(.hour, from: t.time)
                        let minute = cal.component(.minute, from: t.time)
                        let color: IColor
                        if let creator = cache.creatorOfTask(tid: t.id) {
                            if let c = cache.getColor(for: creator) {
                                color = c
                            } else {
                                color = defIColor()
                                cache.set(color: color, for: creator) { }
                            }
                        } else {
                            color = defIColor()
                        }
                        let m = ScheduleEventModel(theme: theme,
                                                   color: color,
                                                   title: t.name,
                                                   timespan: NSRange(location: hour * timeDiv + minute,
                                                                     length: Int(t.duration * Double(timeDiv))),
                                                   timeDiv: timeDiv)
                        append(model: m,
                               into: cci, // put every chair in different section
                               sectionName: nsection)
                        update = true
                    }
                }
            }
            updateData.value = update
        }
        getContentsFromNetwork()
        readCalendarState()
    }

    public func model(for indexPath: IndexPath) -> ScheduleEventModel {
        let m = models[indexPath.section]
        return m![indexPath.row]
    }

    public func modelsCount(for section: Int) -> Int {
        if let m = models[section] {
            return m.count
        }
        return 0
    }

    public func title(for section: Int) -> String {
        if section < titles.count {
            return titles[section]
        }
        return "?"
    }

    public func sectionsCount() -> Int {
        return models.count
    }

    // MARK: - Private

    private func defIColor() -> IColor {
        return IColor(r: 255,
                      g: 0,
                      b: 0,
                      a: 1)
    }

    private func append(model: ScheduleEventModel, into section: Int, sectionName: String) {
        if models[section] == nil {
            models[section] = [ScheduleEventModel]()
            if section >= titles.count {
                for _ in 0...(section - titles.count) {
                    titles.append("")
                }
            }
            titles[section] = sectionName
        }
        models[section]?.append(model)
    }

    private func getContentsFromNetwork() {
        let now = Date()
        let fetchAndUpdate = { [weak self] in
            guard let `self` = self else { return }
            self.latestNetwokUpdate = now.addingTimeInterval(const.kUpdateTime)
            if self.firstTime {
                self.dispatcher?.showBusyIndicator()
            }
            self.sync()
        }
        if latestNetwokUpdate == nil {
            fetchAndUpdate()
        } else if let nUpdate = latestNetwokUpdate, nUpdate < now {
            fetchAndUpdate()
        }
    }

    private func sync() {
        let resetFirstTime = {
            if self.firstTime {
                self.firstTime = false
                self.dispatcher?.hideBusyIndicator()
            }
        }
        /*
        if chairs.count > 0 {
            // get info for chairs
            // TODO: add network.getTasksFor(chairId: ...) like this one:
            // network.getTasksFor(clinicId: from: till:)
            return // do not load anything else
        }
        */
        if let o = org,
            let p = person {
            // first we have to find all the clinics for the given person
            // in cache or in network...
            network.getClinicsFor(organization: o, and: p.id,
                                  success: { [weak self] clinics in
                                    if clinics.count > 0 {
                                        var failure: NSError? = nil
                                        let lock = NSLock()
                                        let g = DispatchGroup()
                                        for c in clinics {
                                            g.enter()
                                            self?.getChairs(for: c.id) { chairs, err in
                                                g.leave()
                                                if let e = err {
                                                    lock.lock()
                                                    failure = e
                                                    lock.unlock()
                                                } else {
                                                    guard let `self` = self else { return }
                                                    for cc in chairs! {
                                                        if !self.chairs.contains(cc.id) {
                                                            self.chairs.append(cc.id)
                                                        }
                                                    }
                                                }
                                            }
                                            g.enter()
                                            self?.getTasks(for: c.id) { _, err in
                                                g.leave()
                                                if let e = err {
                                                    lock.lock()
                                                    failure = e
                                                    lock.unlock()
                                                }
                                            }
                                        }
                                        g.notify(queue: .main) {
                                            self?.dispatcher?.hideBusyIndicator()
                                            if let e = failure {
                                                self?.dispatcher?.show(error: e.localizedDescription)
                                            } else {
                                                resetFirstTime()
                                                // refresh models
                                                self?.getContents()
                                            }
                                        }
                                    } else {
                                        // no clinics, nothing to show, do not call network again:
                                        resetFirstTime()
                                    }
            }, failure: { [weak self] e in
                self?.dispatcher?.hideBusyIndicator()
                self?.dispatcher?.show(error: e.localizedDescription)
            })
        }
    }

    private func getChairs(for cid: String, completion: @escaping (([Chair]?, NSError?) -> Void)) {
        network.getInfoFor(clinicId: cid, success: { [weak self] cinfo in
            guard let `self` = self else { return }
            let g = DispatchGroup()
            for c in cinfo.chairs {
                if !self.cache.exists(chair: c.id) {
                    g.enter()
                    self.cache.create(chair: c) {
                        g.leave()
                    }
                }
            }
            g.notify(queue: self.queue) {
                completion(cinfo.chairs, nil)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }

    private func getTasks(for cid: String, completion: @escaping (([Task]?, NSError?) -> Void)) {
        let tillDate = Date(timeInterval: const.kDay, since: fetchDate)
        network.getTasksFor(clinicId: cid, from: fetchDate,
                            till: tillDate,
                            success: { [weak self] tinfo in
                                guard let `self` = self else { return }
                                var tasks = [Task]()
                                let g = DispatchGroup()
                                for t in tinfo {
                                    g.enter()
                                    self.cache.attach(task: t.task,
                                                      chairId: t.chairId,
                                                      pid: t.creatorId,
                                                      forPid: t.forPersonId) {
                                        g.leave()
                                    }
                                    tasks.append(t.task)
                                }
                                g.notify(queue: self.queue) {
                                    completion(tasks, nil)
                                }
        }, failure: { e in
            completion(nil, e)
        })
    }

    private func bind() {
        showHideCalendar.asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                let state = self.calState.value
                let def = UserDefaults.standard
                let opened: Bool
                // reverse state:
                if state == .opened {
                    opened = false
                } else {
                    opened = true
                }
                def.set(opened, forKey: const.kCalendarState)
                def.synchronize()
                self.setCalendar(opened)
            })
            .disposed(by: disposeBag)
    }

    private func readCalendarState() {
        let def = UserDefaults.standard
        let opened: Bool
        if let _opened = def.object(forKey: const.kCalendarState) as? Bool {
            opened = _opened
        } else {
            opened = false
        }
        setCalendar(opened)
    }

    private func setCalendar(_ opened: Bool) {
        calState.value = opened ? .opened : .closed
    }

    private struct const {
        static let kCalendarState = "SchedulerCalendarState"
        static let kDay: TimeInterval = 60 * 60 * 24
        static let kUpdateTime: TimeInterval = 60 * 2
    }

    private let network: NetScheduleProtocol
    private let cache: CacheScheduleProtocol
    /// titles for sections (clinic/chair title)
    private var titles = [String]()
    /// array of models to be shown
    private var models = [Int:[ScheduleEventModel]]()
    /// time to update messages from network?
    private var latestNetwokUpdate: Date? = nil
    /// are we trying to download contents from network for the first time?
    private var firstTime = true
    private let disposeBag = DisposeBag()
    private let queue: DispatchQueue
}
