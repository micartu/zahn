//
//  AuthViewModel.swift
//  denti
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import RxSwift

enum AuthExit {
    case logout
}

final public class AuthViewModel {
    // MARK: - Inputs
    /// dispatcher
    public weak var dispatcher: CommonCoordinator? = nil

    /// user wants to be authorized by his fingerprints
    public let performFingerAuth = PublishSubject<Void>()

    /// user wants to logout
    public let performPassReset = PublishSubject<Void>()

    /// password text
    public let pass = Variable<String>("")

    // MARK: - Outputs

    /// if we shouldn't show fingerprints button
    public let fingerprintHidden = Variable<Bool>(true)

    /// reset already entered text
    public  let resetInput = Variable<Bool>(false)

    public init(secrets: SecretService) {
        self.secrets = secrets
        fingerprintHidden.value = !secrets.canAccessKeychain()
        bind()
    }

    // MARK: - Private

    private func checkMagic() {
        if secrets.magic == Const.domain {
            resetInput.value = false
            fingerprintHidden.value = true
            dispatcher?.exitWith(result: nil)
        } else {
            resetInput.value = true
        }
    }

    private func bind() {
        pass.asObservable()
            .subscribe(onNext: { [weak self] newPass in
                guard let sself = self else { return }
                sself.secrets.pwd = newPass
                sself.checkMagic()
            })
            .disposed(by: disposeBag)

        performFingerAuth.asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                if self.secrets.restoreKeyFromEnclave() {
                    self.checkMagic()
                }
            })
            .disposed(by: disposeBag)

        performPassReset.asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                self.dispatcher?.showYesNO(title: "Reset the PIN".localized,
                                           message: "Are you sure?".localized,
                                           actionYes:{
                                            self.dispatcher?.exitWith(result: AuthExit.logout)
                }, actionNo: nil)
            })
            .disposed(by: disposeBag)
    }


    private var secrets: SecretService
    private let disposeBag = DisposeBag()
}
