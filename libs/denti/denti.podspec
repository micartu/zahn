Pod::Spec.new do |s|
	s.platform = :ios
	s.ios.deployment_target = '9.3'
	s.name             = 'denti'
	s.version          = '0.1.0'
	s.summary          = 'platform independent business logic for denti app'
	s.requires_arc 	   = true

	s.description      = <<-DESC
Platform independent business logic for dental app
	DESC

	s.homepage         = 'https://github.com/micartu/denti'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'micartu' => 'michael.artuerhof@gmail.com' }
	s.source           = { :git => 'https://github.com/micartu/denti.git', :tag => s.version.to_s }

	# s.ios.deployment_target = '9.3'

	s.source_files = 'denti/**/*.{swift}'

	s.frameworks = 'Foundation'
	s.dependency 'denticom', '~> 0.1.0'
	s.dependency 'RxSwift', '~> 4.3.1'
	s.swift_version = "4.2"
end
