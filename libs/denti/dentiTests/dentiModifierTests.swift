//
//  dentiModifierTests.swift
//  dentiTests
//
//  Created by Michael Artuerhof on 10.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import XCTest
import denticom
import dcache
import RxSwift
@testable import denti

class dentiModifierTests: XCTestCase {
    var cache: Cache!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testCreationMode() {
        let exp = expectation(description: "ModifierViewModel Creation")
        let net = FakeModifierSimpleNetwork()
        let modifier = ModifierViewModel(network: net,
                                         secret: MockSecrets(),
                                         cache: cache)
        modifier.updateData.asObservable()
            .subscribe(onNext: { upd in
                if upd {
                    let sections = modifier.sectionsCount()
                    XCTAssertTrue(sections > 0, "but it was: \(sections)")
                    let m1 = modifier.model(for: IndexPath(item: 0, section: 0)) as! ChangeTextModel
                    XCTAssertTrue(m1.editable == true, "but it was: \(m1.editable)")
                    XCTAssertTrue(m1.type == .text, "but it was: \(m1.type)")
                    XCTAssertTrue(m1.text == "", "but it was: \(m1.text)")
                    exp.fulfill()
                }
            })
            .disposed(by: disposeBag)
        modifier.theme = Theme(name: "test")
        modifier.getContents()
        wait(for: [exp], timeout: kTimeout)
    }

    func testUpdateMode() {
        let exp = expectation(description: "ModifierViewModel Update Data")
        let net = FakeModifierSimpleNetwork()
        let secret = MockSecrets()
        let modifier = ModifierViewModel(network: net,
                                         secret: secret,
                                         cache: cache)
        let kUsr = "_test_usr_"
        var m: ChangeTextModel? = nil
        var count = 0
        modifier.updateData.asObservable()
            .subscribe(onNext: { upd in
                if upd {
                    let sections = modifier.sectionsCount()
                    if m == nil {
                        for i in 0..<modifier.modelsCount(for: 0) {
                            if let model = modifier.model(for: IndexPath(item: i, section: 0)) as? ChangeTextModel {
                                if model.type == .password {
                                    m = model
                                    break
                                }
                            }
                        }
                    }
                    XCTAssertTrue(sections > 0, "but it was: \(sections)")
                    let m1 = modifier.model(for: IndexPath(item: 0, section: 0)) as! ChangeTextModel
                    XCTAssertTrue(m1.editable == false, "but it was: \(m1.editable)")
                    XCTAssertTrue(m1.type == .text, "but it was: \(m1.type)")
                    XCTAssertTrue(m1.text == kUsr, "but it was: \(m1.text)")
                    if let mm = m {
                        if count == 0 {
                            m = nil
                            count += 1
                            XCTAssertTrue(mm.showPass == false, "but it was: \(mm)")
                            modifier.btnEyeTouched(id: mm.id)
                            // wait till next update
                        } else if count == 1 {
                            XCTAssertTrue(m?.showPass == true,
                                          "but it was: \(String(describing: m))")
                            exp.fulfill()
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
        secret.user = kUsr
        secret.password = kUsr
        modifier.theme = Theme(name: "test")
        modifier.person = Person(id: kUsr,
                                 login: kUsr,
                                 name: kUsr,
                                 midname: "",
                                 surname: "",
                                 phones: [],
                                 address: "")
        modifier.getContents()
        wait(for: [exp], timeout: kTimeout)
    }

    // MARK: - Private
    private let disposeBag = DisposeBag()
}

class FakeModifierSimpleNetwork: DummyNet {
    override init() {
        super.init()
    }
}

class MockSecrets: SecretService {
    var authenticated: Bool = false
    var isSocialAuth: Bool = false
    var socialAuthType: String = ""
    var socialAuthToken: String = ""
    var socialAuthTokenId: String = ""
    var socialAuthExpire: Date? = nil
    var authorized: Bool = false
    var isEncrypted: Bool = false
    var isAuthCreated: Bool = false
    var pwd: String = ""
    var magic: String = ""
    var session: String = ""
    var user: String = ""
    var password: String = ""
    var cardId: String = ""
    var uid: String = ""
    func canAccessKeychain() -> Bool {
        return false
    }

    func restoreKeyFromEnclave() -> Bool {
        return true
    }

    func saveKeyToEnclave() {
    }

    func erase() {
    }

    func encrypt(_ s: String) -> String {
        return s
    }

    func decrypt(_ s: String) -> String {
        return s
    }
}

extension FakeModifierSimpleNetwork: NetModifierProtocol { }
extension Cache: CacheModifierProtocol { }
