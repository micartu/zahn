//
//  DummyNet.swift
//  dentiTests
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom
@testable import denti

protocol NetAsWhole:
    NetTaskService,
    NetOrgService,
    NetPersonService,
    NetClinicService,
    NetLoginService {}

/// network class which does nothing
/// designed for overriding only
open class DummyNet: NetAsWhole {
    public func getTasksFor(clinicId: String,
                            from dateStart: Date,
                            till dateEnd: Date,
                            success: (([ClinicTask]) -> Void)?,
                            failure: ((NSError) -> Void)?) {
    }

    public func getOrganizations(like name: String,
                                 for user: String,
                                 success: (([Org]) -> Void)?,
                                 failure: ((NSError) -> Void)?) {
    }

    public func getPersonInfo(organization: Org,
                              requester rlogin: String,
                              requested ulogin: String,
                              success: ((Person) -> Void)?,
                              failure: ((NSError) -> Void)?) {
    }
    
    public func updatePersonInfo(organization: Org,
                          creator: String,
                          person p: Person,
                          updPassword: String,
                          success: (() -> Void)?,
                          failure: ((NSError) -> Void)?) {
    }
    
    public func addPerson(organization: Org,
                   creator: String,
                   person p: Person,
                   updPassword: String,
                   success: ((Person) -> Void)?,
                   failure: ((NSError) -> Void)?) {
    }

    public func getClinicsFor(organization: Org,
                              and login: String,
                              success: (([Clinic]) -> Void)?,
                              failure: ((NSError) -> Void)?) {
    }

    public func getClinicsRolesFor(organization: Org,
                                   and login: String,
                                   success: (([ClinicPersonRoles]) -> Void)?,
                                   failure: ((NSError) -> Void)?) {
    }

    public func getInfoFor(clinicId: String,
                           success: ((ClinicInfo) -> Void)?,
                           failure: ((NSError) -> Void)?) {
    }

    public func changeCredentials(login: String,
                                  password: String,
                                  success: (() -> Void)?,
                                  failure: ((NSError) -> Void)?) {
    }

    public func logout(success: (() -> Void)?,
                       failure: ((NSError) -> Void)?) {
    }
}
