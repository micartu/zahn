//
//  common.swift
//  dentiTests
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom
import dcache
@testable import denti

let kTimeout: TimeInterval = 5
let kTestUser = "test_user_login"

extension Cache: CacheScheduleProtocol { }
