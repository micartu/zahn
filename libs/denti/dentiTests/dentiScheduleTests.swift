//
//  dentiScheduleTests.swift
//  dentiScheduleTests
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
import denticom
import dcache
import RxSwift
@testable import denti

class dentiScheduleTests: XCTestCase {
    var cache: Cache!
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        cache = Cache()
        dataInit()
        let _ = sys.initModule()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testGetInfoFromNetwork() {
        let exp = expectation(description: "ScheduleViewModel Creation")
        let net = FakeClinicTaskSimpleNetwork(clinics: [cinfo.clinic],
                                              info: [cinfo],
                                              tasks: [ctask])
        let scheduler = ScheduleViewModel(network: net,
                                          cache: cache)
        scheduler.updateData.asObservable()
            .subscribe(onNext: { upd in
                if upd {
                    let id = const.kId
                    // check if model was created for showing it to the user:
                    let count = scheduler.modelsCount(for: 0)
                    XCTAssert(count == 1, "but it was: \(count)")
                    let sections = scheduler.sectionsCount()
                    XCTAssert(sections == 1, "but it was: \(sections)")
                    let title = scheduler.title(for: 0)
                    XCTAssert(title == "\(id) - \(id)", "but it was: \(title)")
                    // then check if task was created in cache:
                    let task = self.cache.get(task: id)
                    XCTAssert(task?.id == id, "but it was: \(String(describing: task))")
                    self.cleaningUp(exp: exp)
                }
            })
            .disposed(by: disposeBag)
        createClinicTaskPerson {
            scheduler.org = self.org
            scheduler.person = self.person
            scheduler.fetchDate = self.curDate
            scheduler.theme = Theme(name: const.kId)
            // first try to grab data...
            scheduler.getContents()
            //..and hope that we'll fetch it from network
        }
        wait(for: [exp], timeout: kTimeout)
    }

    private func createClinicTaskPerson(completion: @escaping (()->Void)) {
        let id = const.kId
        let chair = Chair(id: id,
                          name: id,
                          descr: id)
        let c = Clinic(id: id,
                       name: id,
                       address: "none")
        let r = Role(id: id,
                     name: "clinic",
                     right: "clinic")
        cache.create(org: org) {
            self.cache.create(person: self.person) {
                self.cache.attach(chair: chair, to: c) {
                    self.cache.add(role: r, for: self.person, in: c.id) {
                        completion()
                    }
                }
            }
        }
    }

    private func dataInit() {
        let id = const.kId
        let kDuration: Double = 2.5
        let chair = Chair(id: id,
                          name: id,
                          descr: id)
        let c = Clinic(id: id,
                       name: id,
                       address: "none")
        curDate = Date()
        let t = Task(id: id,
                     name: id,
                     time: curDate,
                     duration: kDuration,
                     descr: id)
        org = Org(id: id,
                  name: "Org",
                  notes: id,
                  address: id)
        person = Person(id: kTestUser,
                        login: kTestUser,
                        name: "john",
                        midname: "vas",
                        surname: "smith",
                        phones: [],
                        address: "none")
        ctask = ClinicTask(task: t,
                           chairId: id,
                           creatorId: kTestUser,
                           forPersonId: kTestUser)
        cinfo = ClinicInfo(clinic: c,
                           tasks: [t],
                           chairs: [chair])
    }

    private func cleaningUp(exp: XCTestExpectation) {
        cache.delete(person: person.id) {
            self.cache.delete(clinic: self.cinfo.clinic.id) {
                self.cache.delete(task: self.ctask.task.id) {
                    self.cache.delete(chair: self.cinfo.chairs.first!.id) {
                        exp.fulfill()
                    }
                }
            }
        }
    }

    private struct const {
        static let kId = "_test_key_id_"
    }

    /// clinic task
    private var ctask: ClinicTask!

    /// clinic info
    private var cinfo: ClinicInfo!

    /// person for operations
    private var person: Person!

    /// organization for operations
    private var org: Org!

    /// date used for creation of tasks and friends
    private var curDate: Date!

    // MARK: - Private
    private let disposeBag = DisposeBag()
}

class FakeClinicTaskSimpleNetwork: DummyNet {
    init(clinics: [Clinic],
         info: [ClinicInfo],
         tasks: [ClinicTask]) {
        self.clinics = clinics
        self.info = info
        self.tasks = tasks
    }

    override func getClinicsFor(organization: Org,
                                and login: String,
                                success: (([Clinic]) -> Void)?,
                                failure: ((NSError) -> Void)?) {
        success?(clinics)
    }

    override func getInfoFor(clinicId: String,
                             success: ((ClinicInfo) -> Void)?,
                             failure: ((NSError) -> Void)?) {
        for i in info {
            if i.clinic.id == clinicId {
                success?(i)
                return
            }
        }
        let e = NSError(domain: Const.domain,
                        code: -1,
                        userInfo: [NSLocalizedDescriptionKey: "not found"])
        failure?(e)
    }

    override func getTasksFor(clinicId: String,
                              from dateStart: Date,
                              till dateEnd: Date,
                              success: (([ClinicTask]) -> Void)?,
                              failure: ((NSError) -> Void)?) {
        success?(tasks)
    }

    private let clinics: [Clinic]
    private let tasks: [ClinicTask]
    private let info: [ClinicInfo]
}

extension FakeClinicTaskSimpleNetwork: NetScheduleProtocol { }

struct Theme: ITheme {
    let name: String
}
