//
//  aliases.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

typealias ACoordinator = BaseCoordinator<AlertBuilder>

class UXCoordinator: ACoordinator, CommonCoordinator {
    /// returns main storyboard needed by a lot of others
    var storyboard: UIStoryboard {
        get {
            return ServicesAssembler.shared.storyboard
        }
    }

    // MARK: - CommonCoordinator

    func exitFrom(coordinator: Any?, with result: Any?) {
        if let c = coordinator as? ACoordinator {
            exitFrom(coordinator: c, with: result)
        }
    }
}
