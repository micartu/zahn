//
//  Theme.swift
//  zahn
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

// uses a concrete theme
protocol TThemeble: class {
    func apply(theme: Theme)
}

struct Theme: ITheme {
    let loadScreenBackColor: UIColor
    let activeColor: UIColor
    let inactiveColor: UIColor
    let buttonActiveTextColor: UIColor
    let translucentButtonBGColor: UIColor
    let separatorColor: UIColor
    let secondaryColor: UIColor
    let emptyBackColor: UIColor
    let warnColor: UIColor
    let tintColor: UIColor
    let mediumFont: UIFont
    let regularFont: UIFont
    let boldFont: UIFont
    let lightFont: UIFont
    let mainColor: UIColor
}
