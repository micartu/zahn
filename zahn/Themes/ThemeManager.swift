//
//  ThemeManager.swift
//  zahn
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

protocol ThemeManagerDelegate: class {
    func themeChanged(_ theme: Theme)
}

protocol ThemeManagerProtocol: class {
    func set(delegate: ThemeManagerDelegate)
    func getCurrentTheme() -> Theme
}

protocol ThemebleRouting: class {
    func change(theme: Theme)
}

class ThemeManager: ThemeManagerProtocol {
    var delegate: ThemeManagerDelegate?
    var theme: Theme?

    init() {
        let loginBlackColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1) /* #333333 */
        let separatorColor = #colorLiteral(red: 0.5922, green: 0.5922, blue: 0.5922, alpha: 1) /* #979797 */
        let warnColor = UIColor.red
        let greyColor = #colorLiteral(red: 0.9098, green: 0.9098, blue: 0.9098, alpha: 1) /* #e8e8e8 */
        let lightBlueColor = #colorLiteral(red: 0.2314, green: 0.5647, blue: 1, alpha: 1) /* #3b90ff */
        let lightBlackColor = #colorLiteral(red: 0.2902, green: 0.2902, blue: 0.2902, alpha: 1) /* #4a4a4a */
        self.theme = Theme(loadScreenBackColor: loginBlackColor,
                           activeColor: lightBlueColor,
                           inactiveColor: greyColor,
                           buttonActiveTextColor: UIColor.white,
                           translucentButtonBGColor: UIColor.lightGray.withAlphaComponent(0.7),
                           separatorColor: separatorColor,
                           secondaryColor: lightBlueColor,
                           emptyBackColor: UIColor.white,
                           warnColor: warnColor,
                           tintColor: lightBlueColor,
                           mediumFont: UIFont(name: "Roboto-Medium", size: 17)!,
                           regularFont: UIFont(name: "Roboto-Regular", size: 12)!,
                           boldFont: UIFont(name: "Roboto-Black", size: 12)!,
                           lightFont: UIFont(name: "Roboto-Light", size: 12)!,
                           mainColor: lightBlackColor)
    }

    public func set(delegate: ThemeManagerDelegate) {
        self.delegate = delegate
    }

    public func getCurrentTheme() -> Theme {
        return theme!
    }
}

extension ThemeManager: ThemebleRouting {
    func change(theme: Theme) {
        self.theme = theme
        self.delegate?.themeChanged(theme)
    }
}
