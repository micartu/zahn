//
//  RoundButton.swift
//  zahn
//
//  Created by Michael Artuerhof on 24.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {
    @IBInspectable
    var cornerRadius: CGFloat = 25 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable
    var customBGColor: UIColor = UIColor.init(red: 0, green: 122/255, blue: 255/255, alpha: 1) {
        didSet {
            hookOnEnabled()
        }
    }

    @IBInspectable
    var disabledBGColor: UIColor = UIColor.lightGray {
        didSet {
            hookOnEnabled()
        }
    }

    override var isEnabled: Bool {
        didSet {
            hookOnEnabled()
        }
    }

    private func hookOnEnabled() {
        if !isEnabled {
            refresh(color: disabledBGColor)
        } else {
            refresh(color: customBGColor)
        }
    }

    private func refresh(color: UIColor) {
        let size: CGSize = CGSize(width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let bgImage: UIImage = (UIGraphicsGetImageFromCurrentImageContext() as UIImage?)!
        UIGraphicsEndImageContext()
        setBackgroundImage(bgImage, for: .normal)
        clipsToBounds = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame);
        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    override func prepareForInterfaceBuilder() {
        sharedInit()
    }

    func sharedInit() {
    }
}
