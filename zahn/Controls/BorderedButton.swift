//
//  BorderedButton.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class BorderedButton: UIButton {
    @IBInspectable
    var cornerRadius: CGFloat = 25 {
        didSet {
            layer.cornerRadius = cornerRadius
            backgroundColor = .clear
            layer.borderWidth = 1
        }
    }
}
