//
//  CornedButton+Theme.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denti

extension CornedButton: TThemeble {
    func apply(theme: Theme) {
        backColor = theme.activeColor
        borderColor = theme.activeColor
        tintColor = theme.secondaryColor
        backgroundColor = theme.activeColor
        disabledBackColor = theme.inactiveColor
        disabledBorderColor = theme.inactiveColor
        titleLabel?.font = theme.mediumFont.withSize(15)
        setTitleColor(theme.buttonActiveTextColor, for: .normal)
        sizeToFit()
    }
}
