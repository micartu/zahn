//
//  PINView.swift
//  zahn
//
//  Created by Michael Artuerhof on 13.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom

protocol PINViewDelegate: class {
    func PINViewEvent(_ codeInputView: PINView, didFinishWithCode code: String)
}

class PINView: UIControl, UIKeyInput {
    weak var delegate: PINViewDelegate? = nil
    var kCount = 4 {
        didSet {
            lastWidth = -1
            rebuild()
        }
    }
    let kOffset: CGFloat = 5
    let hiddenSymbol = " "
    let enteredSymbol = "•"
    var hideEnteredSymbols = true
    var initialized = false
    let kDelWidth: CGFloat = 6.2
    let kDelHeight: CGFloat = 1.44
    var code = ""
    private var lastWidth: CGFloat = 0
    var fontName: String = "Roboto-Medium"
    var fontColor: UIColor = UIColor.white
    var fontSize: CGFloat = 40
    var timer: Timer? = nil

    var hasText: Bool {
        return nextTag > 1 ? true : false
    }

    private var nextTag = 1

    func insertText(_ text: String) {
        if nextTag <= kCount {
            (viewWithTag(nextTag)! as! UILabel).text = text
            if nextTag > 1 && hideEnteredSymbols {
                timer = nil
                hideInputText(with: enteredSymbol)
            }
            code += text
            nextTag += 1

            if nextTag > kCount {
                delay(0.3) { [weak self] in
                    guard let sself = self else { return }
                    sself.delegate?.PINViewEvent(sself, didFinishWithCode: sself.code)
                }
            } else if hideEnteredSymbols {
                timer = Timer.scheduledTimer(timeInterval: 1,
                                             target: self,
                                             selector: #selector(timerEvent),
                                             userInfo: nil,
                                             repeats: false)
            }
        }
    }

    @objc func timerEvent() {
        DispatchQueue.main.async { [weak self] in
            self?.hideInputText(with: self?.enteredSymbol ?? "")
        }
    }

    func deleteBackward() {
        if nextTag > 1 {
            nextTag -= 1
            (viewWithTag(nextTag)! as! UILabel).text = hiddenSymbol
            code = String(code.dropLast())
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        rebuild()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        rebuild()
    }

    // MARK: - UIResponder

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        rebuild()
    }

    // MARK: - UITextInputTraits

    var keyboardType: UIKeyboardType { get { return .numberPad } set { } }

    // MARK: - Private

    func resetInput() {
        hideInputText(with: hiddenSymbol)
        nextTag = 1
        code = ""
    }

    private func hideInputText(with symbol: String) {
        for i in 1..<nextTag {
            if let label = viewWithTag(i) as? UILabel {
                if label.text != symbol {
                    label.text = symbol
                }
            }
        }
    }

    private func rebuild() {
        let bwidth = bounds.width
        // are there any changes from the last time?
        if lastWidth == bwidth {
            // nothing to be rebuilt
            return
        } else {
            lastWidth = bwidth
            if subviews.count > 0 {
                for v in subviews {
                    v.removeFromSuperview()
                }
            }
        }
        initializeDefaults()
    }

    override func layoutSubviews() {
        rebuild()
        super.layoutSubviews()
    }

    private func initializeTaps() {
        if !initialized {
            initialized = true
        } else { return }
        let tap = UITapGestureRecognizer(target: self, action: #selector(becomeFirstResponder))
        addGestureRecognizer(tap)
    }

    private func initializeDefaults() {
        var lastImageView: UIImageView? = nil
        let c: CGFloat = CGFloat(kCount)
        let w: CGFloat = (bounds.width - (kOffset * 2 + kDelWidth) * c) / c
        initializeTaps()
        for i in 1...kCount {
            let imgView = UIImageView(image: UIImage(named: "PINRect")) // frame around digit
            let symbol = UILabel() // and digit itself
            symbol.translatesAutoresizingMaskIntoConstraints = false
            symbol.font = UIFont(name: fontName, size: fontSize)
            symbol.textColor = .white
            let s: String
            if code.count >= i {
                s = String(describing: code[code.index(code.startIndex, offsetBy: i - 1)])
            } else {
                s = hiddenSymbol
            }
            symbol.text = s
            symbol.tag = i
            imgView.translatesAutoresizingMaskIntoConstraints = false
            imgView.contentMode = .scaleAspectFit
            addSubview(imgView)
            addSubview(symbol)
            if lastImageView == nil {
                addConstraints([imgView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
                                imgView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor)])
            } else {
                // add a delimiter between digits
                let delimiter = UIImageView(image: UIImage(named: "pindelimiter"))
                delimiter.contentMode = .scaleAspectFit
                delimiter.translatesAutoresizingMaskIntoConstraints = false
                addSubview(delimiter)
                addConstraints([delimiter.widthAnchor.constraint(equalToConstant: kDelWidth),
                                delimiter.heightAnchor.constraint(equalToConstant: kDelHeight),
                                delimiter.leadingAnchor.constraint(equalTo: lastImageView!.trailingAnchor, constant:kOffset),
                                delimiter.centerYAnchor.constraint(equalTo: lastImageView!.centerYAnchor),
                                imgView.leadingAnchor.constraint(equalTo: delimiter.trailingAnchor, constant:kOffset),
                                imgView.centerYAnchor.constraint(equalTo: lastImageView!.centerYAnchor)])
            }
            addConstraints([imgView.widthAnchor.constraint(equalToConstant: w),
                            imgView.heightAnchor.constraint(equalTo: layoutMarginsGuide.heightAnchor),
                            symbol.centerXAnchor.constraint(equalTo: imgView.centerXAnchor),
                            symbol.centerYAnchor.constraint(equalTo: imgView.centerYAnchor)])
            lastImageView = imgView
        }
    }
}

