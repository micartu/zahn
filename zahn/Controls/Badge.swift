//
//  Badge.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class Badge: UIView {

    @IBInspectable
    var color: UIColor = UIColor.red {
        didSet {
            updateBadge(number: count)
        }
    }

    private var count = 0
    private var badgeLabel: UILabel? = nil
    private var circleLayer: CAShapeLayer? = nil

    private func setup(badge: UILabel, with number: Int) {
        badge.alpha = number > 0 ? 1 : 0
        badge.text = "\(number)"
    }

    func addBadge(number: Int) {
        removeBadge()

        let circleLayer = CAShapeLayer()
        let sz = bounds.size
        circleLayer.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0,
                                                       width: sz.width, height: sz.height)).cgPath
        circleLayer.fillColor = color.cgColor;
        circleLayer.strokeColor = color.cgColor;
        circleLayer.lineWidth = 1;
        layer.addSublayer(circleLayer)

        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = color
        label.layer.masksToBounds = true
        label.font = UIFont(name: "Roboto-Medium", size: 10)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.isUserInteractionEnabled = false

        addSubview(label)
        setup(badge: label, with: number)
        addConstraint(NSLayoutConstraint(item: label,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: label,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerY,
                                         multiplier: 1,
                                         constant: 0))

        self.circleLayer = circleLayer
        badgeLabel = label
    }

    func updateBadge(number: Int) {
        count = number
        if number == 0 {
            removeBadge()
            return
        }
        if let badge = self.badgeLabel {
            setup(badge: badge, with: number)
        } else {
            addBadge(number: number)
        }
        let scale: CGFloat = 1.4
        badgeLabel?.transform = CGAffineTransform(scaleX: scale, y: scale)
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0,
                       options: .curveEaseIn,
                       animations: { [weak self] in
                        self?.badgeLabel?.transform = .identity
        })
    }

    func removeBadge() {
        badgeLabel?.removeFromSuperview()
        badgeLabel = nil
        circleLayer?.removeFromSuperlayer()
        circleLayer = nil
    }
}
