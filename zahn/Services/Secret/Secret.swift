//
//  Secret.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import LocalAuthentication
import CryptoSwift
import Valet
import denticom

final class Secret: SecretService {
    let kEnclavePwd = "password_sec_enclave"
    let kAuthCreated = "auth_created"
    let kAuthSocial = "auth_social"
    let kAuthSocialType = "auth_social_type"
    let kAuthSocialToken = "auth_social_access_token"
    let kAuthSocialTokenId = "auth_social_token_id"
    let kAuthSocialExpire = "auth_social_expire"
    let kSession = "session"
    let kPhone = "phonenumber"
    let kMagic = "magicWord"
    let kPassword = "password"
    let kCardId = "cardIdTinkoff"
    let kUseEncrSecrets = "zahn_use_secrets"
    let basepass = "4iL7Gy0YHO25+O7JDDAr0AuAYtnBysoV"
    let kUID = "uid"
    let defaults: UserDefaults
    let enclave: SinglePromptSecureEnclaveValet
    
    init() {
        defaults = UserDefaults.standard
        enclave = SinglePromptSecureEnclaveValet.valet(with: Identifier(nonEmpty: Const.domain)!, accessControl: .userPresence)
    }
    
    func erase() {
        isAuthCreated = false
        _pwd = ""
        user = ""
        password = ""
        session = ""
        magic = ""
        isSocialAuth = false
        socialAuthExpire = nil
        socialAuthToken = ""
        socialAuthType = ""
    }
    
    var authorized: Bool {
        get {
            return magic == Const.domain
        }
    }
    var authenticated: Bool {
        get {
            if ((user.count > 0 && password.count > 0) || isSocialAuth) {
                return true
            }
            return false
        }
    }
    
    var isEncrypted: Bool {
        get {
            if let enc = defaults.object(forKey: kUseEncrSecrets) as? Bool {
                return enc
            }
            return true
        }
        set {
            // check if we have real PIN key first
            // and then recreate data using or not encryption
            if authorized {
                let usr = user
                let pwd = password
                let sess = session
                let cid = cardId
                let id = uid
                save(bool: newValue, for: kUseEncrSecrets)
                user = usr
                password = pwd
                session = sess
                cardId = cid
                uid = id
            }
        }
    }
    
    var isAuthCreated: Bool {
        get {
            return getBool(for: kAuthSocial)
        }
        set {
            save(bool: newValue, for: kAuthSocial)
        }
    }
    
    var isSocialAuth: Bool {
        get {
            return getBool(for: kAuthCreated)
        }
        set {
            save(bool: newValue, for: kAuthCreated)
        }
    }
    
    private var _pwd: String = ""
    
    var pwd: String {
        get {
            return _pwd
        }
        set {
            var s = newValue
            if s.count == 0 {
                // just rest password key
                _pwd = s
                return
            }
            if s.count < basepass.count {
                s.append(basepass)
            }
            let index = s.index(s.startIndex, offsetBy: basepass.count)
            s = String(s[..<index])
            _pwd = s
        }
    }
    
    func canAccessKeychain() -> Bool {
        return LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    var magic: String {
        get {
            return getString(for: kMagic)
        }
        set {
            save(string: newValue, for: kMagic)
        }
    }
    var session: String {
        get {
            return getString(for: kSession)
        }
        set {
            save(string: newValue, for: kSession)
        }
    }
    var user: String {
        get {
            return getString(for: kPhone)
        }
        set {
            save(string: newValue, for: kPhone)
        }
    }
    var password: String {
        get {
            return getString(for: kPassword)
        }
        set {
            save(string: newValue, for: kPassword)
        }
    }
    var cardId: String {
        get {
            return getString(for: kCardId)
        }
        set {
            save(string: newValue, for: kCardId)
        }
    }
    var uid: String {
        get {
            if let s = defaults.object(forKey: kUID) as? String {
                return s
            } else {
                return "X-X-X"
            }
        }
        set {
            defaults.set(newValue, forKey: kUID)
            defaults.synchronize()
        }
    }
    
    // MARK: - social networks stuff
    
    var socialAuthType: String {
        get {
            return getString(for: kAuthSocialType)
        }
        set {
            save(string: newValue, for: kAuthSocialType)
        }
    }
    var socialAuthTokenId: String {
        get {
            return getString(for: kAuthSocialTokenId)
        }
        set {
            save(string: newValue, for: kAuthSocialTokenId)
        }
    }
    var socialAuthToken: String {
        get {
            return getString(for: kAuthSocialToken)
        }
        set {
            save(string: newValue, for: kAuthSocialToken)
        }
    }
    var socialAuthExpire: Date? {
        get {
            return defaults.object(forKey: kAuthSocialExpire) as? Date
        }
        set {
            defaults.set(newValue, forKey: kAuthSocialExpire)
            defaults.synchronize()
        }
    }
    
    // MARK: - encryption
    
    func encrypt(_ s: String) -> String {
        do {
            let encrypted = try ChaCha20(key: pwd, iv: Const.iv).encrypt(s.bytes)
            if let str = encrypted.toBase64() {
                return str
            }
        }
        catch {
            print("Secrets, encryption error: ", error)
        }
        return ""
    }
    
    func decrypt(_ s: String) -> String {
        guard let data = Data(base64Encoded: s) else {
            return ""
        }
        do {
            let decrypted = try ChaCha20(key: pwd, iv: Const.iv).decrypt(data.bytes)
            let d = Data(bytes: decrypted)
            if let o = String(bytes: d, encoding: .utf8) {
                return o
            }
        }
        catch {
            print("Secrets, decryption error: ", error)
        }
        return ""
    }
    
    func restoreKeyFromEnclave() -> Bool {
        let out = enclave.string(forKey: kEnclavePwd, withPrompt: "Identify yourself".localized)
        switch out {
        case .success(let s):
            pwd = s
            return true
        default:
            pwd = ""
        }
        return false
    }
    
    func saveKeyToEnclave() {
        enclave.set(string: pwd, forKey: kEnclavePwd)
    }
    
    // MARK: - Private
    
    private func getBool(for key: String) -> Bool {
        if let b = defaults.object(forKey: key) as? Bool {
            return b
        }
        return false
    }
    
    private func save(bool: Bool, for key: String) {
        defaults.set(bool, forKey: key)
        defaults.synchronize()
    }
    
    private func getString(for key: String) -> String {
        if let s = defaults.object(forKey: key) as? String {
            if let enc = defaults.object(forKey: kUseEncrSecrets) as? Bool,
                enc == false, key != kMagic {
                // data isn't encrypted anymore, return it as it is:
                return s
            }
            if _pwd.count > 0 {
                return decrypt(s)
            }
            return s
        }
        return ""
    }
    
    private func save(string: String, for key: String) {
        var s: String
        if _pwd.count > 0 {
            if let enc = defaults.object(forKey: kUseEncrSecrets) as? Bool,
                enc == false, key != kMagic {
                // data isn't encrypted anymore, use it as it is:
                s = string
            } else {
                s = encrypt(string)
            }
        } else {
            s = string
        }
        defaults.set(s, forKey: key)
        defaults.synchronize()
    }
}
