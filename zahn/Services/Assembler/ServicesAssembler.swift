//
//  ServicesAssembler.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import dcache

final class ServicesAssembler {
    private static var sharedServices: ServicesAssembler = {
        let cfg = NetworkConfig.getConfiguration()
        let secret = Secret()
        let transport = MobileNetTransport(config: cfg)
        let manager = NetManager(cfg: cfg,
                                 transport: transport,
                                 secret: secret)
        let network = NetFun(netmanager: manager,
                             secret: secret)
        let cache = Cache()
        let secrets = Secret()

        let shared = ServicesAssembler(network: network,
                                       secrets: secrets,
                                       cache: cache)
        return shared
    }()

    init(network: NetFun,
         secrets: SecretService,
         cache: Cache) {
        self.network = network
        self.secrets = secrets
        self.cache = cache
        pricef = NumberFormatter()
        pricef.numberStyle = .currency
        pricef.currencySymbol = "₽"
        pricef.maximumFractionDigits = 2
        pricef.minimumFractionDigits = 0
        themeManager = ThemeManager()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
    }

    // MARK: - Public member
    let pricef: NumberFormatter
    let storyboard: UIStoryboard
    let themeManager: ThemeManager
    let cache: Cache
    let network: NetFun
    let secrets: SecretService

    // MARK: - Accessors

    static var shared: ServicesAssembler {
        return sharedServices
    }
}
