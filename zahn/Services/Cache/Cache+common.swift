//
//  Cache+common.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import denti
import dcache

// MARK: - Protocols for Denti Models

extension Cache: CacheClinicsProtocol { }

extension Cache: CacheLoginProtocol { }

extension Cache: CacheScheduleProtocol { }

extension Cache: CacheModifierProtocol { }

extension Cache: CacheTaskerProtocol { }
