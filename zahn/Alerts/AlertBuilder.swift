//
//  AlertBuilder.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti
import denticom
import Lottie
import LGSideMenuController

final class AlertBuilder: AlertBuilderProtocol {
    public static func controller(title: String? = nil, message: String? = nil, style: alertStyle) -> AlertController {
        let s: UIAlertController.Style
        switch style {
        case .actSheet:
            s = .actionSheet
        case .alert:
            s = .alert
        }
        let ac = UIAlertController(title: title, message: message, preferredStyle: s)
        return ac
    }

    public static func action(title: String, style: alertButtonStyle, handler: (() -> Void)?) -> AlertControllerAction {
        return UIAlertAction.create(atitle: title, astyle: style, ahandler: handler)
    }

    public static func chooseFrom(titles: [String],
                                  title: String,
                                  okAction:((String, Int) -> Void)?) -> ViewControlable {
        let ac = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        for i in titles.indices {
            let m = titles[i]
            let action = UIAlertAction(title: m, style: .default, handler: { a in
                if a.atitle == m {
                    okAction?(m, i)
                }
            })
            ac.add(action: action)
        }
        let cancel = UIAlertAction(title: Const.text.kCancel,
                                   style: .cancel,
                                   handler: nil)
        ac.add(action: cancel)
        return ac
    }

    static func showBusyIndicator() {
        if let smc = UIApplication.shared.keyWindow?.rootViewController as? LGSideMenuController {
            if let mv = smc.rootView {
                if mv.viewWithTag(const.kBlurTag) != nil {
                    return // it's shown already
                }
                let blured = UIView()
                blured.backgroundColor = UIColor.white.withAlphaComponent(0.5)
                blured.tag = const.kBlurTag
                mv.addSubview(blured)
                blured.translatesAutoresizingMaskIntoConstraints = false
                mv.addConstraints([blured.topAnchor.constraint(equalTo: mv.topAnchor),
                                   blured.leadingAnchor.constraint(equalTo: mv.leadingAnchor),
                                   blured.trailingAnchor.constraint(equalTo: mv.trailingAnchor),
                                   blured.bottomAnchor.constraint(equalTo: mv.bottomAnchor)])
                let anim = LOTAnimationView(name: "clock")
                anim.contentMode = .scaleAspectFill
                anim.loopAnimation = true
                // add a shadow:
                anim.layer.masksToBounds = false
                anim.layer.shadowOffset = CGSize(width: 2, height: 2)
                anim.layer.shadowRadius = 5
                anim.layer.shadowOpacity = 0.5
                blured.addSubview(anim)
                anim.translatesAutoresizingMaskIntoConstraints = false
                blured.addConstraints([anim.widthAnchor.constraint(equalToConstant: const.kClockWidth),
                                       anim.heightAnchor.constraint(equalToConstant: const.kClockWidth),
                                       anim.centerXAnchor.constraint(equalTo: blured.centerXAnchor),
                                       anim.centerYAnchor.constraint(equalTo: blured.centerYAnchor)])
                anim.play()
            }
            smc.isLeftViewDisabled = true // do not let user access menu in busy state
        }
    }

    static func hideBusyIndicator() {
        if let smc = UIApplication.shared.keyWindow?.rootViewController as? LGSideMenuController {
            smc.isLeftViewDisabled = false
            if let mv = smc.rootView {
                if let blured = mv.viewWithTag(const.kBlurTag) {
                    blured.removeFromSuperview()
                }
            }
        }
    }

    struct const {
        static let kBlurTag = 777
        static let kClockWidth: CGFloat = 76
    }
}
