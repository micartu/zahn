//
//  AskForPINCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

class AskForPINCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "AskForPINViewController") as! AskForPINViewController
        vc.dispatcher = self
        vc.initialize()
        viewControllable = vc
    }
}
