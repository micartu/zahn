//
//  AskForPINViewController.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

enum createPINResult {
    case withPIN
    case noPIN
}

final class AskForPINViewController: BaseViewController {
    @IBOutlet weak var lblCreatePIN: UILabel!
    @IBOutlet weak var btnYes: RoundButton!
    @IBOutlet weak var btnNo: BorderedButton!

    weak var dispatcher: CommonCoordinator? = nil

    func initialize() {
        navigationItem.hidesBackButton = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addDismissKeyboard()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)

        lblCreatePIN.font = theme.regularFont.withSize(25)
        btnYes.tintColor = theme.secondaryColor
        btnNo.layer.borderColor = theme.secondaryColor.cgColor
    }

    // MARK: - Actions

    @IBAction func yesAction(_ sender: Any) {
        dispatcher?.exitWith(result: createPINResult.withPIN)
    }

    @IBAction func noAction(_ sender: Any) {
        dispatcher?.exitWith(result: createPINResult.noPIN)
    }
}
