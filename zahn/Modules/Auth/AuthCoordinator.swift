//
//  AuthCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

final class AuthCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "AuthVC") as! AuthVC
        let vm = AuthViewModel(secrets: ServicesAssembler.shared.secrets)
        vm.dispatcher = self
        vc.viewModel = vm
        viewControllable = vc
    }
}
