//
//  CreatePINCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

final class CreatePINCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "CreatePINVC") as! CreatePINVC
        vc.dispatcher = self
        vc.secrets = ServicesAssembler.shared.secrets
        vc.initialize()
        viewControllable = vc
    }
}
