//
//  CreatePINVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa

final class CreatePINVC: BaseViewController {
    @IBOutlet weak var lblInputPIN: UILabel!
    @IBOutlet weak var pinView: PINView!
    var pin: String = ""

    weak var dispatcher: CommonCoordinator? = nil
    var secrets: SecretService!

    func initialize() {
        navigationItem.hidesBackButton = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addDismissKeyboard()
        secrets = ServicesAssembler.shared.secrets
        pinView.delegate = self
        pinView.becomeFirstResponder()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        lblInputPIN.font = theme.lightFont.withSize(22)
    }

    // MARK: - Private

    private func resaveData(with key: String) {
        let uid = secrets.uid
        let pass = secrets.password
        let session = secrets.session
        let user = secrets.user
        if key.count > 0 {
            secrets.pwd = key
            secrets.saveKeyToEnclave()
        } else {
            secrets.pwd = ""
        }
        // crypt data with the given key:
        secrets.uid = uid
        secrets.user = user
        secrets.password = pass
        secrets.session = session
        secrets.magic = Const.domain
        // switch off encryption, we use it only
        // for checking if user entered a correct PIN:
        secrets.isEncrypted = false
    }
}

extension CreatePINVC: PINViewDelegate {
    func PINViewEvent(_ codeInputView: PINView, didFinishWithCode code: String) {
        if pin == "" {
            pin = code
            lblInputPIN.text = "Enter again please:".localized
        } else {
            if pin == code {
                resaveData(with: code)
                dispatcher?.exitWith(result: nil)
            } else {
                dispatcher?.show(error: "PINs do not match!".localized)
                pin = ""
                lblInputPIN.text = "Enter your PIN:".localized
            }
        }
        pinView.resetInput()
    }
}
