//
//  AuthVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa
import AVFoundation

final class AuthVC: BaseViewController, PINViewDelegate {
    @IBOutlet weak var lblPIN: UILabel!
    @IBOutlet weak var btnFingerprint: UIButton!
    @IBOutlet weak var pinView: PINView!
    @IBOutlet weak var btnForgotPass: UIButton!
    weak var fingerPrintOnKeyboard: UIButton? = nil
    
    var viewModel: AuthViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDismissKeyboard()
        bind()
        pinView.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        pinView.becomeFirstResponder()
    }
    
    override func apply(theme: Theme) {
        super.apply(theme: theme)
        navigationController?.setNavigationBarHidden(true, animated: false)
        lblPIN.font = theme.lightFont.withSize(22)
        btnForgotPass.titleLabel?.font = theme.mediumFont.withSize(15)
    }
    
    // MARK: - Private
    
    private func bind() {
        btnFingerprint.rx.tap
            .bind(to: viewModel.performFingerAuth)
            .disposed(by: disposeBag)
        
        btnForgotPass.rx.tap
            .bind(to: viewModel.performPassReset)
            .disposed(by: disposeBag)
        
        viewModel.fingerprintHidden.asObservable()
            .subscribe(onNext: { [weak self] hidden in
                self?.btnFingerprint.isHidden = hidden
                self?.fingerPrintOnKeyboard?.isHidden = hidden
            })
            .disposed(by: disposeBag)
        
        viewModel.resetInput.asObservable()
            .subscribe(onNext: { [weak self] reset in
                if reset {
                    if let code = self?.pinView.code, code.count > 0 {
                        self?.wrongPasswordAnimation()
                    }
                    self?.pinView.resetInput()
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func t(_ t: Double) -> NSNumber {
        return NSNumber(value: t)
    }
    
    private func wrongPasswordAnimation() {
        let a = CAKeyframeAnimation()
        a.keyPath = "position.x"
        a.values = [0, 10, -10, 10, 0]
        a.keyTimes = [0, t(1/8), t(3/8), t(5/8), 1]
        a.duration = 0.4
        a.isAdditive = true
        pinView.layer.add(a, forKey: "shake")
        AudioServicesPlayAlertSoundWithCompletion(kSystemSoundID_Vibrate, nil)
    }
    
    // MARK: - PINViewDelegate
    
    func PINViewEvent(_ codeInputView: PINView, didFinishWithCode code: String) {
        viewModel.pass.value = code
    }
    
    // MARK: - Actions / Custom stuff
    
    @objc func callFingerPrint() {
        btnFingerprint.sendActions(for: .touchUpInside)
    }
    
    @objc func keyboardWillShow(_ note : Notification) -> Void{
        DispatchQueue.main.async { [weak self] in
            guard let sself = self else { return }
            if sself.viewModel.fingerprintHidden.value {
                return
            }
            let button = UIButton()
            button.setImage(UIImage(named: "fingerprint"), for: .normal)
            let keyBoardWindow = UIApplication.shared.windows.last
            button.frame = CGRect(x: 0, y: (keyBoardWindow?.frame.size.height)!-53, width: 106, height: 53)
            keyBoardWindow?.addSubview(button)
            keyBoardWindow?.bringSubviewToFront(button)
            button.addTarget(sself, action: #selector(sself.callFingerPrint), for: .touchUpInside)
            sself.fingerPrintOnKeyboard = button
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
