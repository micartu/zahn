//
//  AppCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import LGSideMenuController

protocol RootCoordinator {
    func launchFrom(_ window: UIWindow)
}

final class AppCoordinator: UXCoordinator {
    init(secrets: SecretService) {
        self.secrets = secrets
        super.init(parent: nil)

        // global notifications
        NotificationCenter.default.addObserver(self, selector: #selector(handleAuthError),
                                               name: NSNotification.Name(rawValue: Const.notification.kAuthErr),
                                               object: nil)
    }

    private func nextCoordinator() -> UXCoordinator {
        let next: UXCoordinator
        if !secrets.authenticated {
            next = LoginCoordinator(parent: self)
        } else if !secrets.isAuthCreated {
            next = CreatePINCoordinator(parent: self)
        } else {
            next = AuthCoordinator(parent: self)
        }
        return next
    }

    override func start() {
        smc = LGSideMenuController()
        smc.leftViewWidth = 250.0
        smc.leftViewPresentationStyle = .slideBelow
        let next = nextCoordinator()
        coordinate(to: next)
        navigationController = UINavigationController(rootViewController: next.viewControllable as! UIViewController)
        smc.rootViewController = navigationController as? UIViewController
        viewControllable = smc
    }

    // MARK: - RootCoordinator

    func setMenuController(to vc: UIViewController?) {
        smc.leftViewController = vc
        if vc != nil {
            smc.rootViewCoverColorForLeftView = UIColor.black.withAlphaComponent(0.5)
        }
    }

    func load(menus: [[menuType]]) {
        let leftMenuVC = storyboard.instantiateViewController(withIdentifier: "menu") as! MenuViewController
        leftMenuVC.delegate = self
        leftMenuVC.show(menus: menus)
        setMenuController(to: leftMenuVC)
    }

    private func loadMenus() {
        // load left menu
        let menus: [[menuType]] = [[.schedule,
                                    .profile,
                                    .clinics],
                                   [.settings],
                                   [.separator],
                                   [.about]]
        load(menus: menus)
    }

    // CommonCoordinator

    override func exitFrom(coordinator: BaseCoordinator<AlertBuilder>, with result: Any?) {
        super.exitFrom(coordinator: coordinator, with: result)
        let logout = { [weak self] in
            // we did logout so we have to remove all the user's data
            // clean secrets first
            self?.secrets.erase()
            let login = LoginCoordinator(parent: self)
            self?.coordinate(to: login)
            self?.setMenuController(to: nil)
        }
        print("exit from coordinator: \(coordinator)")
        switch coordinator {
        case is LoginCoordinator:
            if let r = result as? loginOut {
                let def = UserDefaults.standard
                def.persist(obj: r.person, for: Const.ids.kCurPerson)
                def.persist(obj: r.org, for: Const.ids.kCurOrg)
                def.synchronize()
				// create pin creator
                let createPIN = CreatePINCoordinator(parent: self)
                coordinate(to: createPIN)
            } else {
                fatalError("couldn't grab user and his organization")
            }
        case is CreatePINCoordinator:
            secrets.isAuthCreated = true
            fallthrough
        case is AuthCoordinator:
            if secrets.authorized {
                let (usr, org) = getSavedUserOrg()
                loadMenus()
                setActive(menu: .schedule)
                moveToScheduleOf(user: usr,
                                 org: org)
            } else {
                logout()
            }
        default:
            break
        }
        super.exitFrom(coordinator: coordinator, with: result)
        // clean old view controllers but after some delay
        // otherwise we'll break the transition animation :(
        delay(0.5) { [weak self] in
            self?.cleanControllersStack()
        }
    }

    internal func cleanControllersStack() {
        if let nav = navigationController as? UINavigationController {
            if let top = nav.viewControllers.last {
                nav.setViewControllers([top], animated: false)
            }
        }
    }

    // MARK: - Actions

    @objc
    private func handleAuthError() {
        logoutAction()
    }

    // MARK: - BaseCoordinator

    override func presented(on vc: ViewControlable, what nvc: ViewControlable?) {
        if let nvc = nvc {
            navigationController?.pushViewController(nvc, animated: true)
        }
    }

    internal func move(to coordinator: UXCoordinator) {
        ccoordinator = coordinator
        coordinate(to: coordinator)
        (coordinator.viewControllable as? BaseViewController)?.menuController = self
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    weak var ccoordinator: UXCoordinator? = nil
    internal var secrets: SecretService
    internal var smc: LGSideMenuController!
}

extension AppCoordinator: MenuRoutable {
    func openMenu() {
        smc.showLeftViewAnimated()
    }

    func closeMenu() {
        smc.hideLeftViewAnimated()
    }
}

extension AppCoordinator: RootCoordinator {
    func launchFrom(_ window: UIWindow) {
        start()
        if let vc = viewControllable as? UIViewController {
            window.rootViewController = vc
        }
    }
}
