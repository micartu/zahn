//
//  AppCoordinator+Go.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

extension AppCoordinator {
    internal func moveToClinicListOf(user: Person, org: Org) {
        let t = ListClinicsCoordinator(person: user,
                                       org: org,
                                       parent: self)
        move(to: t)
    }

    internal func moveToScheduleOf(user: Person, org: Org) {
        let s = ScheduleCoordinator(person: user,
                                    org: org,
                                    parent: self)
        move(to: s)
    }

    internal func moveToSettings() {
        let s = SettingsCoordinator(parent: self)
        move(to: s)
    }

    internal func moveToProfileOf(user: Person, org: Org) {
        let p = ModifierCoordinator(person: user,
                                    org: org,
                                    parent: self)
        move(to: p)
    }

    internal func moveToTasker(user: Person, org: Org) {
        let p = TaskerCoordinator(person: user,
                                  org: org,
                                  parent: self)
        move(to: p)
    }

    internal func moveToAboutSection() {
        let a = AboutCoordinator(parent: self)
        move(to: a)
    }

    internal func getSavedUserOrg() -> (Person, Org) {
        let def = UserDefaults.standard
        if let person = def.objectFromData(for: Const.ids.kCurPerson) as? Person,
            let org = def.objectFromData(for: Const.ids.kCurOrg) as? Org {
            return (person, org)
        } else {
            fatalError("couldn't get user and his/her organization")
        }
    }

    internal func logoutAction() {
        let login = LoginCoordinator(parent: self)
        // clean all the memory
        stopChildren()
        // we did logout so we have to remove all the user's data
        // clean secrets first
        secrets.erase()
        coordinate(to: login)
        setMenuController(to: nil)
        delay(2) { [weak self] in
            self?.hideBusyIndicator()
            self?.cleanControllersStack()
        }
    }
}
