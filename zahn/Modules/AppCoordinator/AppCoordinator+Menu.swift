//
//  AppCoordinator+Menu.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom

extension AppCoordinator: MenuDelegate {
    func menuAction(_ action: menuType) -> Bool {
        switch action {
        case .clinics:
            closeAnd { [weak self] in
                if let (usr, org) = self?.getSavedUserOrg() {
                    self?.moveToClinicListOf(user: usr,
                                             org: org)
                }
            }
        case .schedule:
            closeAnd { [weak self] in
                if let (usr, org) = self?.getSavedUserOrg() {
                    self?.moveToScheduleOf(user: usr,
                                           org: org)
                }
            }
        case .profile:
            closeAnd { [weak self] in
                if let (usr, org) = self?.getSavedUserOrg() {
                    self?.moveToProfileOf(user: usr,
                                           org: org)
                }
            }
        case .messages:
            closeAnd { [weak self] in
                if let (usr, org) = self?.getSavedUserOrg() {
                    self?.moveToTasker(user: usr,
                                       org: org)
                }
            }
        case .settings:
            closeAnd { [weak self] in
                self?.moveToSettings()
            }
        case .about:
            closeAnd { [weak self] in
                self?.moveToAboutSection()
            }
        case .separator:
            break
        default:
            print("unhandled menu action: \(action)")
            show(title: "", message: "The section would be available in the next version".localized)
        }
        return true
    }

    // MARK: - Private

    private func closeAnd(unload: Bool, action: @escaping (() -> Void)) {
        let act = { [weak self] in
            if unload {
                if let l = self?.ccoordinator {
                    self?.exitFrom(coordinator: l, with: nil)
                }
            }
            action()
        }
        if smc.isLeftViewVisible {
            closeMenu()
            // wait until closing animation ends:
            delay(Const.kAnimationTime) {
                act()
            }
        } else { // simply go to the menu, without closing anything
            act()
        }
    }

    private func closeAnd(action: @escaping (() -> Void)) {
        closeAnd(unload: true, action: action)
    }

    func setActive(menu: menuType) {
        if let lm = smc.leftViewController as? MenuViewController {
            lm.activeMenu = menu
            lm.update()
        }
    }

    func activeMenu() -> menuType {
        if let lm = smc.leftViewController as? MenuViewController {
            return lm.activeMenu
        }
        return .map
    }
}
