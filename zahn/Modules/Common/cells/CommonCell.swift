//
//  CommonCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol CommonCell: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension CommonCell {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}
