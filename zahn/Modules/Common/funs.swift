//
//  funs.swift
//  zahn
//
//  Created by Michael Artuerhof on 02.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import STPopup

enum phoneModel {
    case unknown
    case iPhoneX
    case iPhoneXR
    case iPhoneXSMax
}

func modelOfPhone() -> phoneModel {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2688: // iPhone XS Max
            return .iPhoneXSMax
        case 1792: // iPhone XR
            return .iPhoneXR
        case 2436: // iPhone X
            return .iPhoneX
        default:
            // do nothing
            break
        }
    }
    return .unknown
}

func generatePopUp(for vc: UIViewController,
                   yfactor: CGFloat = 0.8,
                   xfactor: CGFloat = 0.8) -> STPopupController {
    return generatePopUp(for: vc,
                         width: -1,
                         height: -1,
                         xfactor: xfactor,
                         yfactor: yfactor)
}

private func getMeasureFor(value: CGFloat, lessThen: CGFloat) -> CGFloat {
    if value > lessThen || value < 0 {
        return lessThen
    }
    return value
}

func generatePopUp(for vc: UIViewController,
                   width: CGFloat,
                   height: CGFloat,
                   xfactor: CGFloat = 0.8,
                   yfactor: CGFloat = 0.8) -> STPopupController {
    let popUp = STPopupController(rootViewController: vc)
    popUp.style = .formSheet
    popUp.transitionStyle = .fade
    let sz = UIScreen.main.bounds.size
    popUp.containerView.layer.cornerRadius = 8
    let widthPortrait = getMeasureFor(value: width, lessThen: sz.width * xfactor)
    let heightPortrait = getMeasureFor(value: height, lessThen: sz.height * yfactor)
    vc.contentSizeInPopup = CGSize(width: widthPortrait,
                                   height: heightPortrait)
    let widthLand = getMeasureFor(value: height, lessThen: sz.height * xfactor)
    let heighLand = getMeasureFor(value: width, lessThen: sz.width * yfactor)
    vc.landscapeContentSizeInPopup = CGSize(width: widthLand,
                                            height: heighLand)
    return popUp
}
