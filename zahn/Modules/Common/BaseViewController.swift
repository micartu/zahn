//
//  BaseViewController.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

open class BaseViewController: UIViewController, TThemeble {
    weak var menuController: MenuRoutable? = nil

    open override func viewDidLoad() {
        super.viewDidLoad()
        let theme = ServicesAssembler.shared.themeManager.getCurrentTheme()
        apply(theme: theme)
    }

    // MARK: - TThemeble

    func apply(theme: Theme) {
        navigationItem.leftBarButtonItem?.tintColor = theme.mainColor
    }

    // MARK: - Helpers

    func addMenu() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(commonMenuAction))
    }

    func addDismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    func viewWithClearBack() -> UIView {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }

    // MARK: - Actions


    @objc
    private func dismissKeyboard() {
        self.view.endEditing(true)
    }

    @objc
    private func commonMenuAction() {
        menuController?.openMenu()
    }
}

extension UIViewController: Themeble {
    public func apply(theme: ITheme) {
        if let t = theme as? Theme {
            if let s = self as? TThemeble {
                s.apply(theme: t)
            }
        }
    }
}

extension BaseViewController: ViewControlable {
    public func present(_ vc: ViewControlableCommon, animated: Bool) {
        present(vc, animated: animated, completion: nil)
    }

    public func present(_ vc: ViewControlableCommon, animated: Bool, completion: (() -> Void)?) {
        if let v = vc as? UIViewController {
            self.present(v, animated: animated, completion: completion)
        }
    }
}
