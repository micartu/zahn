//
//  protocols.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol MenuRoutable: class {
    func openMenu()
    func closeMenu()
}
