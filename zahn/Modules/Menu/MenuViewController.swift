//
//  MenuViewController.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom

enum menuType: Int {
    case map
    case schedule
    case clinics
    case messages
    case settings
    case profile
    case logout
    case about
    case separator
}

protocol MenuDelegate: class {
    func menuAction(_ action: menuType) -> Bool
}

protocol MenuService: class {
    func show(menus: [[menuType]])
    func changeMessagesCountTo(_ count: Int)
    func update()
}

final class MenuViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var activeMenu: menuType = .map

    weak var delegate: MenuDelegate? = nil
    internal var records = [[menuType]]()
    internal var addedMenus: [[String:Any]]? = nil
    internal var theme: Theme!
    internal var messagesCount = 0
    private let kCellHeight: CGFloat = 42
    private let kFooterHeight: CGFloat = 20
    private let kFirstHeaderHeight: CGFloat = 30
    private var sepSection = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self

        let def = UserDefaults.standard
        if let menus = def.objectFromData(for: Const.keys.kDinaMenus) as? [[String:Any]] {
            addedMenus = menus
            def.set(nil, forKey: Const.keys.kDinaMenus)
            def.synchronize()
            if records.count > 0 {
                parseAddedMenus()
            }
        }

        // subscribe to notification for dynamical menu entries:
        NotificationCenter.default.addObserver(self, selector: #selector(handleHiddenMenu(obj:)),
                                               name: NSNotification.Name(rawValue: Const.notification.kShowChannels),
                                               object: nil)
    }

    // MARK: - TThemeble

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        self.theme = theme
        tableView.tintColor = theme.activeColor
    }

    // MARK: - Private

    private func parseAddedMenus() {
        if let mm = addedMenus {
            for m in mm {
                handle(info: m)
            }
        }
    }

    private func handle(info: [String:Any]) {
        if let section = info[Const.keys.kSection] as? Int,
            let mm = info[Const.keys.kMenu] as? Int,
            let hidden = info[Const.keys.kHide] as? Bool,
            let m = menuType(rawValue: mm) {
            if hidden {
                hide(menu: m, at: section)
            } else {
                add(menu: m, at: section)
            }
        }
    }

    @objc private func handleHiddenMenu(obj: Notification) {
        if let info = obj.userInfo as? [String:Any] {
            handle(info: info)
        }
    }

    private func add(menu: menuType, at section: Int) {
        for s in records.indices {
            if s == section {
                // check if it was already added to this section:
                for m in records[s] {
                    if m == menu {
                        return
                    }
                }
                records[s].append(menu)
                tableView.reloadData()
                break
            }
        }
    }

    private func hide(menu: menuType, at section: Int) {
        for s in records.indices {
            if s == section {
                for m in records[s].indices {
                    let mm = records[s][m]
                    if mm == menu {
                        records[s].remove(at: m)
                        tableView.reloadData()
                        break
                    }
                }
                break
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MenuViewController: MenuService {
    func show(menus: [[menuType]]) {
        records = menus
        parseAddedMenus()
    }

    func update() {
        tableView.reloadData()
    }

    func changeMessagesCountTo(_ count: Int) {
        messagesCount = count
        update()
    }
}

extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records[section].count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return records.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menu = (records[indexPath.section])[indexPath.row]
        if menu == .separator {
            let cell = tableView.dequeueReusableCell(withIdentifier: "separator")!
            let view = UIView()
            view.backgroundColor = .clear
            cell.backgroundView = view
            cell.selectionStyle = .none
            sepSection = indexPath.section
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuCell
        let title: String!
        switch menu {
        case .separator:
            title = ""
        case .map:
            title = "Map".localized.uppercased()
        case .logout:
            title = "Logout".localized.uppercased()
        case .messages:
            title = "Messages".localized.uppercased()
        case .profile:
            title = "Profile".localized.uppercased()
        case .schedule:
            title = "Schedule".localized.uppercased()
        case .clinics:
            title = "Clinics".localized.uppercased()
        case .settings:
            title = "Settings".localized.uppercased()
        case .about:
            title = "About App".localized.uppercased()
        }
        let font: UIFont = theme.mediumFont
        if indexPath.section == 0 {
            cell.lblTitle.font = font.withSize(20)
        } else {
            cell.lblTitle.font = font.withSize(15)
        }
        if activeMenu == menu {
            cell.activatedMark.isHidden = false
            cell.activatedMark.backgroundColor = theme.tintColor
            cell.lblTitle.textColor = theme.tintColor
        } else {
            cell.activatedMark.isHidden = true
            cell.lblTitle.textColor = theme.mainColor
        }
        cell.lblTitle.text = title
        cell.badge.color = theme.activeColor
        if menu == .messages {
            cell.badge.updateBadge(number: messagesCount)
        } else {
            cell.badge.updateBadge(number: 0)
        }
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { // align first position according to design
            return kFirstHeaderHeight
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return kFooterHeight
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section != sepSection {
            return kCellHeight
        }
        var used: CGFloat = tableView.frame.origin.y + kFirstHeaderHeight
        for i in records.indices {
            if i != sepSection {
                used += CGFloat(records[i].count) * kCellHeight + kFooterHeight
            }
        }
        let sepHeight = view.frame.size.height - used - kFooterHeight / 2
        if sepHeight > 0 {
            switch modelOfPhone() {
            case .iPhoneX:
                return sepHeight - kFooterHeight * 2
            case .iPhoneXR:
                return sepHeight - kFooterHeight * 1.0
            case .iPhoneXSMax:
                return sepHeight - kFooterHeight * 1.5
            default:
                return sepHeight
            }
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return viewWithClearBack()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewWithClearBack()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selected = (records[indexPath.section])[indexPath.row]
        // we could touch pay-entry many times in order
        // to test new location for fueling ability...
        if selected != activeMenu {
            activeMenu = selected
            _ = delegate?.menuAction(activeMenu)
            delay(0.5) { [weak self] in
                self?.update()
            }
        }
    }
}
