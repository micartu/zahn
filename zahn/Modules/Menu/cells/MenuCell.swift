//
//  MenuCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var badge: Badge!
    @IBOutlet weak var activatedMark: UIView!
}
