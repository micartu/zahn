//
//  LoginViewController.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa

final class LoginViewController: BaseViewController {
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: CornedButton!

    var viewModel: LoginViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        loginField.delegate = self
        passwordField.delegate = self
        addDismissKeyboard()
        delay(1) { [weak self] in
            self?.loginButton.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - BaseViewController

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        loginButton.apply(theme: theme)
    }

    // MARK: - Private

    private func bind() {
        loginField.rx.text
            .orEmpty // Make it non-optional
            .bind(to: viewModel.user)
            .disposed(by: disposeBag)

        passwordField.rx.text
            .orEmpty // Make it non-optional
            .bind(to: viewModel.password)
            .disposed(by: disposeBag)

        loginButton.rx.tap
            .bind(to: viewModel.performLogin)
            .disposed(by: disposeBag)
    }

    private func tryLogin() {
        loginButton.sendActions(for: .touchUpInside)
    }

    private let disposeBag = DisposeBag()
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginField {
            passwordField.becomeFirstResponder()
        }
        if textField == passwordField {
            tryLogin()
        }
        return true
    }
}
