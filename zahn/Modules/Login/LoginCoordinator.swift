//
//  LoginCoordinator.swift
//  zahn
//
//  Created by michael on 13.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

final class LoginCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let vm = LoginViewModel(network: ServicesAssembler.shared.network,
                                secrets: ServicesAssembler.shared.secrets,
                                cache: ServicesAssembler.shared.cache)
        vm.dispatcher = self
        vc.viewModel = vm
        viewControllable = vc
    }
}
