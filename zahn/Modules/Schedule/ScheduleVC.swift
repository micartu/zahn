//
//  ScheduleVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 31.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa

final class ScheduleVC: BaseViewController {
    @IBOutlet weak var lblCurDate: UILabel!
    @IBOutlet weak var imageDropDateSel: UIImageView!
    @IBOutlet weak var viewDateSelection: UIView!
    @IBOutlet weak var btnDateSelection: UIButton!
    // choose a date in calendar view:
    @IBOutlet weak var viewBorderCalendar: UIView!
    @IBOutlet weak var calHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarView: FSCalendar!

    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var btnNextMonth: UIButton!
    @IBOutlet weak var btnLastMonth: UIButton!
    @IBOutlet weak var btnCurMonth: UIButton!
    // collection view
    @IBOutlet weak var collectionView: UICollectionView!

    var curDate = Date()

    var viewModel: ScheduleViewModel!

    func initialize() {
        title = "Schedule".localized
        addMenu()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
        setupCalenderView()
        setupScheduler()

        viewModel.getContents()

        btnNextMonth.addTarget(self, action: #selector(btnNextMonthAction), for: .touchUpInside)
        btnLastMonth.addTarget(self, action: #selector(btnLastMonthAction), for: .touchUpInside)
        btnCurMonth.addTarget(self, action: #selector(btnCurMonthAction), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateColumnWidth()
        layout.recreate = true
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateColumnWidth()
        layout.recreate = true
        collectionView.reloadData()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - BaseViewController

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        viewModel.theme = theme
        self.theme = theme
        viewDateSelection.layer.borderColor = theme.tintColor.cgColor
        viewDateSelection.layer.borderWidth = 1
        viewDateSelection.layer.cornerRadius = 8
        lblCurDate.font = theme.regularFont.withSize(16)
        lblCurDate.textColor = theme.tintColor
        imageDropDateSel.tintColor = theme.tintColor
        lblMonth.font = theme.regularFont.withSize(18)
        lblMonth.textColor = theme.mainColor
    }

    // MARK: - Private

    private func setupCalenderView() {
        let scopeGesture = UIPanGestureRecognizer(target: self.calendarView,
                                                  action: #selector(calendarView.handleScopeGesture(_:)))
        calendarView.addGestureRecognizer(scopeGesture)
        calendarView.delegate = self
        calendarView.dataSource = self
        calendarView.allowsSelection = true
        //calendarView.today = nil // Hides the today circle
        calendarView.swipeToChooseGesture.isEnabled = true

        df.dateStyle = .medium
        df.timeStyle = .none

        let fweekd = Calendar.current.firstWeekday
        calendarView.firstWeekday = UInt(fweekd + 1)
        set(date: Date())
    }

    private func setupScheduler() {
        collectionView.dataSource = self
        collectionView.delegate = self

        layout.eventTimeDivider = CGFloat(viewModel.timeDiv)
        layout.cellWidth = 300
        layout.delimiter = true
        layout.topPadding = 5
        let font = theme.mediumFont.withSize(10)
        let fontAttributes = [NSAttributedString.Key.font: font]
        let time = "00:00 "
        let size = (time as NSString).size(withAttributes: fontAttributes)
        layout.leftPadding = size.width + 5
        collectionView.collectionViewLayout = layout
        collectionView.register(HourCell.self, forSupplementaryViewOfKind: CalendarViewLayout.Const.kHour,
                                withReuseIdentifier: "HourCell")
        collectionView.register(DelimiterCell.self, forSupplementaryViewOfKind: CalendarViewLayout.Const.kDelimiter,
                                withReuseIdentifier: "DelimiterCell")
        collectionView.register(HeaderCell.self, forSupplementaryViewOfKind: CalendarViewLayout.Const.kCaption,
                                withReuseIdentifier: "HeaderCell")
        collectionView.register(CurrentTimeCell.self, forSupplementaryViewOfKind: CalendarViewLayout.Const.kCurHour,
                                withReuseIdentifier: "CurrentTimeCell")
        updateColumnWidth()
        collectionView.reloadData()
    }

    private func updateColumnWidth() {
        let maxX = layout.leftPadding
        let sections = self.numberOfSections(in: collectionView)
        let cellWidth = (collectionView.bounds.width - maxX) / (sections > 0 ? CGFloat(sections) : 1)
        if cellWidth > const.minWidth {
            layout.cellWidth = cellWidth
        } else {
            layout.cellWidth = const.minWidth
        }
    }

    private func updateDate() {
        lblCurDate.text = df.string(from: curDate)
    }

    private func bind() {
        btnDateSelection.rx.tap
            .bind(to: viewModel.showHideCalendar)
            .disposed(by: disposeBag)

        viewModel.calState.asObservable()
            .subscribe(onNext: { [weak self] state in
                guard let `self` = self else { return }
                if state == .opened {
                    self.viewBorderCalendar.showView()
                    self.imageDropDateSel.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                } else {
                    self.viewBorderCalendar.hideView()
                    self.imageDropDateSel.transform = .identity
                }
            })
            .disposed(by: disposeBag)
    }

    private func moveCalendart(month: Int) {
        let date = calendarView.currentPage
        var c = DateComponents()
        c.month = month
        if let next = Calendar.current.date(byAdding: c, to: date) {
            calendarView.setCurrentPage(next, animated: true)
        }
    }

    @objc
    private func btnNextMonthAction() {
        moveCalendart(month: 1)
    }

    @objc
    private func btnLastMonthAction() {
        moveCalendart(month: -1)
    }

    @objc
    private func btnCurMonthAction() {
        calendarView.setCurrentPage(Date(), animated: true)
    }

    internal func set(date: Date) {
        let today = Calendar.current.startOfDay(for: date)
        let d = df.string(from: today)
        lblMonth.text = d
        curDate = today
        viewModel.fetchDate = today
        updateDate()
    }

    internal var df = DateFormatter()
    private let disposeBag = DisposeBag()
    private let layout = CalendarViewLayout()

    internal var theme: Theme!

    private struct const {
        static let minWidth: CGFloat = 50
    }
}

extension ScheduleVC: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        set(date: date)
    }
}

extension ScheduleVC: FSCalendarDataSource {
}

extension ScheduleVC: UICollectionViewDelegate {
}

extension ScheduleVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.sectionsCount()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.modelsCount(for: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let m = viewModel.model(for: indexPath)
        return collectionView.dequeueReusableCell(withModel: m, for: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let model: CellAnyModel
        if kind == CalendarViewLayout.Const.kHour {
            model = HourModel(indexPath: indexPath,
                              theme: theme)
        } else if kind == CalendarViewLayout.Const.kCurHour {
            model = CurrentTimeModel(date: Date())
        } else if kind == CalendarViewLayout.Const.kCaption {
            let title = viewModel.title(for: indexPath.section)
            model = HeaderModel(caption: title)
        } else {
            model = DelimiterModel(color: UIColor.lightGray)
        }
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withModel: model, for: indexPath)
    }
}

extension ScheduleVC: CalendarViewLayoutDelegate {
    func calendarViewLayout(_ layout: CalendarViewLayout,
                            timespanForCellAt indexPath: IndexPath) -> NSRange {
        let m = viewModel.model(for: indexPath)
        return m.timespan
    }
}
