//
//  ScheduleCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 31.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti
import denticom

final class ScheduleCoordinator: UXCoordinator {
    init(person: Person,
         org: Org,
         parent: BaseCoordinator<AlertBuilder>?) {
        self.person = person
        self.org = org
        super.init(parent: parent)
    }

    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleVC
        let vm = ScheduleViewModel(network: ServicesAssembler.shared.network,
                                   cache: ServicesAssembler.shared.cache)
        let today = Calendar.current.startOfDay(for: Date())
        vm.dispatcher = self
        vm.org = org
        vm.person = person
        vm.fetchDate = today
        vc.viewModel = vm
        vc.initialize()
        viewControllable = vc
    }

    private let person: Person
    private let org: Org
}
