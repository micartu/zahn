//
//  CalendarViewLayout.swift
//  zahn
//
//  Created by Michael Artuerhof on 03.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol CalendarViewLayoutDelegate {
    func calendarViewLayout(_ layout: CalendarViewLayout,
                            timespanForCellAt indexPath: IndexPath) -> NSRange
}

final class CalendarViewLayout: UICollectionViewLayout {
    var showCurrentTime: Bool = true {
        didSet {
            invalidateLayout()
        }
    }
    var header: Bool = true {
        didSet {
            invalidateLayout()
        }
    }
    var headerHeight: CGFloat = 20 {
        didSet {
            if header {
                invalidateLayout()
            }
        }
    }
    var delimiter: Bool = true {
        didSet {
            invalidateLayout()
        }
    }
    var delimiterWidth: CGFloat = 3 {
        didSet {
            if delimiter {
                invalidateLayout()
            }
        }
    }
    var delimiterTopPadding: CGFloat = 6 {
        didSet {
            if delimiter {
                invalidateLayout()
            }
        }
    }
    var eventTimeDivider: CGFloat = 1 {
        didSet {
            invalidateLayout()
        }
    }
    var topPadding: CGFloat = 0 {
        didSet {
            invalidateLayout()
        }
    }
    var leftPadding: CGFloat = 0 {
        didSet {
            invalidateLayout()
        }
    }
    var cellWidth: CGFloat = 0 {
        didSet {
            invalidateLayout()
        }
    }
    var recreate: Bool = true {
        didSet {
            if recreate {
                invalidateLayout()
            }
        }
    }

    override func prepare() {
        super.prepare()
        guard let collectionView = self.collectionView else { return }
        guard collectionView.numberOfSections != 0 else { return }
        guard recreate else { return }

        cachedSupAttribs.removeAll()
        cachedEventsAttribs.removeAll()

        var minX: CGFloat = 0
        var maxX: CGFloat = 0

        // create events themselves:
        if let delegate = collectionView.dataSource as? CalendarViewLayoutDelegate {
            for s in 0..<collectionView.numberOfSections {
                let itemsCount = collectionView.numberOfItems(inSection: s)
                for item in 0..<itemsCount {
                    let indexPath = IndexPath(item: item, section: s)
                    if let evnt = createAttributesForEvent(at: indexPath, with: delegate) {
                        if evnt.frame.minX < minX {
                            minX = evnt.frame.minX
                        }
                        if evnt.frame.maxX > maxX {
                            maxX = evnt.frame.maxX
                        }
                        cachedEventsAttribs[indexPath] = evnt
                    }
                }
                if delimiter {
                    // items till 25 on section: 0 are used by hours supplimentary views;
                    // so first item won't be shown here:
                    let indexPath = IndexPath(item: 0, section: s)
                    cachedSupAttribs[indexPath] = createAttributesForVerticalDelimiter(at: indexPath)
                }
                // create header if needed:
                if header {
                    let indexPath = IndexPath(item: 25, section: s)
                    cachedSupAttribs[indexPath] = createAttributesForCaption(at: indexPath,
                                                                             width: cellWidth)
                }
            }
        }

        // create additional views (for showing hours and stuff):
        for h in 0..<24 {
            let indexPath = IndexPath(item: h, section: 0)
            cachedSupAttribs[indexPath] = createAttributesForHours(at: indexPath,
                                                                   width: max(collectionView.bounds.width, maxX - minX))
        }
        // create current time
        if showCurrentTime {
            let indexPath = IndexPath(item: 24, section: 0)
            let time = createAttributesForCurrentHour(at: indexPath,
                                                      width: max(collectionView.bounds.width, maxX - minX))!
            cachedSupAttribs[indexPath] = time
            if leftPadding > 0 {
                collectionView.scrollRectToVisible(time.frame,
                                                   animated: false)
            }
        }

        // create items only if needed
        recreate = false
    }

    override var collectionViewContentSize: CGSize {
        let topEdge = min(cachedEventsAttribs.values.map { $0.frame.minY }.min() ?? 0,
                          cachedSupAttribs.values.map { $0.frame.minY }.min() ?? 0)
        let bottomEdge = max(cachedEventsAttribs.values.map { $0.frame.maxY }.max() ?? 0,
                             cachedSupAttribs.values.map { $0.frame.maxY }.max() ?? 0)
        let leftmostEdge = min(cachedEventsAttribs.values.map { $0.frame.minX }.min() ?? 0,
                               cachedSupAttribs.values.map { $0.frame.minX }.min() ?? 0)
        let rightmostEdge = max(cachedEventsAttribs.values.map { $0.frame.maxX }.max() ?? 0,
                                cachedSupAttribs.values.map { $0.frame.maxX }.max() ?? 0)
        return CGSize(width: rightmostEdge - leftmostEdge, height: bottomEdge - topEdge)
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attributes = cachedEventsAttribs[indexPath] else { fatalError("No attributes cached") }
        return attributes
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attributes = cachedSupAttribs[indexPath] else { fatalError("No hour attributes cached") }
        return attributes
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let sup = cachedSupAttribs
            .map { $0.value }
            .filter { $0.frame.intersects(rect) }
        let events = cachedEventsAttribs
            .map { $0.value }
            .filter { $0.frame.intersects(rect) }
        var sum = events
        sum.append(contentsOf: sup)
        return sum
    }

    // MARK: - Custom methods

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if newBounds.size != collectionView?.bounds.size {
            cachedSupAttribs.removeAll()
            cachedEventsAttribs.removeAll()
        }
        return true
    }

    override func invalidateLayout(with context: UICollectionViewLayoutInvalidationContext) {
        if context.invalidateDataSourceCounts {
            cachedSupAttribs.removeAll()
            cachedEventsAttribs.removeAll()
        }
        super.invalidateLayout(with: context)
    }

    // MARK: - Private

    private func createAttributesForEvent(at indexPath: IndexPath, with delegate: CalendarViewLayoutDelegate) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        let timespan = delegate.calendarViewLayout(self, timespanForCellAt: indexPath)
        let posY = CGFloat(timespan.location) / eventTimeDivider * const.kHourHeight
        let height = CGFloat(timespan.length) / eventTimeDivider * const.kHourHeight

        attributes.frame.size = CGSize(width: cellWidth, height: height)
        attributes.frame.origin.x = CGFloat(indexPath.section) * cellWidth + leftPadding
        attributes.frame.origin.y = posY + topPadding
        if header {
            attributes.frame.origin.y += headerHeight
        }
        return attributes
    }

    private func createAttributesForHours(at indexPath: IndexPath, width: CGFloat) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: Const.kHour, with: indexPath)
        attributes.frame.size = CGSize(width: width, height: const.kHourHeight)
        attributes.frame.origin.x = 0
        attributes.frame.origin.y = CGFloat(indexPath.item) * const.kHourHeight
        if header {
            attributes.frame.origin.y += headerHeight
        }
        return attributes
    }

    private func createAttributesForCurrentHour(at indexPath: IndexPath, width: CGFloat) -> UICollectionViewLayoutAttributes? {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: Const.kCurHour, with: indexPath)
        attributes.frame.size = CGSize(width: width, height: const.kHourHeight)
        attributes.frame.origin.x = 0
        attributes.frame.origin.y = (CGFloat(hour) + CGFloat(minute) / 60) * const.kHourHeight
        if header {
            attributes.frame.origin.y += headerHeight
        }
        return attributes
    }

    private func createAttributesForVerticalDelimiter(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: Const.kDelimiter, with: indexPath)
        attributes.frame.size = CGSize(width: delimiterWidth,
                                       height: const.kHourHeight * 24 - delimiterTopPadding)
        attributes.frame.origin.x = CGFloat(indexPath.section) * cellWidth + leftPadding
        attributes.frame.origin.y = 0
        if header {
            attributes.frame.origin.y += headerHeight - delimiterTopPadding
        }
        return attributes
    }

    private func createAttributesForCaption(at indexPath: IndexPath, width: CGFloat) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: Const.kCaption, with: indexPath)
        attributes.frame.size = CGSize(width: width,
                                       height: headerHeight)
        attributes.frame.origin.x = CGFloat(indexPath.section) * cellWidth + leftPadding
        attributes.frame.origin.y = 0
        return attributes
    }

    private var cachedEventsAttribs = [IndexPath : UICollectionViewLayoutAttributes]()
    private var cachedSupAttribs = [IndexPath : UICollectionViewLayoutAttributes]()

    private struct const {
        static let kHourHeight: CGFloat = 60
    }

    public struct Const {
        static let kHour = "hour"
        static let kCaption = "headerCaption"
        static let kDelimiter = "verticalDelimiter"
        static let kCurHour = "currentHour"
    }
}
