//
//  CurrentTimeModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol CurrentTimeCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension CurrentTimeCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension CurrentTimeModel: CurrentTimeCellModel {
    func setup(cell: CurrentTimeCell) {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        cell.set(time: String(format: "%02ld:%02ld", hour, minute))
    }
}

struct CurrentTimeModel {
    let date: Date
}
