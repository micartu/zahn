//
//  EventCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 03.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

final class EventCell: UICollectionViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var leftView: UIView!
}

extension EventCell: ScheduleCell {
    func set(title: String) {
        lblTitle.text = title
    }

    func set(color: IColor) {
        leftView.backgroundColor = color.ui
        backgroundColor = color.ui.withAlphaComponent(0.5)
    }

    func set(time: String) {
        lblTime.text = time
    }
}

extension EventCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.boldFont.withSize(15)
        lblTime.font = theme.mediumFont.withSize(12)
    }
}
