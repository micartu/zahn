//
//  DelimiterCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class DelimiterCell: UICollectionReusableView {
    var color: UIColor = UIColor(white: 0, alpha: 0.2) {
        didSet {
            line.backgroundColor = color
        }
    }
    var lineWidth: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    var offsetY: CGFloat = 5 {
        didSet {
            setNeedsLayout()
        }
    }

    override init(frame: CGRect) {
        line = UIView()
        super.init(frame: frame)

        // vertical line
        line.backgroundColor = color
        self.addSubview(line)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // delimiter size:
        line.frame = CGRect(x: 0,
                            y: offsetY,
                            width: lineWidth,
                            height: bounds.height - offsetY * 2)
    }

    // MARK: - Private

    private let line: UIView
}
