//
//  HeaderModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol HeaderCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension HeaderCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension HeaderModel: HeaderCellModel {
    func setup(cell: HeaderCell) {
        cell.set(caption: caption)
    }
}

struct HeaderModel {
    let caption: String
}
