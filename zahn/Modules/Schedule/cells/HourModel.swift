//
//  HourModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol HourCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension HourCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension HourModel: HourCellModel {
    func setup(cell: HourCell) {
        cell.font = theme.mediumFont.withSize(10)
        cell.set(time: String(format: "%02ld:00", indexPath.row))
    }
}

struct HourModel {
    let indexPath: IndexPath
    let theme: Theme
}
