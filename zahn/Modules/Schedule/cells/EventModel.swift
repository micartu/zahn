//
//  EventModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 03.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

protocol EventCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension EventCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension ScheduleEventModel: EventCellModel {
    func setup(cell: EventCell) {
        setup(scheduleCell: cell)
    }
}
