//
//  HourCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 03.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class HourCell: UICollectionReusableView {
    var background: UIColor = UIColor(white: 0, alpha: 0.2) {
        didSet {
            hourLine.backgroundColor = background
        }
    }
    var textColor: UIColor = UIColor.black {
        didSet {
            lblHour.textColor = textColor
        }
    }
    var font: UIFont = UIFont.systemFont(ofSize: 10) {
        didSet {
            lblHour.font = font
        }
    }
    var offsetTextX: CGFloat = 5 {
        didSet {
            setNeedsLayout()
        }
    }
    var offsetTrailingX: CGFloat = 10 {
        didSet {
            setNeedsLayout()
        }
    }
    var offsetY: CGFloat = 5 {
        didSet {
            setNeedsLayout()
        }
    }
    var lineHeight: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }

    override init(frame: CGRect) {
        lblHour = UILabel()
        hourLine = UIView()
        super.init(frame: frame)

        // hour label:
        lblHour.font = font
        lblHour.textColor = textColor
        lblHour.textAlignment = .left
        self.addSubview(lblHour)

        // hour delimiter line:
        hourLine.backgroundColor = background
        self.addSubview(hourLine)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // label sizes:
        lblHour.frame.origin.x = 2
        lblHour.frame.origin.y = 0

        let hourMaxX = lblHour.frame.maxX + offsetTextX

        // hour delimiter sizes:
        hourLine.frame = CGRect(x: hourMaxX,
                                y: offsetY,
                                width: self.bounds.width - hourMaxX - offsetTrailingX,
                                height: lineHeight)
    }

    func set(time: String) {
        lblHour.text = time
        lblHour.sizeToFit()
        setNeedsLayout()
    }

    func getMaxHourX() -> CGFloat {
        return lblHour.frame.maxX + offsetTextX
    }

    // MARK: - Prvate

    private let lblHour: UILabel
    private let hourLine: UIView
}
