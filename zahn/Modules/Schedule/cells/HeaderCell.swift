//
//  HeaderCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class HeaderCell: UICollectionReusableView {
    var textColor: UIColor = UIColor.black {
        didSet {
            lblTitle.textColor = textColor
        }
    }
    var font: UIFont = UIFont.systemFont(ofSize: 14) {
        didSet {
            lblTitle.font = font
        }
    }

    override init(frame: CGRect) {
        lblTitle = UILabel()
        super.init(frame: frame)

        lblTitle.font = font
        lblTitle.textColor = textColor
        lblTitle.textAlignment = .center
        lblTitle.numberOfLines = 0
        self.addSubview(lblTitle)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        addConstraints([lblTitle.topAnchor.constraint(equalTo: topAnchor),
                        lblTitle.leadingAnchor.constraint(equalTo: leadingAnchor),
                        lblTitle.trailingAnchor.constraint(equalTo: trailingAnchor),
                        lblTitle.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func set(caption: String) {
        lblTitle.text = caption
    }

    // MARK: - Prvate
    private let lblTitle: UILabel
}
