//
//  DelimiterModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol DelimiterCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension DelimiterCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension DelimiterModel: DelimiterCellModel {
    func setup(cell: DelimiterCell) {
        cell.color = color
    }
}

struct DelimiterModel {
    let color: UIColor
}
