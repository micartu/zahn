//
//  ChangeTextCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

protocol ChangeTextCellModel: CommonCell { }

extension ChangeTextModel: ChangeTextCellModel {
    func setup(cell: EditableTextCell) {
        setup(changeTextCell: cell)
    }
}
