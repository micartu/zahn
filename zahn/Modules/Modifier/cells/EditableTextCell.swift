//
//  EditableTextCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti
import denticom

final class EditableTextCell: UITableViewCell {
    weak var delegate: EditableCellDelegateProtocol? = nil
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var editField: UITextField!
    @IBOutlet weak var lblReadOnly: UILabel!
    @IBOutlet weak var viewUnderscore: UIView!
    @IBOutlet weak var imageEdit: UIImageView!
    @IBOutlet weak var imageEye: UIImageView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var btnEdit: UIButton!

    // MARK: - Actions
    @IBAction func btnEyeAction(_ sender: Any) {
        delegate?.btnEyeTouched(id: id)
    }

    @IBAction func btnEditAction(_ sender: Any) {
        if !editField.isHidden {
            editPrimaryAction(editField)
        } else {
            delegate?.btnEditTouched(id: id)
        }
    }

    @IBAction func editPrimaryAction(_ sender: Any) {
        delegate?.editEnded(id: id,
                            text: editField.text ?? "")
    }

    // MARK: - Private
    private var id: String = ""
    private var type: ChangeTextType!
    private var theme: Theme!
}

extension EditableTextCell: ChangeTextCell {
    func set(type: ChangeTextType, showPass: Bool) {
        self.type = type
        switch type {
        case .text:
            imageCategory.isHidden = true
        case .phone:
            imageCategory.isHidden = false
            imageCategory.image = UIImage(named: "phone")!
        case .password:
            imageCategory.isHidden = true
            imageEye.isHidden = false
            imageEye.image = UIImage(named: showPass ? "eye" : "eye-crossed")!
        case .mail:
            imageCategory.isHidden = false
            imageCategory.image = UIImage(named: "email")!
        case .birthday:
            imageCategory.isHidden = true
        }
        if type == .password {
            editField.isSecureTextEntry = !showPass
            imageEye.isHidden = false
            btnEye.isHidden = false
        } else {
            imageEye.isHidden = true
            btnEye.isHidden = true
            editField.isSecureTextEntry = false
        }
    }
    func set(title: String) {
        lblTitle.text = title
    }
    func set(text: String) {
        lblReadOnly.text = text
        editField.text = text
    }
    func set(id: String) {
        self.id = id
    }
    func set(editable: Bool, err: Bool) {
        editField.isHidden = !editable
        lblReadOnly.isHidden = editable
        viewUnderscore.isHidden = !editable
        imageEdit.image = UIImage(named: editable ? "done" : "edit")!
        if editable && err {
            imageCategory.tintColor = theme.warnColor
            viewUnderscore.backgroundColor = theme.warnColor
        }
        if type == .password {
            // UILabel cannot show * instead of usual symbols
            // for passwords, so use UITextField for that
            if !editable {
                editField.isHidden = false
                editField.isEnabled = false
                lblReadOnly.isHidden = true
            } else { // editable password
                editField.isEnabled = true
                btnEye.isHidden = true
                imageEye.isHidden = true
                editField.isSecureTextEntry = false
            }
        }
    }
    func set(delegate: EditableCellDelegateProtocol) {
        self.delegate = delegate
    }
    func set(focused: Bool) {
        if focused && !editField.isHidden {
            superview?.endEditing(true)
            delay(0.1) { [weak self] in
                self?.editField.becomeFirstResponder()
            }
        }
    }
}

extension EditableTextCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.boldFont.withSize(15)
        lblReadOnly.font = theme.mediumFont.withSize(15)
        imageCategory.tintColor = theme.tintColor
        imageEdit.tintColor = theme.tintColor
        imageEye.tintColor = theme.tintColor
        viewUnderscore.backgroundColor = theme.tintColor
        self.theme = theme
    }
}
