//
//  ModifierButtonCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

class ModifierButtonCell: UITableViewCell {
    weak var delegate: ModifierButtonDelegateProtocol? = nil
    @IBOutlet weak var button: RoundButton!

    // MARK: - Actions

    @IBAction func btnAction(_ sender: Any) {
        delegate?.buttonTouched(id: id)
    }

    // MARK: - Private
    private var id: String = ""
}

extension ModifierButtonCell: ModifierButtonProtocol {
    func set(delegate: ModifierButtonDelegateProtocol) {
        self.delegate = delegate
    }
    func set(title: String) {
        button.setTitle(title, for: .normal)
    }
    func set(id: String) {
        self.id = id
    }
    func set(focused: Bool) {
    }
}

extension ModifierButtonCell: TThemeble {
    func apply(theme: Theme) {
        button.customBGColor = theme.tintColor
        button.setTitleColor(theme.buttonActiveTextColor, for: .normal)
    }
}
