//
//  ModifierButtonCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denti

protocol ModifierButtonCellModel: CommonCell { }

extension ModifierButtonModel: ModifierButtonCellModel {
    func setup(cell: ModifierButtonCell) {
        setup(buttonCell: cell)
    }
}
