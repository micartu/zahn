//
//  ModifierCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

final class ModifierCoordinator: UXCoordinator {
    init(person: Person,
         org: Org,
         parent: BaseCoordinator<AlertBuilder>?) {
        self.person = person
        self.org = org
        super.init(parent: parent)
    }
    
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "ModifierVC") as! ModifierVC
        let vm = ModifierViewModel(network: ServicesAssembler.shared.network,
                                   secret: ServicesAssembler.shared.secrets,
                                   cache: ServicesAssembler.shared.cache)
        vm.dispatcher = self
        vm.person = person
        vm.org = org
        vc.viewModel = vm
        vc.initialize()
        viewControllable = vc
    }
    
    
    private let person: Person?
    private let org: Org?
}
