//
//  SettingsCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 02.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

final class SettingsCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.dispatcher = self
        vc.initialize()
        viewControllable = vc
    }
}
