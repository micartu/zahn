//
//  SettingsVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 02.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import DropDown

final class SettingsVC: BaseViewController {
    @IBOutlet weak var lblLocalizationDescr: UILabel!
    @IBOutlet weak var lblCurLanguage: UILabel!
    @IBOutlet weak var imageDropLang: UIImageView!
    @IBOutlet weak var viewLangSelection: UIView!

    weak var dispatcher: CommonCoordinator? = nil

    func initialize() {
        title = "Settings".localized
        addMenu()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addDismissKeyboard()
        initDropdown()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - BaseViewController

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        viewLangSelection.layer.borderColor = theme.mainColor.cgColor
        viewLangSelection.layer.borderWidth = 1
        viewLangSelection.layer.cornerRadius = 8
        lblLocalizationDescr.font = theme.regularFont.withSize(16)
        lblCurLanguage.font = theme.regularFont.withSize(16)
    }

    // MARK: - Actions

    @IBAction func btnLangSelAction(_ sender: Any) {
        dropDown.show()
    }

    // MARK: - Private

    private func baseToEn(lang: String) -> String {
        if lang != "Base" {
            return lang
        } else {
            return "en"
        }
    }

    private func initDropdown() {
        let langs = Bundle.main.localizations.filter({ $0 != "Base" })
        DropDown.setupDefaultAppearance()
        lblCurLanguage.text = baseToEn(lang: Bundle.main.preferredLocalizations.first!)
        // language drop down menu:
        dropDown.customCellConfiguration = nil
        dropDown.anchorView = lblCurLanguage
        dropDown.direction = .bottom
        dropDown.dismissMode = .onTap
        dropDown.width = lblCurLanguage.bounds.width * 1.3
        dropDown.dataSource = langs
        dropDown.backgroundColor = .white
        dropDown.selectionAction = { [weak self] (index, item) in
            let lang: String
            if item != "en" {
                lang = item
            } else {
                lang = "Base"
            }
            self?.lblCurLanguage.text = item
            self?.changeToLanguage(lang)
        }
    }

    private func changeToLanguage(_ langCode: String) {
        if Bundle.main.preferredLocalizations.first != langCode {
            dispatcher?.showYesNO(title: "Reload the app?".localized,
                                  message: "In order to change the language, the App must be closed and reopened by you".localized,
                                  actionYes: {
                                    let def = UserDefaults.standard
                                    def.set([langCode], forKey: "AppleLanguages")
                                    def.synchronize()
                                    delay(0.5) {
                                        exit(EXIT_SUCCESS)
                                    }
            }, actionNo: nil)
        }
    }

    private let dropDown = DropDown()
}
