//
//  AboutCoordinator.swift
//  benzo
//
//  Created by Michael Artuerhof on 01.06.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import UIKit
import denti

class AboutCoordinator: UXCoordinator {
    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        vc.dispatcher = self
        vc.initialize()
        viewControllable = vc
    }
}
