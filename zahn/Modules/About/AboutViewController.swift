//
//  AboutViewController.swift
//  benzo
//
//  Created by Michael Artuerhof on 01.06.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import UIKit
import denticom
import denti

final class AboutViewController: BaseViewController {
    @IBOutlet weak var lblVersion: UILabel!

    /// dispatcher
    weak var dispatcher: CommonCoordinator? = nil

    func initialize() {
        addMenu()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "About App".localized
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            var v = "Version:".localized +  " \(version)"
            if let bv = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                v += ".\(bv)"
            }
            lblVersion.text = v
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // make navigation bar not a translucent
        UIApplication.shared.keyWindow?.backgroundColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Themeble

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        navigationItem.leftBarButtonItem?.tintColor = theme.mainColor
        lblVersion.font = theme.mediumFont.withSize(15)
    }
}
