//
//  TaskerVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa

final class TaskerVC: BaseViewController {
    @IBOutlet weak var tableview: UITableView!

    var viewModel: TaskerViewModel!

    func initialize() {
        title = "Tasker".localized
        addMenu()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self
        tableview.dataSource = self

        bind()

        viewModel.getContents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - BaseViewController

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        viewModel.theme = theme
        self.theme = theme
    }

    // MARK: - Private

    private func bind() {
        viewModel.updateData.asObservable()
            .subscribe(onNext: { [weak self] update in
                if update {
                    self?.tableview.reloadData()
                }
            })
            .disposed(by: disposeBag)
    }

    private let disposeBag = DisposeBag()
    private var theme: Theme!
}

extension TaskerVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.modelsCount(for: section)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionsCount()
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(for: section)
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { // no header for the 1st section
            return 0
        }
        return 20
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let m = viewModel.model(for: indexPath)
        return tableView.dequeueReusableCell(withModel: m as! CellAnyModel, for: indexPath)
    }
}

extension TaskerVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return viewWithClearBack()
        }
        let v = UIView()
        let text = viewModel.title(for: section)
        let label = UILabel()
        label.font = theme.regularFont.withSize(20)
        label.text = text
        label.textColor = theme.inactiveColor
        v.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        v.backgroundColor = theme.emptyBackColor
        v.addConstraints([label.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: 40),
                          label.topAnchor.constraint(equalTo: v.topAnchor)])
        return v
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return viewWithClearBack()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        viewModel.selected(indexPath: indexPath)
    }
}
