//
//  TaskerCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import STPopup

final class TaskerCoordinator: UXCoordinator {
    init(person: Person,
         org: Org,
         parent: BaseCoordinator<AlertBuilder>?) {
        self.person = person
        self.org = org
        super.init(parent: parent)
    }

    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "TaskerVC") as! TaskerVC
        let vm = TaskerViewModel(network: ServicesAssembler.shared.network,
                                   secret: ServicesAssembler.shared.secrets,
                                   cache: ServicesAssembler.shared.cache)
        vm.dispatcher = self
        vm.org = org
        vc.viewModel = vm
        vc.initialize()
        viewControllable = vc
    }

    @objc
    internal func chooseOkAction() {
        if let p = chosenPerson {
            selectedPersonCompletion?(p)
            selectedPersonCompletion = nil
            pop?.dismiss()
            pop = nil
            chosenPerson = nil
        } else {
            show(error: "Choose a person!".localized)
        }
    }

    private let person: Person?
    private let org: Org?
    private var chosenPerson: Person? = nil
    private var pop: STPopupController? = nil
    private var selectedPersonCompletion: ((Person?) -> Void)? = nil
}

extension TaskerCoordinator: TaskerRoutable {
    func choosePerson(completion: @escaping ((Person?) -> Void)) {
        guard selectedPersonCompletion == nil else { return }
        let vm = ChoosePersonViewModel()
        vm.dispatcher = self
        let vc = storyboard.instantiateViewController(withIdentifier: "ChooserVC") as! ChooserVC
        vc.title = "Choose a Person".localized
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK",
                                                               style: .plain,
                                                               target: self,
                                                               action: #selector(chooseOkAction))
        vc.initializer = { tableview in
            let identifier = String(describing: ChooserImgTextCell.self)
            tableview.register(UINib(nibName: identifier, bundle: nil),
                               forCellReuseIdentifier: identifier)
        }
        vc.exiter = { [weak self] in
            self?.selectedPersonCompletion = nil
            self?.pop = nil
            self?.chosenPerson = nil
        }
        vc.viewModel = vm
        let p = generatePopUp(for: vc,
                              width: 300, height: 200,
                              xfactor: 0.8, yfactor: 0.5)
        p.present(in: viewControllable as! UIViewController)
        pop = p
        selectedPersonCompletion = completion
    }
}

extension TaskerCoordinator: ChosenPersonRes {
    func chosen(person: Person) {
        chosenPerson = person
    }
}
