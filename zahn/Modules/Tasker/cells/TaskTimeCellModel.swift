//
//  TaskTimeCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denti

protocol TaskTimeCellModel: CommonCell { }

extension TaskTimeModel: TaskTimeCellModel {
    func setup(cell: TaskTimeCell) {
        setup(timeCell: cell)
    }
}
