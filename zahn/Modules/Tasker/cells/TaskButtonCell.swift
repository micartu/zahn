//
//  TaskButtonCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

class TaskButtonCell: UITableViewCell {
    weak var delegate: TaskButtonDelegateProtocol? = nil
    @IBOutlet weak var button: RoundButton!

    // MARK: - Actions

    @IBAction func btnAction(_ sender: Any) {
        delegate?.buttonTouched(id: id)
    }

    // MARK: - Private
    private var id: String = ""
}

extension TaskButtonCell: TaskButtonProtocol {
    func set(delegate: TaskButtonDelegateProtocol) {
        self.delegate = delegate
    }
    func set(title: String) {
        button.setTitle(title, for: .normal)
    }
    func set(id: String) {
        self.id = id
    }
    func set(focused: Bool) {
    }
}

extension TaskButtonCell: TThemeble {
    func apply(theme: Theme) {
        button.customBGColor = theme.tintColor
        button.setTitleColor(theme.buttonActiveTextColor, for: .normal)
    }
}
