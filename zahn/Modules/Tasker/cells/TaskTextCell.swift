//
//  TaskTextCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti
import denticom

class TaskTextCell: UITableViewCell {
    weak var delegate: TaskTextDelegateProtocol? = nil
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var editText: UITextField!
    @IBOutlet weak var imageCategory: UIImageView!

    // MARK: - Actions

    @IBAction func editTextPrimaryAction(_ sender: Any) {
        if let t = editText.text {
            delegate?.changed(text: t, id: id)
        }
    }

    // MARK: - Private
    private var id: String = ""
    private var theme: Theme!
}

extension TaskTextCell: TaskTextProtocol {
    func set(delegate: TaskTextDelegateProtocol) {
        self.delegate = delegate
    }
    func set(err: Bool) {
        backgroundColor = err ? theme.warnColor : theme.emptyBackColor
    }
    func set(title: String) {
        lblTitle.text = title
    }
    func set(text: String) {
        editText.text = text
    }
    func set(id: String) {
        self.id = id
    }
    func set(editable: Bool) {
        editText.isEnabled = editable
    }
    func set(image: String?) {
        if let pimg = image,
            let im = UIImage(named: pimg) {
            imageCategory.image = im
            imageCategory.isHidden = false
        } else {
            imageCategory.isHidden = true
        }
    }
    func set(focused: Bool) {
        guard focused else { return }
        superview?.endEditing(true)
        delay(0.1) { [weak self] in
            self?.editText.becomeFirstResponder()
        }
    }
}

extension TaskTextCell: UITextFieldDelegate {
}

extension TaskTextCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.boldFont.withSize(18)
        imageCategory.tintColor = theme.tintColor
        self.theme = theme
    }
}
