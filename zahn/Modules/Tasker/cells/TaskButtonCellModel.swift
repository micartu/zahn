//
//  TaskButtonCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denti

protocol TaskButtonCellModel: CommonCell { }

extension TaskButtonModel: TaskButtonCellModel {
    func setup(cell: TaskButtonCell) {
        setup(buttonCell: cell)
    }
}
