//
//  TaskTimeCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

class TaskTimeCell: UITableViewCell {
    weak var delegate: TaskTimeDelegateProtocol? = nil
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStartDesc: UILabel!
    @IBOutlet weak var lblDurationDesc: UILabel!
    @IBOutlet weak var lblDateDesc: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    // MARK: - Actions

    @IBAction func btnStartAction(_ sender: Any) {
        delegate?.changeTime(id: id)
    }

    @IBAction func btnDurationAction(_ sender: Any) {
        delegate?.changeDuration(id: id)
    }

    @IBAction func btnDateAction(_ sender: Any) {
        delegate?.changeDate(id: id)
    }

    // MARK: - Private
    private var id: String = ""
}

extension TaskTimeCell: TaskTimeProtocol {
    func set(delegate: TaskTimeDelegateProtocol) {
        self.delegate = delegate
    }
    func set(start: String) {
        lblStart.text = start
    }
    func set(duration: String) {
        lblDuration.text = duration
    }
    func set(date: String) {
        lblDate.text = date
    }
    func set(id: String) {
        self.id = id
    }
    func set(focused: Bool) {
    }
}

extension TaskTimeCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.boldFont.withSize(18)
        lblStartDesc.font = theme.regularFont.withSize(18)
        lblDurationDesc.font = theme.regularFont.withSize(18)
        lblDateDesc.font = theme.regularFont.withSize(18)
        lblStart.font = theme.boldFont.withSize(18)
        lblDuration.font = theme.boldFont.withSize(18)
        lblDate.font = theme.boldFont.withSize(18)
    }
}
