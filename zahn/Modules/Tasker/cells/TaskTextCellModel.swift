//
//  TaskTextCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 11.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denti

protocol TaskTextCellModel: CommonCell { }

extension TaskTextModel: TaskTextCellModel {
    func setup(cell: TaskTextCell) {
        setup(textCell: cell)
    }
}
