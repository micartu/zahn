//
//  ChooseTextCellModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 12.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denti

protocol ChooseTextCellModel: CommonCell { }

extension ChooseTextModel: ChooseTextCellModel {
    func setup(cell: ChooserImgTextCell) {
        setup(textCell: cell)
    }
}
