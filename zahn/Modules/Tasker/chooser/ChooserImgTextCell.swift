//
//  ChooserImgTextCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 12.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denti

final class ChooserImgTextCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var leadingTitleConstraint: NSLayoutConstraint!
}

extension ChooserImgTextCell: ChooseTextProtocol {
    func set(title: String) {
        lblTitle.text = title
    }
    func set(image: String?) {
        if let path = image,
            let im = UIImage(named: path) {
            imageTitle.image = im
            imageTitle.showView()
        } else {
            imageTitle.hideView()
            leadingTitleConstraint.constant = 20
        }
    }
    func set(focused: Bool) {
        imageSelected.isHidden = !focused
    }
}

extension ChooserImgTextCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.regularFont.withSize(18)
        imageTitle.tintColor = theme.tintColor
        imageSelected.tintColor = theme.tintColor
    }
}
