//
//  ListClinicsCoordinator.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

final class ListClinicsCoordinator: UXCoordinator {
    init(person: Person,
         org: Org,
         parent: BaseCoordinator<AlertBuilder>?) {
        self.person = person
        self.org = org
        super.init(parent: parent)
    }

    override func start() {
        let vc = storyboard.instantiateViewController(withIdentifier: "ListClinicsVC") as! ListClinicsVC
        let vm = ListClinicsViewModel(network: ServicesAssembler.shared.network,
                                      person: person,
                                      org: org,
                                      cache: ServicesAssembler.shared.cache)
        vm.dispatcher = self
        vc.viewModel = vm
        vc.initialize()
        viewControllable = vc
    }

    private let person: Person
    private let org: Org
}
