//
//  ListClinicsVC.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti
import RxSwift
import RxCocoa

final class ListClinicsVC: BaseViewController {
    @IBOutlet weak var tableview: UITableView!

    var viewModel: ListClinicsViewModel!
    
    func initialize() {
        title = "Clinics".localized
        addMenu()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self
        tableview.dataSource = self
        
        bind()
        addDismissKeyboard()
        
        viewModel.getContents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - BaseViewController

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        viewModel.theme = theme
    }

    // MARK: - Private

    private func bind() {
        viewModel.updateData.asObservable()
            .subscribe(onNext: { [weak self] update in
                if update {
                    self?.tableview.reloadData()
                }
            })
            .disposed(by: disposeBag)
        /*
        loginButton.rx.tap
            .bind(to: viewModel.performLogin)
            .disposed(by: disposeBag)
		*/
    }

    private let disposeBag = DisposeBag()
}

extension ListClinicsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.modelsCount(for: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionsCount()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(for: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { // no header for the 1st section
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let m = viewModel.model(for: indexPath)
        return tableView.dequeueReusableCell(withModel: m, for: indexPath)
    }
}

extension ListClinicsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /*
        if section == 0 {
            return viewWithClearBack()
        }
        let v = UIView()
        let text = viewModel.sections[section]
        let label = UILabel()
        let theme = viewModel.theme!
        label.font = theme.regularFont.withSize(20)
        label.text = text
        label.textColor = theme.msgInactiveColor
        v.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        v.backgroundColor = theme.textLoginColor
        v.addConstraints([label.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: 40),
                          label.topAnchor.constraint(equalTo: v.topAnchor)])
        return v
 */
        return viewWithClearBack()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return viewWithClearBack()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

