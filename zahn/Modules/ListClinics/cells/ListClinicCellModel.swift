//
//  ListClinicCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

protocol ListClinicCellModel: CellAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

extension ListClinicCellModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

extension ClinicModel: ListClinicCellModel {
    func setup(cell: ListClinicCell) {
        setup(clinicCell: cell)
    }
}
