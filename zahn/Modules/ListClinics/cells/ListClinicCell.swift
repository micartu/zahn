//
//  ListClinicCell.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

final class ListClinicCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
}

extension ListClinicCell: ClinicCell {
    func set(title: String) {
        lblTitle.text = title
    }

    func set(notes: String) {
        lblNotes.text = notes
    }

    func set(address: String) {
        lblAddress.text = address
    }
}

extension ListClinicCell: TThemeble {
    func apply(theme: Theme) {
        lblTitle.font = theme.boldFont.withSize(15)
        lblNotes.font = theme.mediumFont.withSize(12)
        lblAddress.font = theme.mediumFont.withSize(12)
    }
}
