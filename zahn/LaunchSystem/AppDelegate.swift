//
//  AppDelegate.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import dcache

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var delegates: [AppDelegateProtocol] = [CoreDataInit(),
                                            ]
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        coordinator = AppCoordinator(secrets: ServicesAssembler.shared.secrets)
        self.window = window
        for d in delegates {
            if !d.initModule() {
                print("Problem when loading a delegate: \(d.description())")
                return false
            }
        }
        coordinator?.launchFrom(window)
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        for d in delegates {
            d.backgroundMethod?(application)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Const.notification.kBackground),
                                        object: nil,
                                        userInfo: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        for d in delegates {
            d.foregroundMethod?(application)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Const.notification.kForeground),
                                        object: nil,
                                        userInfo: nil)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        for d in delegates {
            d.remoteNotification?(userInfo)
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        for d in delegates {
            let _ = d.canOpenURL?(app, url, options)
        }
        return true
    }
    
    // MARK: - Private
    
    private var coordinator: AppCoordinator? = nil
}
