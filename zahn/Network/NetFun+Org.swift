//
//  NetFun+Org.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension NetFun: NetOrgService {
    func getOrganizations(like name: String,
                          for user: String,
                          success: (([Org]) -> Void)?,
                          failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = OrgRequestInfo(orgName: name,
                               ulogin: user,
                               base: base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/get_org", d: d, success: { (ans: [NSDictionary]) in
            DispatchQueue.main.async {
                success?(parseOrgs(from: ans))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
}
