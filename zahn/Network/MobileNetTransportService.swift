//
//  MobileNetTransportService.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import Alamofire

protocol MobileNetTransportService: NetTransportService {
    var sessionManager: SessionManager { get }
    func commonParsingResponse(_ response: DataResponse<Any>,
                               success: ([NSDictionary]) -> Void,
                               failure: ((NSError) -> Void)?)
}
