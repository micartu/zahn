//
//  PersonUpdateInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 10.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

class PersonUpdateInfo: BaseInfo {
    let orgId: Int
    let usrCreator: String
    let ulogin: String
    let id: Int
    let name: String
    let midname: String
    let surname: String
    let pass: String
    
    enum PersonUpdatCodingKeys: String, CodingKey {
        case orgId = "org_id"
        case usrCreator = "creator_login"
        case ulogin = "login"
        case id
        case name
        case midname
        case surname
        case pass = "password"
    }
    
    init(orgId: String,
         usrCreator: String,
         ulogin: String,
         id: Int,
         name: String,
         midname: String,
         surname: String,
         password: String,
         base: BaseInfoStruct) {
        self.orgId = orgId.toInt
        self.usrCreator = usrCreator
        self.ulogin = ulogin
        self.id = id
        self.name = name
        self.midname = midname
        self.surname = surname
        self.pass = password
        super.init(base: base)
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PersonUpdatCodingKeys.self)
        try container.encode(orgId, forKey: .orgId)
        try container.encode(usrCreator, forKey: .usrCreator)
        try container.encode(ulogin, forKey: .ulogin)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(midname, forKey: .midname)
        try container.encode(surname, forKey: .surname)
        try container.encode(pass, forKey: .pass)
        try super.encode(to: encoder)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PersonUpdatCodingKeys.self)
        orgId = try container.decode(Int.self, forKey: .orgId)
        usrCreator = try container.decode(String.self, forKey: .usrCreator)
        ulogin = try container.decode(String.self, forKey: .ulogin)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        midname = try container.decode(String.self, forKey: .midname)
        surname = try container.decode(String.self, forKey: .surname)
        pass = try container.decode(String.self, forKey: .pass)
        try super.init(from: decoder)
    }
}
