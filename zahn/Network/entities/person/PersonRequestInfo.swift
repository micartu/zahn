//
//  PersonRequestInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

class PersonRequestInfo: BaseInfo {
    let orgId: Int
    let rlogin: String
    let ulogin: String

    enum PersonRequestCodingKeys: String, CodingKey {
        case orgId = "org_id"
        case rlogin = "requester_login"
        case ulogin = "user_login"
    }

    init(orgId: String,
         rlogin: String,
         ulogin: String,
         base: BaseInfoStruct) {
        self.orgId = orgId.toInt
        self.rlogin = rlogin
        self.ulogin = ulogin
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PersonRequestCodingKeys.self)
        try container.encode(orgId, forKey: .orgId)
        try container.encode(rlogin, forKey: .rlogin)
        try container.encode(ulogin, forKey: .ulogin)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PersonRequestCodingKeys.self)
        orgId = try container.decode(Int.self, forKey: .orgId)
        rlogin = try container.decode(String.self, forKey: .rlogin)
        ulogin = try container.decode(String.self, forKey: .ulogin)
        try super.init(from: decoder)
    }
}
