//
//  ClinicRequestInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

class ClinicRequestInfo: BaseInfo {
    let cid: Int

    enum ClinicInfoCodingKeys: String, CodingKey {
        case cid
    }

    init(cid: String,
         base: BaseInfoStruct) {
        self.cid = cid.toInt
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ClinicInfoCodingKeys.self)
        try container.encode(cid, forKey: .cid)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ClinicInfoCodingKeys.self)
        cid = try container.decode(Int.self, forKey: .cid)
        try super.init(from: decoder)
    }
}
