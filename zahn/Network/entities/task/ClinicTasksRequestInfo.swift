//
//  ClinicTasksRequestInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

class ClinicTasksRequestInfo: BaseInfo {
    let cid: Int // clinic id
    let startDate: String
    let endDate: String

    enum ClinicTasksRequestCodingKeys: String, CodingKey {
        case cid
        case startDate = "date_start"
        case endDate = "date_end"
    }

    init(cid: String,
         startDate: String,
         endDate: String,
         base: BaseInfoStruct) {
        self.cid = cid.toInt
        self.startDate = startDate
        self.endDate = endDate
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ClinicTasksRequestCodingKeys.self)
        try container.encode(cid, forKey: .cid)
        try container.encode(startDate, forKey: .startDate)
        try container.encode(endDate, forKey: .endDate)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ClinicTasksRequestCodingKeys.self)
        cid = try container.decode(Int.self, forKey: .cid)
        startDate = try container.decode(String.self, forKey: .startDate)
        endDate = try container.decode(String.self, forKey: .endDate)
        try super.init(from: decoder)
    }
}
