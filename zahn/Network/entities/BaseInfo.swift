//
//  BaseInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 29.06.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

struct BaseInfoStruct {
    let uid: String
    let os: String
    let appVersion: String
    let osVersion: String
    let deviceId: String
}

open class BaseInfo: Codable {
    let uid: String
    let os: String
    let appVersion: String
    let osVersion: String
    let deviceId: String

    enum CodingKeys: String, CodingKey {
        case uid
        case os
        case appVersion = "app_version"
        case osVersion = "os_version"
        case deviceId = "device_id"
    }

    init(base: BaseInfoStruct) {
        self.uid = base.uid
        self.os = base.os
        self.appVersion = base.appVersion
        self.osVersion = base.osVersion
        self.deviceId = base.deviceId
    }
}
