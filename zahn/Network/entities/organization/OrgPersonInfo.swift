//
//  OrgPersonInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

class OrgPersonInfo: BaseInfo {
    let orgId: Int
    let ulogin: String

    enum OrgCodingKeys: String, CodingKey {
        case orgId = "org_id"
        case ulogin = "login"
    }

    init(orgId: String,
         ulogin: String,
         base: BaseInfoStruct) {
        self.orgId = orgId.toInt
        self.ulogin = ulogin
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: OrgCodingKeys.self)
        try container.encode(orgId, forKey: .orgId)
        try container.encode(ulogin, forKey: .ulogin)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OrgCodingKeys.self)
        orgId = try container.decode(Int.self, forKey: .orgId)
        ulogin = try container.decode(String.self, forKey: .ulogin)
        try super.init(from: decoder)
    }
}
