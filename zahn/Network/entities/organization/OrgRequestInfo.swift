//
//  OrgRequestInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

class OrgRequestInfo: BaseInfo {
    let orgName: String
    let ulogin: String

    enum OrgCodingKeys: String, CodingKey {
        case orgName = "org_name"
        case ulogin = "requester_login"
    }

    init(orgName: String,
         ulogin: String,
         base: BaseInfoStruct) {
        self.orgName = orgName
        self.ulogin = ulogin
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: OrgCodingKeys.self)
        try container.encode(orgName, forKey: .orgName)
        try container.encode(ulogin, forKey: .ulogin)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OrgCodingKeys.self)
        orgName = try container.decode(String.self, forKey: .orgName)
        ulogin = try container.decode(String.self, forKey: .ulogin)
        try super.init(from: decoder)
    }
}
