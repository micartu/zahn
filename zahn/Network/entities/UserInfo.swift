//
//  UserInfo.swift
//  zahn
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

class UserInfo: BaseInfo {
    let user: String

    enum UserCodingKeys: String, CodingKey {
        case user = "user_id"
    }

    init(user: String,
         base: BaseInfoStruct) {
        self.user = user
        super.init(base: base)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: UserCodingKeys.self)
        try container.encode(user, forKey: .user)
        try super.encode(to: encoder)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UserCodingKeys.self)
        user = try container.decode(String.self, forKey: .user)
        try super.init(from: decoder)
    }
}
