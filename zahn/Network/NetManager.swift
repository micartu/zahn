//
//  NetManager.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import Alamofire
import denticom

final class NetManager: NetManagerService {
    private let transport: MobileNetTransport
    private let cfg: NetworkConfig
    private let sessionManager: SessionManager
    private let secret: SecretService
    private let authHandler: AuthHandler

    init(cfg: NetworkConfig, transport: MobileNetTransport, secret: SecretService) {
        self.cfg = cfg
        self.transport = transport
        self.secret = secret
        self.authHandler = AuthHandler(transport: transport,
                                       secret: secret,
                                       cfg: cfg)
        sessionManager = transport.sessionManager
        sessionManager.adapter = authHandler
        sessionManager.retrier = authHandler
    }

    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?) {
        authHandler.auth(login: login,
                         password: password) { json, err in
                            if let e = err {
                                failure?(e)
                            } else {
                                let out: NSDictionary
                                if let j = json {
                                    out = j
                                } else {
                                    out = [:] as NSDictionary
                                }
                                success?(out)
                            }
        }
    }

    func logout(success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = BaseInfo(base: base)
        let d = try! JSONEncoder().encode(s)
        transport.postQuery(url: "/logout", data: d,
                            success: { (ans: NSDictionary) in
                                success?(ans)
        }, failure: { e in
            failure?(e)
        })
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        transport.getQuery(url: url, success: success, failure: failure)
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        transport.getQuery(url: url, success: success, failure: failure)
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
        transport.getRawQuery(url: url, success: success, failure: failure)
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        transport.postQuery(url: url, data: data, success: success, failure: failure)
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        transport.postQuery(url: url, data: data, success: success, failure: failure)
    }
}
