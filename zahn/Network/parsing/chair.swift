//
//  chair.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

func parseChair(from d: NSDictionary) -> Chair {
    let id = parseInt(d, key: "chair_id")
    let name = parseString(d, for: "chair_name")
    let descr = parseString(d, for: "chair_descr")
    return Chair(id: "\(id)",
                 name: name,
                 descr: descr)
}
