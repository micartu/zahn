//
//  clinic.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

func parseClinics(from d: [NSDictionary]) -> [Clinic] {
    var out = [Clinic]()
    for c in d {
        let cc = parseClinic(from: c)
        out.append(cc)
    }
    return out
}

func parseClinic(from d: NSDictionary) -> Clinic {
    let id = parseInt(d, key: "cid")
    let name = parseString(d, for: "cname")
    let address = parseString(d, for: "caddress")
    return Clinic(id: "\(id)",
        name: name,
        address: address)
}

func parseClinicPersonRoles(from d: NSDictionary) -> ClinicPersonRoles {
    let roles: [String]
    if let rr = d["roles"] as? [String] {
        roles = rr
    } else {
        roles = [String]()
    }
    return ClinicPersonRoles(clinic: parseClinic(from: d),
                             roles: roles)
}

func parseClinicsPersonRoles(from dd: [NSDictionary]) -> [ClinicPersonRoles] {
    var pp = [ClinicPersonRoles]()
    for p in dd {
        let cp = parseClinicPersonRoles(from: p)
        pp.append(cp)
    }
    return pp
}

func parseClinicInfo(from d: NSDictionary) -> ClinicInfo {
    let clinic = parseClinic(from: d)
    var tasks = [Task]()
    let df = dateFormatterForTasks()
    if let tt = d["tasks"] as? [NSDictionary] {
        for t in tt {
            let task = parseTask(from: t, df: df)
            tasks.append(task)
        }
    }
    var chairs = [Chair]()
    if let cc = d["chairs"] as? [NSDictionary] {
        for c in cc {
            let chair = parseChair(from: c)
            chairs.append(chair)
        }
    }
    return ClinicInfo(clinic: clinic,
                      tasks: tasks,
                      chairs: chairs)
}
