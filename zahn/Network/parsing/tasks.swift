//
//  tasks.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

func dateFormatterForTasks() -> DateFormatter {
    let df = DateFormatter()
    df.locale = Locale(identifier: "en_US_POSIX")
    df.dateFormat = "yyyy-MM-dd HH:mm"
    return df
}

func parseClinicTasks(from d: [NSDictionary]) -> [ClinicTask] {
    var out = [ClinicTask]()
    let df = dateFormatterForTasks()
    for c in d {
        let cc = parseClinicTask(from: c, df: df)
        out.append(cc)
    }
    return out
}

func parseClinicTask(from d: NSDictionary, df: DateFormatter) -> ClinicTask {
    let task = parseTask(from: d, df: df)
    let chairId = parseInt(d, key: "chair_id")
    let creatorId = parseInt(d, key: "task_created_person_id")
    let taskForPerson = parseInt(d, key: "task_for_person_id")
    return ClinicTask(task: task,
                      chairId: "\(chairId)",
                      creatorId: "\(creatorId)",
                      forPersonId: "\(taskForPerson)")
}

func parseTask(from d: NSDictionary, df: DateFormatter) -> Task {
    let tid = parseInt(d, key: "tid")
    let tname = parseString(d, for: "tname")
    let tdescr = parseString(d, for: "descr")
    let time = parseString(d, for: "time")
    let tduration = parseString(d, for: "tduration")
    return Task(id: "\(tid)",
        name: tname,
        time: df.date(from: time)!,
        duration: tduration.toDouble,
        descr: tdescr)
}
