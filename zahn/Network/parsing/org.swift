//
//  org.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

func parseOrgs(from d: [NSDictionary]) -> [Org] {
    var out = [Org]()
    for dd in d {
        let o = parseOrg(from: dd)
        out.append(o)
    }
    return out
}

func parseOrg(from d: NSDictionary) -> Org {
    let id = parseInt(d, key: "org_id")
    let name = parseString(d, for: "org_name")
    let notes = parseString(d, for: "org_notes")
    let address = parseString(d, for: "org_address")
    return Org(id: "\(id)",
               name: name,
               notes: notes,
               address: address)
}
