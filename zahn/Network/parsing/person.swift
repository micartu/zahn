//
//  person.swift
//  zahn
//
//  Created by Michael Artuerhof on 28.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

func parsePerson(from d: NSDictionary, login: String) -> Person {
    let id = parseString(d, for: "id")
    let name = parseString(d, for: "name")
    let midname = parseString(d, for: "midname")
    let surname = parseString(d, for: "surname")
    let address = parseString(d, for: "address")
    return Person(id: id,
                  login: login,
                  name: name,
                  midname: midname,
                  surname: surname,
                  phones: [],
                  address: address)
}
