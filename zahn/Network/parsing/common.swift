//
//  common.swift
//  zahn
//
//  Created by Michael Artuerhof on 27.08.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

func parseArrayOfStrings(_ res: NSDictionary, for key: String) -> [String]? {
    if let tmp = res[key] as? [String] {
        return tmp
    }
    return nil
}

func parseString(_ res: NSDictionary, for key: String) -> String {
    if let tmp = res[key] as? String {
        return tmp
    }
    return ""
}

func parseDouble(_ res: NSDictionary, key: String) -> Double {
    var out: String
    if let tmp = res[key] as? Double {
        return tmp
    }
    if let tmp = res[key] as? String {
        out = tmp
    } else {
        out = "0"
    }
    return out.toDouble
}

func parseInt(_ res: NSDictionary, key: String) -> Int {
    var out: Int
    if let tmp = res[key] as? Int {
        out = tmp
    }
    else if let tmp = res[key] as? String {
        out = tmp.toInt
    }
    else {
        out = 0
    }
    return out
}

func convertDate(df: DateFormatter, date: String) -> NSDate {
    let s: String
    if let range = date.range(of: ".") {
        s = String(date[date.startIndex..<range.lowerBound])
    } else if let range = date.range(of: "+") {
        s = String(date[date.startIndex..<range.lowerBound])
    } else {
        s = date
    }
    if let ddate = df.date(from: s) {
        return ddate as NSDate
    } else {
        return NSDate()
    }
}

private var osVersion = ""
private var appVersion = ""

private func getUID() -> String {
    let def = UserDefaults.standard
    let key = "UUID_of_Application"
    if let uid = def.value(forKey: key) as? String {
        return uid
    } else {
        let v = UUID().uuidString
        def.set(v, forKey: key)
        def.synchronize()
        return v
    }
}

func buildBaseInfo(uid: String) -> BaseInfoStruct {
    if osVersion.count == 0 {
        let s = ProcessInfo().operatingSystemVersion
        osVersion = "\(s.majorVersion).\(s.minorVersion).\(s.patchVersion)"
        let ver = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let bv = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        appVersion = ver + ".\(bv)"
    }
    return BaseInfoStruct(uid: uid,
                          os: "ios",
                          appVersion: appVersion,
                          osVersion: osVersion,
                          deviceId: getUID())
}
