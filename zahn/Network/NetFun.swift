//
//  NetFun.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

final class NetFun: NetFunService {
    let netmanager: NetManagerService
    let secret: SecretService
    init(netmanager: NetManagerService, secret: SecretService) {
        self.netmanager = netmanager
        self.secret = secret
    }

    // MARK: - Internal functions

    internal func post(url: String, d: Data,
                      success: @escaping ((NSDictionary) -> Void),
                      failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.postQuery(url: url, data: d,
                                       success: { (ans: NSDictionary) in
                                        success(ans)
                                        self?.executeNext()
            }, failure: { e in
                failure(e)
                self?.executeNext()
            })
        }
    }

    internal func post(url: String, d: Data,
                       success: @escaping (([NSDictionary]) -> Void),
                       failure: @escaping ((NSError) -> Void)) {
        put { [weak self] in
            self?.netmanager.postQuery(url: url, data: d,
                                       success: { (ans: [NSDictionary]) in
                                        success(ans)
                                        self?.executeNext()
            }, failure: { e in
                failure(e)
                self?.executeNext()
            })
        }
    }

    internal func put(_ op: @escaping (() -> Void)) {
        lock.lock()
        operations.append(op)
        lock.unlock()
        executeNext()
    }

    // MARK: - Private

    private func executeNext() {
        queue.addOperation { [weak self] in
            let opp: (() -> Void)?
            self?.lock.lock()
            opp = self?.operations.popLast()
            self?.lock.unlock()
            if let op = opp {
                op()
            }
        }
    }

    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "NetFun Queue"
        queue.qualityOfService = .userInitiated
        return queue
    }()
    private lazy var operations = [() -> Void]()
    internal var user = ""
    private let lock = NSLock()
}
