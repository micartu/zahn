//
//  NetFun+Clinic.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension NetFun: NetClinicService {
    func getClinicsFor(organization: Org, and login: String,
                       success: (([Clinic]) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = OrgPersonInfo(orgId: organization.id,
                              ulogin: login,
                              base: base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/clinics_org", d: d, success: { (ans: [NSDictionary]) in
            DispatchQueue.main.async {
                success?(parseClinics(from: ans))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }

    func getClinicsRolesFor(organization: Org, and login: String,
                       success: (([ClinicPersonRoles]) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = OrgPersonInfo(orgId: organization.id,
                              ulogin: login,
                              base: base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/usr_clinic_roles", d: d, success: { (ans: [NSDictionary]) in
            DispatchQueue.main.async {
                success?(parseClinicsPersonRoles(from: ans))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }

    func getInfoFor(clinicId: String,
                     success: ((ClinicInfo) -> Void)?,
                     failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = ClinicRequestInfo(cid: clinicId, base: base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/tasks_clinic", d: d, success: { (ans: NSDictionary) in
            DispatchQueue.main.async {
                success?(parseClinicInfo(from: ans))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
}
