//
//  AuthHandler.swift
//  zahn
//
//  Created by Michael Artuerhof on 09.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import Alamofire
import denticom

final class AuthHandler: RequestRetrier {
    init(transport: MobileNetTransport,
         secret: SecretService,
         cfg: NetworkConfig) {
        self.transport = transport
        self.secret = secret
        self.cfg = cfg

        refreshToken = ""
    }

    func auth(login: String,
              password: String,
              completion: @escaping ((NSDictionary?, NSError?) -> Void)) {
        let authURL = cfg.baseUrl + "/token"
        let parameters: Parameters = ["grant_type": "password",
                                      "scope": "everything",
                                      "username": login,
                                      "password": password,
                                      "client_id": cfg.clientId,
                                      "client_secret": cfg.clientSecret]
        Alamofire.request(authURL,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding(destination: .queryString))
            .responseJSON(completionHandler: { [unowned self] response in
                self.transport.commonParsingResponse(response, success: { jsonAr in
                    let json = jsonAr[0]
                    guard let token = json["access_token"] as? String else {
                        self.handleProblemsIn(json, completion: completion)
                        return
                    }
                    guard let refresh_token = json["refresh_token"] as? String else {
                        self.handleProblemsIn(json, completion: completion)
                        return
                    }
                    self.secret.session = token
                    self.refreshToken = refresh_token
                    self.authErrors = 0
                    completion(json, nil)
                }, failure: { [weak self] e in
                    guard let `self` = self else { return }
                    self.authErrors += 1
                    if self.authErrors > const.maxErr {
                        // call global deauth
                        self.authErrors = 0
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Const.notification.kAuthErr),
                                                        object: nil,
                                                        userInfo: [:])
                    }
                    completion(nil, e)
                })
            })
    }

    func should(_ manager: SessionManager,
                retry request: Request,
                with error: Error,
                completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }

        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let `self` = self else { return }

                    self.lock.lock() ; defer { self.lock.unlock() }

                    if let accessToken = accessToken, let refreshToken = refreshToken {
                        self.secret.session = accessToken
                        self.refreshToken = refreshToken
                    }

                    self.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    self.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }

    // MARK: - Private - Refresh Tokens

    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }

        isRefreshing = true

        let authURL = cfg.baseUrl + "/token"
        let parameters: Parameters = ["grant_type": "refresh_token",
                                      "scope": "everything",
                                      "access_token": secret.session,
                                      "refresh_token": refreshToken,
                                      "client_id": cfg.clientId,
                                      "client_secret": cfg.clientSecret]
        sessionManager.request(authURL, method: .post,
                               parameters: parameters,
                               encoding: URLEncoding(destination: .queryString))
            .responseJSON { [weak self] response in
                guard let `self` = self else { return }

                if
                    let json = response.result.value as? [String: Any],
                    let accessToken = json["access_token"] as? String,
                    let refreshToken = json["refresh_token"] as? String
                {
                    completion(true, accessToken, refreshToken)
                } else {
                    self.auth(login: self.secret.user,
                              password: self.secret.password) { json, err in
                                if let e = err {
                                    print("!!error in re/auth: \(e.localizedDescription)")
                                    completion(false, nil, nil)
                                } else {
                                    self.authErrors = 0
                                    completion(true, nil, nil)
                                }
                    }
                }
                self.isRefreshing = false
        }
    }

    // MARK: - Private

    private func handleProblemsIn(_ json: NSDictionary,
                                  completion: @escaping ((NSDictionary?, NSError?) -> Void)) {
        let descr: String
        if let description = json["error_description"] as? String {
            descr = description
        }
        else {
            descr = "Network Auth Problems".localized
        }
        let error = NSError(domain: Const.domain,
                            code: Const.err.kNetAuth,
                            userInfo: [NSLocalizedDescriptionKey: descr])
        completion(nil, error)
    }

    private typealias RefreshCompletion =
        (_ succeeded: Bool,
        _ accessToken: String?,
        _ refreshToken: String?) -> Void
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders

        return SessionManager(configuration: configuration)
    }()

    private let lock = NSLock()

    private let transport: MobileNetTransport
    private let cfg: NetworkConfig
    private var secret: SecretService

    private var refreshToken: String

    private var authErrors = 0
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []

    private struct const {
        static let maxErr = 4
    }
}

extension AuthHandler: RequestAdapter {
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(cfg.baseUrl) {
            var urlRequest = urlRequest
            if secret.session.count > 0 {
                urlRequest.setValue(secret.session, forHTTPHeaderField: "code")
            }
            return urlRequest
        }
        return urlRequest
    }
}
