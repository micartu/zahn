//
//  MobileNetTransport.swift
//  zahn
//
//  Created by Michael Artuerhof on 26.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import Alamofire
import denticom

final class MobileNetTransport: MobileNetTransportService {
    var sessionManager = SessionManager()

    init(config: NetworkConfig) {
        cfg = config
        base = cfg.baseUrl
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        getCommonQuery(url: url, successArray: nil, successDict: success, failure: failure)
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        getCommonQuery(url: url, successArray: success, successDict: nil, failure: failure)
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        sessionManager.request(absUrl).validate().responseData() { [unowned self] resp in
            if let response = resp.response {
                if let err = self.checkStatusCode(response.statusCode) {
                    failure?(err)
                } else {
                    if let d = resp.data {
                        if let s = success {
                            s(d)
                        }
                    } else {
                        self.handle(failure: failure,
                                    descr: "No Data was returned".localized)
                    }
                }
            }
            else {
                self.handle(failure: failure,
                            descr: "Network failure".localized)
            }
        }
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        postCommonQuery(url: url, data: data, successArray: nil, successDict: success, failure: failure)
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        postCommonQuery(url: url, data: data, successArray: success, successDict: nil, failure: failure)
    }

    // MARK: - MobileNetTransportService

    func commonParsingResponse(_ response: DataResponse<Any>,
                               success: ([NSDictionary]) -> Void,
                               failure: ((NSError) -> Void)?) {
        var error: NSError?
        if let c = response.response?.statusCode {
            error = checkStatusCode(c)
        } else {
            error = NSError(domain: Const.domain,
                            code: Const.err.kNet,
                            userInfo: [NSLocalizedDescriptionKey: "Network Status Code Failure".localized])
        }
        if let e = error {
            failure?(e)
        } else { // no errors so far
            if let result = response.result.value {
                if let jsonAr = result as? Array<NSDictionary> {
                    // array of results? it looks like all ok and it's a result of execution,
                    // so no status and we have to pass it further:
                    success(jsonAr)
                } else if let json = result as? NSDictionary {
                    // try to find the status of execution in answer
                    var statusOk = true
                    var err: NSError? = nil
                    if let status = json["status"] as? String {
                        // so we've got the status of execution (it could be set to 'ok' or 'error')
                        if status != "ok" {
                            statusOk = false
                            var code = Const.err.kNet
                            if let errCode = json["code"] as? Int {
                                code = errCode
                            }
                            let errDescription: String
                            // try to find description of error
                            if let res = json["result"] as? NSDictionary {
                                errDescription = getLocalizedValue(key: "description", from: res)
                            } else {
                                errDescription = "?"
                            }
                            err = NSError(domain: Const.domain,
                                          code: code,
                                          userInfo: [NSLocalizedDescriptionKey: errDescription])
                        }
                    }
                    // extra check for error field for explanation of what happened:
                    if json["error"] != nil {
                        statusOk = false
                        var code = Const.err.kNet
                        if let errCode = json["code"] as? Int {
                            code = errCode
                        }
                        let errDescription = getLocalizedValue(key: "error", from: json)
                        err = NSError(domain: Const.domain,
                                      code: code,
                                      userInfo: [NSLocalizedDescriptionKey: errDescription])
                    }
                    if statusOk {
                        // do we have an extra field of result?
                        if let res = json["result"] as? NSDictionary {
                            success([res])
                        } else if let res = json["result"] as? [NSDictionary] {
                            success(res)
                        } else { // no field with results so pass the whole mess further:
                            success([json])
                        }
                    } else {
                        if let e = err {
                            failure?(e)
                        } else {
                            handle(failure: failure, descr: "Cannot parse json".localized)
                        }
                    }
                }
            } else {
                handle(failure: failure, descr: "Cannot parse json".localized)
            }
        }
    }

    // MARK: - Private

    private func notLocalizedValue(_ key: String, from json: NSDictionary) -> String {
        if let v = json[key] as? String {
            return v
        }
        return ""
    }

    private func getLocalizedValue(key: String, from json: NSDictionary) -> String {
        let value: String
        if let locale = NSLocale.current.languageCode {
            if let valLocalized = json[key + "_" + locale.lowercased()] as? String {
                value = valLocalized
            } else {
                value = notLocalizedValue(key,from: json)
            }
        } else {
            value = notLocalizedValue(key, from: json)
        }
        return value
    }

    private func getCommonQuery(url: String,
                                successArray: (([NSDictionary]) -> Void)?,
                                successDict: ((NSDictionary) -> Void)?,
                                failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        sessionManager.request(absUrl).validate().responseJSON { [unowned self] response in
            self.commonParsingResponse(response, success: { json in
                if let s = successArray {
                    s(json)
                }
                if let s = successDict {
                    s(json[0])
                }
            }, failure: failure)
        }
    }

    private func postCommonQuery(url: String,
                                 data: Data,
                                 successArray: (([NSDictionary]) -> Void)?,
                                 successDict: ((NSDictionary) -> Void)?,
                                 failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        let u = URL(string: absUrl)!
        var request = URLRequest(url: u)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        sessionManager.request(request).validate().responseJSON { [unowned self] response in
            self.commonParsingResponse(response, success: { json in
                if let s = successArray {
                    s(json)
                }
                if let s = successDict {
                    if json.count > 0 {
                        s(json[0])
                    } else {
                        s([:])
                    }
                }
            }, failure: failure)
        }
    }

    private func checkStatusCode(_ status: Int) -> NSError? {
        var error: NSError?
        switch (status) {
        case 200:
            error = nil
        case 401:
            error = NSError(domain: Const.domain,
                            code: Const.err.kNetAuth,
                            userInfo: [NSLocalizedDescriptionKey: "Authorization failure".localized])
        default:
            error = NSError(domain: Const.domain,
                            code: Const.err.kNet,
                            userInfo: [NSLocalizedDescriptionKey: "Network failure".localized])
        }
        return error
    }

    private func handle(failure: ((NSError) -> Void)?, descr: String) {
        if let f = failure {
            let error = NSError(domain: Const.domain,
                                code: Const.err.kNet,
                                userInfo: [NSLocalizedDescriptionKey: descr])
            f(error)
        }
    }

    private let cfg: NetworkConfig
    private let base: String
}
