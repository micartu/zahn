//
//  NetFun+Person.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension NetFun: NetPersonService {
    func getPersonInfo(organization: Org,
                       requester rlogin: String,
                       requested ulogin: String,
                       success: ((Person) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = PersonRequestInfo(orgId: organization.id,
                                  rlogin: rlogin,
                                  ulogin: ulogin,
                                  base:base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/get_person", d: d, success: { (ans: NSDictionary) in
            DispatchQueue.main.async {
                success?(parsePerson(from: ans, login: ulogin))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
    
    func updatePersonInfo(organization: Org,
                          creator: String,
                          person p: Person,
                          updPassword: String,
                          success: (() -> Void)?,
                          failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = PersonUpdateInfo(orgId: organization.id,
                                 usrCreator: creator,
                                 ulogin: p.login,
                                 id: p.id.toInt,
                                 name: p.name,
                                 midname: p.midname,
                                 surname: p.surname,
                                 password: updPassword,
                                 base:base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/update_person", d: d, success: { (ans: NSDictionary) in
            DispatchQueue.main.async {
                success?()
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
    
    func addPerson(organization: Org,
                   creator: String,
                   person p: Person,
                   updPassword: String,
                   success: ((Person) -> Void)?,
                   failure: ((NSError) -> Void)?) {
        let base = buildBaseInfo(uid: secret.uid)
        let s = PersonUpdateInfo(orgId: organization.id,
                                 usrCreator: creator,
                                 ulogin: p.login,
                                 id: p.id.toInt,
                                 name: p.name,
                                 midname: p.midname,
                                 surname: p.surname,
                                 password: updPassword,
                                 base:base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/add_person", d: d, success: { (ans: NSDictionary) in
            DispatchQueue.main.async {
                success?(parsePerson(from: ans, login: p.login))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
}
