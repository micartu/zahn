//
//  NetFun+common.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom
import denti

protocol NetFunService:
    NetTaskService,
    NetOrgService,
    NetPersonService,
    NetClinicService,
    NetLoginService {}


// MARK: - Protocols for Denti Models

extension NetFun: NetLoginProtocol { }

extension NetFun: NetClinicsProtocol { }

extension NetFun: NetScheduleProtocol { }

extension NetFun: NetModifierProtocol { }

extension NetFun: NetTaskerProtocol { }
