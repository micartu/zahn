//
//  NetFun+Task.swift
//  zahn
//
//  Created by Michael Artuerhof on 05.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension NetFun: NetTaskService {
    func getTasksFor(clinicId: String,
                     from dateStart: Date,
                     till dateEnd: Date,
                     success: (([ClinicTask]) -> Void)?,
                     failure: ((NSError) -> Void)?) {
        let df = dateFormatterForTasks()
        let base = buildBaseInfo(uid: secret.uid)
        let s = ClinicTasksRequestInfo(cid: clinicId,
                                       startDate: df.string(from: dateStart),
                                       endDate: df.string(from: dateEnd),
                                       base: base)
        let d = try! JSONEncoder().encode(s)
        post(url: "/tasks_clinic", d: d, success: { (ans: [NSDictionary]) in
            DispatchQueue.main.async {
                success?(parseClinicTasks(from: ans))
            }
        }, failure: { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        })
    }
}
