//
//  NetFun+Login.swift
//  zahn
//
//  Created by Michael Artuerhof on 22.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import denticom

extension NetFun: NetLoginService {
    func changeCredentials(login: String, password: String, success: (() -> Void)?, failure: ((NSError) -> Void)?) {
        user = login // save it for future use
        put { [weak self] in
            self?.netmanager.auth(login: login, password: password, success: { ans in
                DispatchQueue.main.async {
                    success?()
                }
            }, failure: { e in
                DispatchQueue.main.async {
                    failure?(e)
                }
            })
        }
    }

    func logout(success: (() -> Void)?,
                failure: ((NSError) -> Void)?) {
        put { [weak self] in
            self?.netmanager.logout(success: { _ in
                success?()
            }, failure: failure)
        }
    }
}
