//
//  UITableView+Model.swift
//  zahn
//
//  Created by Michael Artuerhof on 21.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueReusableCell(withModel model: CellAnyModel, for indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: type(of: model).CellAnyType)
        let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        model.setupAny(cell: cell)
        return cell
    }
}
