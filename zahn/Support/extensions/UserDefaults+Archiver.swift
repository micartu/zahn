//
//  UserDefaults+Archiver.swift
//  zahn
//
//  Created by Michael Artuerhof on 16.07.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension UserDefaults {
    func persist(obj: Any, for key: String) {
        let archived = NSKeyedArchiver.archivedData(withRootObject: obj)
        set(archived, forKey: key)
        synchronize()
    }

    func objectFromData(for key: String) -> Any? {
        if let data = object(forKey: key) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data)
        }
        return nil
    }
}
