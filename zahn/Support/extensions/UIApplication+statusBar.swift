//
//  UIApplication+statusBar.swift
//  zahn
//
//  Created by Michael Artuerhof on 24.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
