//
//  CellAnyModel.swift
//  zahn
//
//  Created by Michael Artuerhof on 04.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol CellAnyModel {
    static var CellAnyType: UIView.Type { get }
    func setupAny(cell: UIView)
}
