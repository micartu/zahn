//
//  UINavigationController+NavigationControlable.swift
//  zahn
//
//  Created by michael on 13.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

extension  UINavigationController: NavigationControlable {
    public func pushViewController(_ vc: ViewControlable, animated: Bool) {
        if let v = vc as? UIViewController {
            self.pushViewController(v, animated: animated)
        }
    }
}

extension  UINavigationController: ViewControlable {
    public func present(_ vc: ViewControlableCommon, animated: Bool) {
        present(vc, animated: animated, completion: nil)
    }

    public func present(_ vc: ViewControlableCommon, animated: Bool, completion: (() -> Void)?) {
        if let v = vc as? UIViewController {
            present(v, animated: animated, completion: completion)
        }
    }
}
