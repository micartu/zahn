//
//  UIAlertController+AlertController.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

extension UIAlertController: ViewControlable {
    public func present(_ vc: ViewControlableCommon, animated: Bool) {
        present(vc, animated: animated, completion: nil)
    }
    
    public func present(_ vc: ViewControlableCommon, animated: Bool, completion: (() -> Void)?) {
        if let v = vc as? UIViewController {
            self.present(v, animated: animated, completion: completion)
        }
    }
}

extension UIAlertController: AlertController {
    public var textFieldsInController: [TextField]? {
        get {
            return self.textFields
        }
    }
    
    public func add(action: AlertControllerAction) {
        if let a = action as? UIAlertAction {
            addAction(a)
        }
    }
    
    public func addTextFieldWith(cfgHandler: ((TextField) -> Void)?) {
        let cfg: ((UITextField) -> Void)?
        if let c = cfgHandler {
            cfg = c
        } else {
            cfg = nil
        }
        addTextField(configurationHandler: cfg)
    }
}
