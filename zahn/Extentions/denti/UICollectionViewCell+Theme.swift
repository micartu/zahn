//
//  UICollectionViewCell+Theme.swift
//  zahn
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

extension UICollectionViewCell: Themeble {
    public func apply(theme: ITheme) {
        if let t = theme as? Theme {
            if let s = self as? TThemeble {
                s.apply(theme: t)
            }
        }
    }
}
