//
//  UITableViewCell+Theme.swift
//  zahn
//
//  Created by Michael Artuerhof on 24.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denticom
import denti

extension UITableViewCell: Themeble {
    public func apply(theme: ITheme) {
        if let t = theme as? Theme {
            if let s = self as? TThemeble {
                s.apply(theme: t)
            }
        }
    }
}
