//
//  UITextField+TextField.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

// we already have text in UITextField... so it conforms to protocol
extension UITextField: TextField { }
