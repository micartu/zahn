//
//  UIAlertAction+AlertAction.swift
//  zahn
//
//  Created by Michael Artuerhof on 23.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import denti

extension UIAlertAction: AlertControllerAction {
    public var atitle: String {
        if let t = title {
            return t
        }
        return ""
    }
    
    public var astyle: alertButtonStyle {
        let s: alertButtonStyle
        switch style {
        case .destructive:
            s = .destruct
        case .cancel:
            s = .cancel
        case .default:
            s = .def
        }
        return s
    }
    
    public static func create(atitle: String?,
                              astyle: alertButtonStyle,
                              ahandler: (() -> Void)?) -> AlertControllerAction {
        let s: UIAlertAction.Style
        switch astyle {
        case .def:
            fallthrough
        default:
            s = .default
        }
        let h: ((UIAlertAction) -> Void)?
        if let a = ahandler {
            h = { _ in
                a()
            }
        } else {
            h = nil
        }
        return UIAlertAction(title: atitle, style: s, handler: h)
    }
}
