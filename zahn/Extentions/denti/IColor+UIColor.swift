//
//  IColor+UIColor.swift
//  zahn
//
//  Created by Michael Artuerhof on 08.01.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import denticom

extension IColor {
    var ui: UIColor {
        get {
            return UIColor(red: CGFloat(r) / 255,
                           green: CGFloat(g) / 255,
                           blue: CGFloat(b) / 255,
                           alpha: CGFloat(a) / 255)
        }
    }
}
