//
//  LGSideMenuController+ViewControllable.swift
//  zahn
//
//  Created by Michael Artuerhof on 25.12.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import LGSideMenuController
import denti

extension LGSideMenuController: ViewControlable {
    public func present(_ vc: ViewControlableCommon, animated: Bool) {
        present(vc, animated: animated, completion: nil)
    }
    
    public func present(_ vc: ViewControlableCommon, animated: Bool, completion: (() -> Void)?) {
        if let v = vc as? UIViewController {
            present(v, animated: animated, completion: completion)
        }
    }
}
